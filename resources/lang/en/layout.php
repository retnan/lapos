<?php

return [

    /*
        Display all static strings in the application
    */
    'user_welcome' => 'Welcome, :name',
];