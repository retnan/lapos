<?php

return [

    /*
        String Translation for /dashboard/admin/product/create
    */
    'product_header' => 'Product Information',
    'product_title' => 'Product Name',
    'product_placeholder_title' => 'Shoes',
    'product_category' => 'Product Category',
    'product_unit_cost_price' => 'Unit Cost Price',
    'product_placeholder_unit_cost_price' => '256390.00',
    'product_tax' => 'Tax <span style="font-size: 10px;">(optional)</span>',
    'product_placeholder_tax' => '0',
    'product_tax_type' => 'Tax Type',
    'product_unit_selling_price' => 'Unit Selling Price',
    'product_placeholder_unit_selling_price' => '256342.00',
    'product_alert_threshold' => 'Alert Threshold <span style="font-size: 10px;">(optional)</span>',
    'product_alert_placeholder_threshold' => '0',
    'product_description' => 'Product Description',
    'product_add' => 'Add Product',
    'product_update' => 'Update Product',

    // String Translation for Product Modal

    'product_modal_header' => 'Delete Record?',
    'product_modal_content' => 'This would permanently remove the record from the system, would you like to continue?',
    'product_modal_decline' => 'No',
    'product_modal_accept' => 'Yes',

    //String translation for product.index

    'product_catalog' => 'Product Catalog',
    'product_selling_price' => 'Selling Price',
    'product_cost_price' => 'Cost Price',

    // String translation for product.receivings
    
    'product_detail' =>'Product Details',
    // 'product_receiving_search_placeholder' => 'Search product...',
    // 'product_receiving_hash' => '#',
    // 'product_receiving_item' => 'Item',
    // 'product_receiving_qty' => 'Qty',
    // 'product_receiving_cost' => 'Cost',
    // 'product_receiving_total' => 'Total',
    // 'product_receiving_cart' => 'There are no items in the cart.',
    'product_min_qantity' => 'Min Quantity',


    /*
        String Translation for /dashboard/admin/category/create
    */
    'category_header' => 'Category Information',
    'category_list_header' => 'Category List',
    'category_edit_header' => 'Edit Category',
    'category_title' => 'Category Name',
    'category_placeholder_title' => 'Phone and Tablets',
    'category_description' => 'Product Description',
    'category_add' => 'Add Category',
    'category_update' => 'Update Category',

    /*
        String Translation for /dashboard/admin/bank/create
    */

    'bank_header' => 'Bank Information',
    'bank_title' => 'Bank Name',
    'bank_placeholder_title' => 'Access Bank',
    'bank_address' => 'Address',
    'bank_sortcode' => 'Sort Code',
    'bank_sortcode_placeholder' => '42414',
    'bank_add' => 'Add Bank',

    /*
        String Translation for /dashboard/admin/bank/edit.
    */

    'bank_edit_header' => 'Edit Bank Information',
    'bank_update' => 'Update Bank',
    'bank_delete' => 'Delete Record',

    /*
        String Translation for /dashboard/admin/bank
    */

    'bank_list_header' => 'Bank List',
    'bank_created_by' => 'Created By',
    'bank_created_at' => 'Created At',

    /*
        String Translation for /dashboard/customer
    */

    'customer_header' => 'Customer List',
    'customer_title' => 'Customer Name',
    'customer_address' => 'Address:',
    'customer_mobile_number' => 'Mobile Number',
    'customer_email' => 'Customer Email',
    'customer_created_by' => 'Created By',
    'customer_created_at' => 'Created At',

    /*
        String Translation for /dashboard/customer/edit
    */

    'customer_info_header' => 'Customer Information',
    'customer_placeholder_title' => 'John Doe',
    'customer_placeholder_mobile_number' => '08161730129',
    'customer_placeholder_email' => 'me@legastro.com.ng',
    'customer_add' => 'Add Customer',
    'customer_update' => 'Update Customer',
    'customer_delete' => 'Delete Customer',

    /*
        String translation for customer confirm delete modal
    */

    'customer_modal_header' => 'Delete Record?',
    'customer_modal_content' => 'This would permanently remove the record from the system, would you like to continue?',
    'customer_modal_decline' => 'No',
    'cutomer_modal_accept' => 'Yes',

    /*
        String Translation for /dashboard/customer/create
    */

    'customer_add' => 'Add Customer',

    /*
        String Translation for /dashboard/admin/discount
    */

    'discount_header' => 'Apply Discounts',
    'discount_coupon' => 'Coupon',
    'discount_coupon_span' => '(optional)',
    'discount_validity' => 'Validity (no. of days)',
    'discount_min_qty' => 'Min. Qty',
    'discount_min_qty_span' => '(Leave empty for any amount)',
    'discount_amount' => 'Discount Amount (%)',
    'discount_type' => 'Discount Type',
    'discount_percentage' => 'Percentage [Default]',
    'discount_description' => 'Description',
    'discount_desc' => 'Please select the products to apply discounts',
    'discount_all' => 'All',
    'discount_title' => 'Title',
    'discount_category' => 'Category',
    'discount_selling_price' => 'Selling Price',
    'discount_cost_price' => 'Cost Price',
    'discount_apply' => 'Apply Discount',

    // String Translation for apply discount modal

    'discount_modal_header' => 'Confirm Submission',
    'discount_modal_description' => 'Apply Discounts',
    'discount_modal_hint' => 'Are you sure you want to apply discounts on the selected products',
    'discount_modal_cancel' => 'Cancel',
    'discount_modal_continue' => 'Continue >>',

    /*
        String Translation for admin dashboard
    */
    
    'admin_product' => 'Product',
    'admin_products' => 'Products',
    'admin_create_product' => 'Create Product',
    'admin_create_product_categories' => 'Product Categories',
    'admin_create_category' => 'Create Category',
    
    'admin_suppliers' => 'Suppliers',
    'admin_create_supplier' => 'Create Supplier',
    'admin_customer' => 'Customer',
    'admin_customer_list' => 'Customer List',
    'admin_create_customer' => 'Create Customer',
    
    'admin_pos' => 'POS',

    'admin_sales' => 'Sales',
    'admin_daily_sales_report' => 'Daily Sales Report',
    'admin_monthly_sales_report' => 'Monthly Sales Report',
    'admin_yearly_sales_report' => 'Yearly Sales Report',
    'admin_range_report' => 'Range Report',
    
    'admin_stocks' => 'Stocks',
    'admin_add_stocks' => 'Add Stocks',
    'admin_stock_history' => 'Stock History',

    'admin_users' => 'Users',
    'admin_user_list' => 'User List',
    'admin_create_user' => 'Create User',
    'admin_receiving' => 'Receivings',
    'admin_control_panel' => 'Control Panel',
    'admin_warehouse_list' => 'Warehouse List',
    'admin_create_warehouse' => 'Create Warehouse',
    'admin_bank_list' => 'Bank List',
    'admin_create_bank' => 'Create Bank',
    'admin_app_settings' => 'App Settings',
    'admin_logout' => 'Log out',

    /*
        String Translation for Cashier Dashboard
    */

    'cashier_home' => 'Home',
    'cashier_customer' => 'Customer',
    'cashier_customer_list' => 'Customer List',
    'cashier_create_customer' => 'Create Customer',
    'cashier_pos' => 'POS',
    'cashier_welcome' => 'Welcome',
    'cashier_logout' => 'Log out',
    
    
    /*
        String Translation for/dashboard/admin/return/sales
    */
    
    'return_qty' => 'Return Qty',
    'return_item' => 'Return Items To',
    

];