<?php

return [

    /*
    * String Translation for /dashboard/admin/warehouse/create
    */
    
    'warehouse_info_header' => 'Warehouse Information',
    'warehouse_name' => 'Warehouse Name',
    'warehouse_name_placeholder' => 'WAREHOUSE-002',
    // 'warehouse_address' => 'Address:',
    'warehouse_add' => 'Add Warehouse',

    /*
        String Translation for /dashboard/admin/warehouse/edit
    */

    'warehouse_edit_information_header' => 'Edit Warehouse Information',    
    'warehouse_update' => 'Update Warehouse',
    'warehouse_delete' => 'Delete Record',


    /*
        String Translation for /dashboard/admin/warehouse/
    */

    'warehouse_list_header' => 'Warehouse List',
    'warehouse_title' => 'Warehouse Title',

    /*
        string translation for /dashboard/admin/warehouse/{id}
    */
    
    'warehouse_items_header' => 'This warehouse items'


];