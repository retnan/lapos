<?php

return [
    
    /*
        String Translatiob for /dashboard/report/account
    */
    'report_account_header' => 'Account',
    'report_account_hash' => 'Account #',
    'report_account_title' => 'Account Title',
    'report_balance' => 'Balance',
    'report_previous_balance' => 'Previous Balance',
    'report_last_updated' => 'Last Updated',
    
    
    /*
        String Translation for /dashboard/report/sales
    */
    
    'sales_header' => 'Sales',
    'sales_account' => 'Account',
    'sales_naration' => 'Naration',
    'sales_user' => 'User',
    'sales_type' => 'Type',
    'sales_date' => 'Date',
    'sales_grand_total' => 'Grand Total',
    
    'to' => 'to',
    /*
        String Translation for /dashboard/admin/stocks
    */
    
    'stocks_header' => 'Stocks',
    
    'transaction_header' => 'Transactions',
    ];