<?php    

return [

    
    /*
        General String Translation for /dashboard/admin/bank/
    */

    
    'description' => 'Description:',
    'optional' => 'optional',
    'default' => 'Default',
    'category' => 'Category',
    'percentage' => 'Percentage',
    'selling_price' => 'Selling Price',
    'cost_price' => 'Cost Price',
    'title' => 'Title',
    'address' => 'Address',
    'created_by' => 'Created By',
    'created_at' => 'Created At',
    'updated_at' => 'Updated At',
    'reset' => 'Reset',
    'total' => 'Total',
    'detach' => 'Detach',
    'amount' => 'Amount',
    'add_record' => 'Add Record',
    'add_product' => 'Add Product',
    'update_record' => 'Update Record',
    'delete_record' => 'Delete Record', 
    'reciept' => 'Reciept',
    'invoice' => 'Invoice',
    'item_tiers' => 'Item Tiers',
    
    'payment_method' => 'Payment Method',
    'seller' => 'SELLER',
    'product' => 'Product',
    'date' => 'Date',
    'cost' => 'Cost',
    'sub_total' => 'Sub Total',
    'order_totals' => 'Order Totals',
    'order_subtotal' => 'Order Total',
    'total_credit' => 'Total Credit',
    'amount_paid' => 'Amount Paid',
    'total_amount_paid' => 'Total Amount Paid',
    'balance_due_for_payment' => 'Balance Due for Payment',
    'back' => 'Back',
    'print' => 'Print',
    'mark_as_paid' => 'Mark As Paid',
    'refund' => 'Refund',
    'stock' => 'STOCK',
    'store' => 'STORE',
    'hash' => '#',
    'item' => 'Item',
    'qty' => 'Qty',
    'price' => 'Price',
    'cost_price' => 'Cost Price',
    'selling_price',
    'tax' => 'Tax',
    'quantity' => 'Quantity',
    'supplier' => 'Supplier',
    'in_order' => 'in-Order:',
    'cash' => 'CASH',
    'credit' => 'CREDIT',
    /*
        General string translation for delete modal
    */
    'product_title' => 'Product Name <span style="font-size: 10px;">(Start typing)</span>',
    
    'delete_record_header' => 'Delete Record?',
    'delete_record_content' => 'This would permanently remove the record from the system, would you like to continue?',
    'delete_record_no' => 'No',
    'delete_record_yes' => 'Yes',
    
    'loader' => 'Loading, please wait...',
    'no_cart_items' => 'There are no items in the cart.',
    'items_return' => 'Itens returned from this order',
    'return_method' => 'Select Refund Method',
    'refund_total' => 'Refund Total',
    'return_items' => 'Return Items',
    'sales_return' => 'Sales Return is not available for this order',
    'total_amount_refund' => 'Total Amount Refund',
    'sales_return_reciept' => 'SALES RETURN RECEIPT',
    'customer_account_credited' => 'Customer Account Credited',
    'original_order' => 'Original Order #',
    // Footer
    'footer' => '&copy 2018 LogicalAddress Ltd. (+2348161730129, sales@logicaladdress.com)',
    'search_placeholder' => 'Search product...',
    'stock_selected' => 'Being selected means that this will add products directly into the stocks.',

    /*
        String translation for /dashboard/admin/stocks
    */
    'stock_catalog' => 'Stocks Catalog',
    // 'supplier_list' => 'Supplier List',
    'company' => 'Company',
    'email' => 'Email',

];

