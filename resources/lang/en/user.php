<?php

return [

    /*
    * String Translation for /dashboard/admin/user/create
    */
    
    'user_information_header' => 'User Information',
    'user_full_name' => 'Full Name:',
    'user_full_name_placeholder' => 'Name',
    'user_email' => 'Email',
    'user_email_placeholder' => 'Email',
    'user_password' => 'Password:',
    'user_password_placeholder' => 'Password',
    'user_access_role' => 'Access Role',
    'user_admin' => 'Admin',
    'user_cashier' => 'Cashier',
    'user_add_user' => 'Add User',

    /*
        String Translation for /dashboard/admin/user/edit
    */

    'user_edit_information_header' => 'Edit User Information',
    'user_can_login' => 'This user can login',
    'user_update' => 'Update User',
    

    /*
        String Translation for /dashboard/admin/user/
    */

    'user_list_header' => 'List of Users',
    'user_account_no' => 'Account No.',
    'user_name' => 'Name',


];