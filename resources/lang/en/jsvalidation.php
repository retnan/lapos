<?php

return [

    /*
        Semantic Javascript Validation String for /dashboard/admin/product/create
    */

    'product_title_empty' => 'Please enter the product title',
    'product_unit_cost_price_empty' => 'Please enter the unit cost price',
    'product_unit_cost_price_number' => 'Please enter a valid cost price',
    'product_unit_selling_price_empty' => 'Please enter the unit selling price',
    'product_unit_selling_price_number' => 'Please enter a valid selling price',
    

    /*
        Validation String for /dashboard/admin/category/create
    */
    'category_title_empty' => 'Please enter the Category name',
    
    /*
    *   Validation string for /dashboard/admin/bank/create
    */
    
    'bank_title_empty' => 'Please enter the Bank name',
    'bank_address_empty' => "Please enter Bank\'s address",
    
    
    /*
        Javascript Validation string translation for /dashboard/admin/customer/create
    */
    'customer_title_empty' => 'Please enter the Customer Name',
    'customer_mobile_number_empty' => 'Please enter the phone number',
    'customer_address_empty' => "Please enter the customer\'s address ",
    
    /*
        Javascript Validation string translation for /dashboard/admin/stocks/create
    */
    
    'stock_quantity_empty' => 'Please enter the quantity',
    'stock_quantity_number' => 'Please enter a vilid quantity',
    
    
    /*
        JS String Translation for /dashboard/admin/supplier/create
    */
    
    'company_title_empty' => 'Please enter the company name',
    'company_mobile_number_empty' => 'Please enter the company\'s phone number',
    'company_address' => 'Please enter the company\'s address',
    
    /*
    Js String Validation for /dashboard/admin/transfer
    */
    
    'transfer_source_empty' => 'Please select the source account',
    'transfer_destination_empty' => 'Please select the destination account',
    'transfer_amount_empty' => 'The amount can not be empty',
    'transfer_number_empty' => 'Please enter a valid amount',
    'transfer_comment_empty' => 'The comment field can not be empty',
    
    /*
        JS String translation for /dashboard/admin/user/create
    */
    
    'user_name_empty' => 'Please enter the user name',
    'user_email_empty' => 'Please enter an email address',
    'user_email_invalid' => 'Please enter a valid email address',
    'user_password_empty' => 'Please enter your password',
    'user_password_lenght' => 'Your password must be at least 6 characters',
    
    /*
        JS String translation for /dashboard/admin/warehouse/create
    */
    
    'warehouse_name_empty' => 'Please enter the Warehouse name',
    'warehouse_address' => 'Please enter the Warehouse\'s address',
    
    /*
        JS String translation for /login
    */
    
    'login_email_empty' => 'Please enter your e-mail',
    'login_email_invalid' => 'Please enter a valid e-mail',
    
    'login_password_empty' => 'Please enter your password',
];