<?php

return [

    
    /* 
        String translation for product.receivings
    */
    
    'receiving_loader' =>'Loading, please wait',
    'receiving_search_placeholder' => 'Search product...',
    // 'receiving_hash' => '#',
    // 'receiving_item' => 'Item',
    // 'receiving_qty' => 'Qty',
    'receiving_cost' => 'Cost',
    'receiving_total' => 'Total',
    'receiving_in_stock' => 'in-Stock:',
    'receiving_cart' => 'There are no items in the cart.',
    'receiving_type_supplier_placeholder' => 'Type supplier name...',

    'receiving_item_tiers' => 'Item Tiers',
    'receiving_amount' => 'Amount',
    'receiving_sub_total' => 'Sub Total',
    
    'receiving_location' => 'Location',
    'receivings_stock' => 'STOCK',
    'receiving_store' => 'STORE',
    'receiving_pick_a_store' => 'Pick a store...',
    'receiving_payment_type' => 'Payment Type',
    'receiving_cash' => 'CASH',
    'receiving_credit' => 'CREDIT',
    'receiving_submit_receiving' => 'Submit Receiving',

    /* 
        String Translation for /dashboard/admin/receivings
    */

    'receiving_modal_header' => 'Info.',
    'receiving_modal_content' => 'Please verify that the supplier is selected and that there are items in your cart.',
    'receiving_modal_stock_content' => 'The selected item is currently nit in stock',
    'receiving_action' => 'Ok',
    'receiving_stock' => 'The selected item is currently not in stock.',

];