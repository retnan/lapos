<?php    

return [

/*
*    String Translation for /dashboard/admin/bank/create
*/
    
    /*
        String Translation for /dashboard/admin/bank/create
    */

    'bank_header' => 'Bank Information',
    'bank_title' => 'Bank Name',
    'bank_placeholder_title' => 'Access Bank',
    'bank_address' => 'Address',
    'bank_sortcode' => 'Sort Code',
    'bank_sortcode_placeholder' => '42414',
    'bank_add' => 'Add Bank',

    /*
        String Translation for /dashboard/admin/bank/edit.
    */

    'bank_edit_header' => 'Edit Bank Information',
    'bank_update' => 'Update Bank',
    'bank_delete' => 'Delete Record',

    /*
        String Translation for /dashboard/admin/bank
    */

    'bank_list_header' => 'Bank List',
    'bank_created_by' => 'Created By',
    'bank_created_at' => 'Created At',


];

