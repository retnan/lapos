<?php

return [

	/*
		String translation for /login
	*/

	'landing_logo' => 'Le Gastro',
	'landing_home' => 'Home',
	'landing_logical_address' => 'Logical Address',
	
	'landing_login' => 'Login',
	'landing_log_in' => 'Log in',
    'getstarted' => 'Get Started',
	'landing_pos_header' => 'Le Gastro Hive',
	'landing_lead_content' => 'We redefine your beauty<br/>
          Call: +2348064747818<br/>
          Mail: sales@logicaladdress.com',
	
	'landing_start_demo' => 'Login',
	'landing_getstarted' => 'CLICK TO GET STARTED',

	'landing_inventory_header' => 'SALON',
	
	'landing_inventory_content' => 'Track your products from warehouse to stock to everywhere it goes and also who is responsible.',
	

	'landing_accounting_header' => 'SPA',
	
	'landing_accounting_content' => 'Track your sales, purchase, expenses and most of all your money where ever it goes.',
	
	
	'landing_social_header' => 'SOCIAl',
	
	'landing_social_content' => 'People within 20km make reservation, rate and comment on your products and locate you. ',
	
	
	'landing_subscribe_header' => 'Subscribe to Mailing List',
	
	
	'landing_subscribe_content' => 'Let us keep you abreast with how to make the most of our Beauty Salon and Spa',
	
	
	
	'landing_subscribe_placeholder' => 'Enter your email address...',
	'landing_subscribe' => 'Subscribe',
	
/*	'landing_footer_header' => '+234(0)8161730129 <br/>nHub Nigeria, 3rd Floor TAEN Business Complex, Opposite NITEL @ Old Airport Junction, Jos, Nigeria',*/
	
	'landing_footer_header' => '+2348064747818 <br/>No. 4 Kolda street <br/>off Ademola Adetokunbo street<br/> wuse 2. Abuja',
	
	
	'landing_partners_header' => 'Partners',
	'nhubnigeria' => 'nHub Nigeria',
	'yin' => 'Young Innovators of Nigeria',

    /*
        String translation for /login
    */
    'login_email_placeholder' => 'Your E-mail',
    'login_password_placeholder' => 'Your Password',
    
    /*'login_message' => 'Email: demo@lapos.ng, Password: demo'*/
    'login_message' => 'Email: admin@legastro.com.ng, Password: admin'
];
