<?php

return [

    /*
    * String Translation for config
    */
    
    'config_app_setting' => 'App Settings',
    'config_company_name' => 'Company Name:',
    'config_email_address' => 'Email Address:',
    'config_mobile_number' => 'Mobile Number:',
    'config_currency_name' => 'Currency Name:',
    'config_currency_symbol' => 'Currency Symbol:',
    'config_company_address' => 'Company Address:',
    'config_update' => 'Update',

    /*
        String Translation for config_placeholder
    */
    'config_company_name_placeholder' => 'nHub Nigeria',
    'config_email_address_placeholder' => 'd.retnan@nhubnigeria.com',
    'config_mobile_number_placeholder' => '+2348161730129',
    'config_currency_name_placeholder' => 'Naira',
    'config_currency_symbol_placeholder' => '₦',
];