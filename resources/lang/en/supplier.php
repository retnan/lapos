<?php

return [

    /*
    * String Translation for /dashboard/admin/supplier/create
    */
    'supplier_header' => 'Supplier Information',
    'supplier_company_name' => 'Company Name:',
    'supplier_company_name_placeholder' => 'Chris Best',
    // 'supplier_address' => 'Address:',
    'supplier_mobile_number' => 'Mobile Number:',
    'supplier_mobile_number_placeholder' => '08161730129',
    'add_supplier' => 'Add Supplier',
    'update_supplier' => 'Update Supplier',
    'delete_supplier' => 'Delete Record',

    'supplier_list_header' => 'Supplier List',
    'supplier_detial_header' => 'Supplier Details',
    'supplier_company' => 'Company',
    'supplier_account_balance' => 'Account Balance',
    'supplier_previous_balance' => 'Previous Balance',
    
    'supplier_recent_purchase_order' => 'Recent Purchase Order',
    'supplier_order_hash' => 'Order #',
    'supplier_receipt' => 'Receipt #',
    'supplier_payment_type' => 'Payment Type'


];