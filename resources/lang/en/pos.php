<?php

return [

    /*
    * String Translation for /dashboard/pos
    */

    'pos_loader' => 'Loading, please wait...',
    'pos_search_placeholder' => 'Search product...',
    'pos_no_items' => 'There are no items in the cart.',
    'pos_customer_search_placeholder' => 'Type customer name...',
    'pos_in_stock' => 'in-Stock',
    
    'pos_item_tiers' => 'Item Tiers',
    'pos_sub_total' => 'Sub Total',
    'pos_sales_tax' => 'Sales Tax',
    'pos_total_credit' => 'Total Credit',
    'pos_payment_type' => 'Payment Type',
    'pos_cash' => 'CASH',
    'pos_credit' => 'CREDIT',
    'pos_deposit' => 'Deposit',
    'pos_submit_order' => 'Submit Order',
    
    'pos_info_header' => 'Info.',
    'pos_info_content' => 'Please verify that the customer is selected and that there are items in your cart.',
    'pos_ok' => 'Ok',
    'pos_not_in_stock' => 'The selected item is currently not in stock.',

    /*
	*	String Translation for /dashboard/pos/receipt
    */

    'reciept_header' => 'Reciept',
    'reciept_reciept' => 'Reciept #',
    'reciept_order' => 'Order #',
    'reciept_date' => 'Reciept Date:',
    'receipt_payment_method' => 'Payment Method',
    'receipt_refund_method' => 'Refund Method',
    'receipt_sold_to' => 'SOLD TO',
    'receipt_item_return' => 'Return Items',
    // 'reciept_order_total' => 'Order Subtotal:',
    'reciept_sales_tax' => 'Sales Tax:',
    // 'receipt_amount_paid' => 'Amount Paid',
    // 'receipt_total_amount_paid' => 'Total Amount Paid:',
    // 'reciept_due_for_payment' => 'Balance Due for Payment:',

    // 'back' => 'Back',
    // 'print' => 'Print',
];