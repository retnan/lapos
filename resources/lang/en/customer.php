<?php

return [
    
    /*
        String translation for /dashbaord/customer
    */
    
    'customer_title' => 'Customer',
    
    /*
        String Translation for /dashboar/customer/{id}
    */
    
    'customer_details_header' => 'Customer Details',
    'customer_account_balance' => 'Account Balance',
    'customer_previous_balance' => 'Previous Balance',
    'customer_recent_sales_order' => 'Recent Sales Order',
    
    'customer_hash' => '#',
    'customer_order' => 'Order #',
    'customer_reciept' => 'Reciept #',
    'customer_payment_type' => 'Payment Type',
    
    
    ];
    