<?php

return [
    
    /*
        String Transation for /dashboard/admin/transfer
    */
    
    'transfer_header' => 'Transfers',
    'transfer_source_account' => 'Source Account',
    'transfer_source_account_placeholder' => 'Pick an account...',
    'transfer_destination_account' => 'Destination Account',
    'transfer_comment' => 'Comment',
    'transfer' => 'Transfer',
    
    'transfer_history_header' => 'Transfer History',
    ];
    