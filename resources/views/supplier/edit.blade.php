@extends(Auth::user()->access_role == "admin" ? 'layouts.admin_dashboard' : (Auth::user()->access_role == "cashier" ? 'layouts.cashier_dashboard' : 'layouts.default'))

@section('scripts')
  <script type="text/javascript" src="/uilib/semantic.min.js"></script>
  <script>
  $(document)
    .ready(function() {
      $('.ui.form')
        .form({
          fields: {
            title: {
              identifier  : 'title',
              rules: [
                {
                  type   : 'empty',
                  prompt : "@lang('jsvalidation.company_title_empty')"
                },
              ]
            },
            mobile_number: {
              identifier  : 'mobile_number',
              rules: [
                {
                  type   : 'empty',
                  prompt : "@lang('jsvalidation.company_mobile_number_empty')"
                },
              ]
            },
            address: {
              identifier  : 'address',
              rules: [
                {
                  type   : 'empty',
                  prompt : "@lang('jsvalidation.company_address')"
                },
              ]
            },
          }
        })
      ;
    })
  ;
  </script>
@stop

@section('stylesheets')
  <link rel="stylesheet" type="text/css" href="/uilib/semantic.min.css"/>
@stop

@section('content')
  {!! Breadcrumbs::render('supplier_edit', $data) !!}
    
   {!! Form::open(array('url' => '/dashboard/admin/supplier/' . $data->id, 'method'=>'PUT', 'class'=>'ui small equal width form')) !!}
      <h4 class="ui dividing header">@lang('supplier.supplier_header')</h4>
      
      @if (count($errors) > 0)
        <div class="ui message" style="color:#9F3A38;font-size: 1em;box-shadow: 0px 0px 0px 1px #E0B4B4 inset, 0px 0px 0px 0px transparent; background-color: #FFF6F6;">
            <ul class="list">
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
      @endif


      <div class="fields">
        <div class="required field">
          <label>@lang('supplier.supplier_company_name')</label>
          <input value="{{ old('title') ? old('title') : $data->title }}" name="title" placeholder="@lang('supplier.supplier_company_name_placeholder')" type="text">
        </div>
      </div>
      
      <div class="fields">
        <div class="required field">
          <label>@lang('general.address')</label>
          <textarea name="address" type="text">{{ old('address') ? old('address') : $data->address }}</textarea>
        </div>
      </div>
      
      <div class="fields">
        <div class="required field">
          <label>@lang('supplier.supplier_mobile_number')</label>
          <input value="{{ old('mobile_number') ? old('mobile_number') : $data->mobile_number }}" name="mobile_number" placeholder="@lang('supplier.supplier_mobile_number_placeholder')" type="text">
        </div>
      </div>
      
    <span><input type="submit" name="submit" class="ui primary button" value="@lang('supplier.update_supplier')"></span>
    <span><a href="#" id="delete_record" class="ui red disabled button">@lang('general.delete_record')</a></span>
    <div class="ui error message"> </div>
    {!! Form::close() !!}
    
    <script type="text/javascript">
    $( document ).ready(function() {
		  $('.ui.radio.checkbox').checkbox();
    });
    </script>
@stop