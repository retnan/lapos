@extends(Auth::user()->access_role == "admin" ? 'layouts.admin_dashboard' : (Auth::user()->access_role == "cashier" ? 'layouts.cashier_dashboard' : 'layouts.default'))


@section('scripts')
  <script type="text/javascript" src="/js/jquery.dataTables.min.js"></script>
  <script type="text/javascript" src="/js/dataTables.semanticui.min.js"></script>
  <script type="text/javascript" src="/uilib/semantic.min.js"></script>
@stop

@section('stylesheets')
  <link rel="stylesheet" type="text/css" href="/uilib/semantic.min.css"/>
  <link rel="stylesheet" type="text/css" href="/css/dataTables.semanticui.min.css"/>
@stop

@section('content')
    {!! Breadcrumbs::render('supplier_show', $data) !!}
    <h4 class="ui dividing header">@lang('supplier.supplier_detial_header')</h4>
    <table class="ui celled striped table">
      <thead>
        <tr>
          <th colspan="3">{{ $data->title}}</th>
        </tr>
      </thead>
    <tbody>
      <tr>
        <td class="collapsing">@lang('general.address')</td>
        <td>{{ $data->address}}</td>
      </tr>
      <tr>
        <td class="collapsing">@lang('supplier.supplier_mobile_number')</td>
        <td>{{ $data->mobile_number}}</td>
      </tr>
      <tr>
        <td class="collapsing">@lang('supplier.supplier_account_balance')</td>
        <td>{{html_entity_decode($AppConfig->currency_symbol, 0, 'UTF-8')}} {{ number_format($data->balance, 2)}}</td>
      </tr>
      <tr>
        <td class="collapsing">@lang('supplier.supplier_previous_balance')</td>
        <td>{{html_entity_decode($AppConfig->currency_symbol, 0, 'UTF-8')}} {{ number_format($data->prev_balance, 2)}}</td>
      </tr>
    </tbody>
  </table>

  <h4 class="ui dividing header">@lang('supplier.supplier_recent_purchase_order')</h4>
  <table class="ui celled striped table">
      <thead>
        <th>@lang('general.hash')</th>
        <th>@lang('supplier.supplier_order_hash')</th>
        <th>@lang('supplier.supplier_receipt')</th>
        <th>@lang('supplier.supplier_payment_type')</th>
        <th>@lang('general.amount')({{html_entity_decode($AppConfig->currency_symbol, 0, 'UTF-8')}})</th>
        <th>@lang('general.created_at')</th>
        <th>@lang('general.updated_at')</th>
      </thead>
      </tbody>
      @foreach ($data->purchase_orders as $item)
        <tr>
          <td class="selectable blue collapsing">
          <a href="{{ '/dashboard/purchase/' . $item->receipt_number }}"><i class="blue browser icon"></i></a>
          </td>
          <td>{{ $item->id}}</td>
          <td><a href="{{ '/dashboard/purchase/' . $item->receipt_number }}">{{ $item->receipt_number}}</a></td>
          <td>{{ $item->payment_type}}</td>
          <td>{{number_format($item->total_amount, 2)}}</td>
          <td>{{ $item->created_at}}</td>
          <td>{{ $item->updated_at}}</td>
        </tr>
        @endforeach
        </tbody>
    </table>
       
    <script type="text/javascript">
    $( document ).ready(function() {
          
    });
    </script>
@stop