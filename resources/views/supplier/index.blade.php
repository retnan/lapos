@extends(Auth::user()->access_role == "admin" ? 'layouts.admin_dashboard' : (Auth::user()->access_role == "cashier" ? 'layouts.cashier_dashboard' : 'layouts.default'))

@section('scripts')
  <script type="text/javascript" src="/js/jquery.dataTables.min.js"></script>
  <script type="text/javascript" src="/js/dataTables.semanticui.min.js"></script>
  <script type="text/javascript" src="/uilib/semantic.min.js"></script>
@stop

@section('stylesheets')
  <link rel="stylesheet" type="text/css" href="/uilib/semantic.min.css"/>
  <link rel="stylesheet" type="text/css" href="/css/dataTables.semanticui.min.css"/>
@stop

@section('content')
    {!! Breadcrumbs::render('supplier_list') !!}
    
    <h4 class="ui dividing header">@lang('supplier.supplier_list_header')</h4>
    
    <table class="ui small celled table display" id="dtable">
      <thead>
        <th></th> 
        <th></th>
        <th>@lang('general.company')</th>
        <th>@lang('general.address')</th>
        <th>@lang('supplier.supplier_mobile_number')</th>
        <th>@lang('general.created_by')</th>
        <th>@lang('general.created_at')</th>
        <!-- <th>Updated At</th> -->
      </thead>
      <tbody>
          
        @foreach ($data as $item)
            <tr>
              <td class="selectable blue collapsing">
                <a href="{{ '/dashboard/admin/supplier/' . $item->id }}"><i class="blue browser icon"></i></a>
              </td>
              <td class="selectable positive collapsing">
                <a href="{{ '/dashboard/admin/supplier/' . $item->id . '/edit' }}"><i class="green write icon"></i></a>
              </td>
              <td><!--<a href="/user/{{ $item->id}}">-->{{ $item->title}}<!--</a>--></td>
              <td>{{ $item->address}}</td>
              <td>{{ $item->mobile_number}}</td>
              <td>{{ $item->user->name}}</td>
              <td>{{ $item->created_at}}</td>
              <!-- <td>{{ $item->updated_at}}</td> -->
            </tr>
        @endforeach
      </tbody>
    </table>
    
    <script type="text/javascript">
    $( document ).ready(function() {
		  $('#dtable').DataTable();
    });
    </script>
@stop