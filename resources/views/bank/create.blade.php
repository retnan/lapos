@extends('layouts.admin_dashboard')

@section('scripts')
  <script type="text/javascript" src="/uilib/semantic.min.js"></script>
  <script>
  $(document)
    .ready(function() {
      $('.ui.form')
        .form({
          fields: {
            title: {
              identifier  : 'title',
              rules: [
                {
                  type   : 'empty',
                  prompt : "@lang('jsvalidation.bank_title_empty')"
                },
              ]
            },
            // sort_code: {
            //   identifier  : 'sort_code',
            //   rules: [
            //     {
            //       type   : 'empty',
            //       prompt : 'Please enter the bank sort number'
            //     },
            //   ]
            // },
            address: {
              identifier  : 'address',
              rules: [
                {
                  type   : 'empty',
                  prompt : "@lang('jsvalidation.bank_address_empty')"
                },
              ]
            },
          }
        })
      ;
    })
  ;
  </script>
@stop

@section('stylesheets')
  <link rel="stylesheet" type="text/css" href="/uilib/semantic.min.css"/>
@stop

@section('content')
    {!! Breadcrumbs::render('bank_create') !!}
   {!! Form::open(array('url' => '/dashboard/admin/bank', 'method'=>'POST', 'class'=>'ui small equal width form')) !!}
      <h4 class="ui dividing header">@lang('bank.bank_header')</h4>
      
    @if (count($errors) > 0)
        <div class="ui message" style="color:#9F3A38;font-size: 1em;box-shadow: 0px 0px 0px 1px #E0B4B4 inset, 0px 0px 0px 0px transparent; background-color: #FFF6F6;">
            <ul class="list">
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif
    
      <div class="fields">
        <div class="required field">
          <label>@lang('bank.bank_title')</label>
          <input name="title" placeholder="@lang('bank.bank_placeholder_title')" type="text">
        </div>
      </div>
      
      <div class="fields">
        <div class="required field">
          <label>@lang('bank.bank_address'):</label>
          <textarea name="address" type="text"></textarea>
        </div>
      </div>
      
      <div class="fields">
        <div class="field">
          <label>@lang('bank.bank_sortcode')</label>
          <input name="sort_code" placeholder="@lang('bank.bank_sortcode_placeholder')" type="text">
        </div>
      </div>
      
    <span><input type="submit" name="submit" class="ui primary button" value="@lang('bank.bank_add')"></span>
    <div class="ui error message"> </div>
    {!! Form::close() !!}
    
    <script type="text/javascript">
    $( document ).ready(function() {
		  $('.ui.radio.checkbox').checkbox();
    });
    </script>
@stop