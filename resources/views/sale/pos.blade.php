@extends(Auth::user()->access_role == "admin" ? 'layouts.admin_dashboard' : (Auth::user()->access_role == "cashier" ? 'layouts.cashier_dashboard' : 'layouts.default'))

@section('scripts')
	<script type="text/javascript">
  		var App = {
  			Currency: "{{$AppConfig->currency_symbol}}",
  		};
  	</script>
  <!-- <script type="text/javascript" src="/uilib/semantic.min.js"></script> -->
  <script type="text/javascript" src="/js/semanticAjaxHack.js"></script>
  <script type="text/javascript" src="/js/knockout-3.4.0.js"></script>
  <script type="text/javascript" src="/js/accounting.min.js"></script>
  <script type="text/javascript" src="/js/shopping_cart.js"></script>
@stop

@section('stylesheets')
  <link rel="stylesheet" type="text/css" href="/uilib/semantic.min.css"/>
  <style type="text/css">
  .product_details{
  	font-style: italic;
  	font-size: 11px;
  	font-weight: bold;
  	color: green;
  }
   .paper-cut:after {
	  content: " ";
	  display: block;
	  position: relative;
	  top: 0px;
	  left: 0px;
	  width: 100%;
	  height: 36px;
	  background: -webkit-linear-gradient(#FFFFFF 0%, transparent 0%), -webkit-linear-gradient(135deg, #e8ebf1 33.33%, transparent 33.33%) 0 0%, #e8ebf1 -webkit-linear-gradient(45deg, #e8ebf1 33.33%, #FFFFFF 33.33%) 0 0%;
	  background: -o-linear-gradient(#FFFFFF 0%, transparent 0%), -o-linear-gradient(135deg, #e8ebf1 33.33%, transparent 33.33%) 0 0%, #e8ebf1 -o-linear-gradient(45deg, #e8ebf1 33.33%, #FFFFFF 33.33%) 0 0%;
	  background: -moz-linear-gradient(#FFFFFF 0%, transparent 0%), -moz-linear-gradient(135deg, #e8ebf1 33.33%, transparent 33.33%) 0 0%, #e8ebf1 -moz-linear-gradient(45deg, #e8ebf1 33.33%, #FFFFFF 33.33%) 0 0%;
	  background-repeat: repeat-x;
	  background-size: 0px 100%, 14px 27px, 14px 27px;
}
  </style>
@stop

@section('content')
	{!! Breadcrumbs::render('pos', Auth::user()->access_role) !!}

	@if (count($errors) > 0)
        <div class="ui message" style="color:#9F3A38;font-size: 1em;box-shadow: 0px 0px 0px 1px #E0B4B4 inset, 0px 0px 0px 0px transparent; background-color: #FFF6F6;">
            <ul class="list">
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif

    <div id="loader" class="ui page inverted dimmer">
      <div class="content">
        <div class="center">
            <div class="ui active text loader">@lang('pos.pos_loader')</div>
        </div>
      </div>
    </div>

    <br/>


	<div class="column">
		<div class="ui two column grid">
			<div class="ten wide column">
				<form id="shopping_cart" class="ui small equal width form">
				<!-- <div class="fields"> -->
				    <div class="field">
				      <!-- <label>Product Name <span style="font-size: 10px;">(Start typing)</span></label> -->
				      <div id="product_search" class="ui fluid category search">
				          <div class="ui icon large input">
				            <input id="product_input" class="prompt" type="text" placeholder="@lang('pos.pos_search_placeholder')" class="width">
				            <i class="search icon"></i>
				          </div>
				          <div class="results"></div>
				      </div>
				    </div>
				<!-- </div> -->
				</form>
				<div class="paper-cut">
				<div class="ui segment">
				<table class="ui very basic center aligned table">
				<thead>
					<th>@lang('general.hash')</th>
					<th>@lang('general.item')</th>
					<th>@lang('general.qty')</th>
					<th>@lang('general.price')</th>
					@if($AppConfig->supports_tax)
					<th>@lang('general.tax')</th>
					@endif
					<!-- <th>Discount</th> -->
					<th>@lang('general.total')</th>
				</thead>
				<tbody data-bind='visible: lines().length > 0, foreach: lines'>
					<tr>
						<td class="selectable positive collapsing">
				            <a href="#" data-bind='click: $parent.removeLine'><i class="green delete icon"></i></a>
				        </td>
						<td><span data-bind='text: title' > </span>
						<div class="product_details">@lang('pos.pos_in_stock') <span data-bind='text: maxQtyAllowed'></span></div></td>
						<td>
						<div class="ui small input">
							<input style="width: 50px;" data-bind='textInput: quantity' type="text"/>
						</div>
						</td>
						<td>{{$AppConfig->currency_symbol}}<span data-bind='text: accounting.formatMoney(unit_selling_price,"")' > </span></td>
						@if($AppConfig->supports_tax)
						<td><span data-bind='text: tax' ></span>%</td>
						@endif
						<!-- <td><span data-bind='text: discount' ></span>%</td> -->
						<td>{{$AppConfig->currency_symbol}}<span data-bind='text: accounting.formatMoney(subtotal(),"")' > </span></td>
					</tr>
				</tbody>
				<tbody data-bind='visible: lines().length == 0'>
					<tr>
						<td colspan="6">@lang('pos.pos_no_items')</td>
					</tr>
				</tbody>
				</table>
				</div>
				</div>
			</div>
			<div class="six wide column">
				<div class="ui segment">

				<!-- <div class="column" style="margin-bottom: 10px;">
					<button data-bind="enable: lines().length > 0, click: resetCart" class="ui tiny orange button">Reset  Cart</button>
					<button data-bind="enable: showCustomer, click: removeCustomer" class="ui tiny teal button">Detach Customer</button>
				</div> -->

				<div class="ui centered grid" style="margin: 10px;">
				<div class="ui buttons">
					<button data-bind="enable: lines().length > 0, click: resetCart" class="ui mini orange button">@lang('general.reset')</button>
			      	<div class="or"></div>
			      	<button data-bind="enable: showCustomer, click: removeCustomer" class="ui mini teal button">@lang('general.detach')</button>
			    </div>
				</div>

				<div class="column" data-bind="ifnot: showCustomer">
					<form class="ui small equal width form">
				    <div class="field">
				      <div id="customer_search" class="ui fluid search">
				          <div class="ui left icon large input">
				            <input class="prompt" type="text" placeholder="@lang('pos.pos_customer_search_placeholder')" class="width">
				            <i class="search icon"></i>
				          </div>
				          <div class="results"></div>
				      </div>
				    </div>
					</form>
				<br/>
				</div>

				<div data-bind="visible: showCustomer">
					<div class="ui divider"></div>
						<div class="column">
							<div class="ui two column grid">
								<div class="four wide column">
									<img data-bind='visible: customerName' src="/images/johndoe.jpg" class="ui medium circular left aligned image">
								</div>
								<div class="twelve wide column">
									<b data-bind='text: customerName'></b> <br/>
									<address data-bind='text: customerAddress'></address>
									<address data-bind='text: customerMobile'></address>
								</div>
							</div>
					</div>
					<div class="ui divider"></div>
					<br/>
				</div>

				<div class="row">
					<table class="ui very basic table">
						<thead>
							<th>@lang('pos.pos_item_tiers')</th>
							<th>@lang('general.amount')</th>
						</thead>
						<tbody>
							<tr>
								<td>@lang('pos.pos_sub_total')</td>
								<td>{{$AppConfig->currency_symbol}}<span data-bind='text: accounting.formatMoney(grandSubTotal(),"")'></span></td>
							</tr>
							@if($AppConfig->supports_tax)
							<tr>
								<td>@lang('pos.pos_sales_tax')</td>
								<td>{{$AppConfig->currency_symbol}}<span data-bind='text: accounting.formatMoney(grandTaxAddition(), "")'></span></td>
							</tr>
							@endif
							<tr data-bind="visible: getDiscount() > 0">
								<td>Discount</td>
								<td>{{$AppConfig->currency_symbol}}<span data-bind='text: getDiscount()'></span></td>
							</tr>
						</tbody>
					</table>
				</div>

				<div class="ui small center statistics">
					<div class="statistic">
				      	<div class="value">
				      		{{$AppConfig->currency_symbol}}<span data-bind="style: { 'text-decoration': deposit_amount() > 0 ? 'line-through' : 'none'}, text: accounting.formatMoney(grandTotalAfterDiscount(), '')"> </span>
				      	</div>
				      	<div class="label">
				      		@lang('general.total')
				      	</div>
				    </div>
			    </div>

			    <div class="ui mini center statistics" data-bind="visible: deposit_amount() > 0">
					<div class="statistic">
				      	<div class="value">
				      		{{$AppConfig->currency_symbol}}<span data-bind='text: accounting.formatMoney(grandTotalAfterDiscount() - deposit_amount(), "")'> </span>
				      	</div>
				      	<div class="label">
				      		@lang('pos.pos_total_credit')
				      	</div>
				    </div>
			    </div>

				<div class="row">
					<form class="ui small equal width form">
				    <div class="field">
				      <div class="ui right action input">
				      	<input type="text" name="coupon" id="coupon" placeholder="Discount Code">
				      	<div data-bind="click: applyDiscountCode" class="ui teal button">
				        	Apply
				      	</div>
				    </div>
				    </div>
					</form>
			    </div> 

			    <div class="ui red segment" style="padding-left: 0px !important;">
				    <h5 class="ui horizontal divider">@lang('pos.pos_payment_type')</h5>
				    <span data-bind="visible: false, text: payment_type"></span>
				    <div class="ui centered grid" style="margin: 0px;">
					    <div class="ui buttons" style="padding-left: 5px;">
							<button id="cash" data-bind="click: tooglePaytypeCash" class="ui tiny positive button">
							<i class="icon bitcoin"></i>
							@lang('pos.pos_cash')</button>
							<button id="transfer" data-bind="click: tooglePaytypeTransfer"  class="ui tiny button">
							<i class="paper plane outline icon"></i>
							USSD
							</button>
							<button id="debt"  data-bind="click: tooglePaytypeCredit" class="ui tiny button">
							<i class="icon money"></i>
							</button>
					</div>
					<div style="margin-top: 10px;" data-bind="visible: payment_type() == 'CREDIT' && lines().length > 0 && showCustomer()">
						{!! Form::open(array('url' => '#', 'method'=>'GET', 'id'=>'deposit')) !!}
				        <div class="ui labeled right action input">
				          <a class="ui basic label">{{$AppConfig->currency_symbol}}</a>
					      <input class="mini ui input" placeholder="@lang('general.amount')" type="text" id="cash_at_hand" name="cash_at_hand">
					      <div class="mini ui basic button" data-bind="click: setDeposit">
					        <i class="add icon"></i>@lang('pos.pos_deposit')
					      </div>
					    </div>
				        {!! Form::close() !!}
				    </div>
					</div>
				</div>



			    <div class="field">
			    	<button data-bind="enable: showCustomer() && lines().length > 0, click: save" class="ui blue right floated button">@lang('pos.pos_submit_order') </button>
			    	<div style="clear:right;"></div>
			    </div>

				</div>
			</div>
		</div>
    </div>

    <div id="validation" class="ui small modal">
    	<i class="close icon"></i>
    	<div class="header">@lang('pos.pos_info_header').</div>
		<div class="content">
			<p>@lang('pos.pos_info_content')</p>
  		</div>
  		<div class="actions">
    		<div class="ui negative button">@lang('pos.pos_ok')</div>
  		</div>
	</div>

	<div id="qtyExceeded" class="ui small modal">
    	<i class="close icon"></i>
    	<div class="header">@lang('pos.pos_info_header')</div>
		<div class="content">
			<p>@lang('pos.pos_info_content')</p>
  		</div>
  		<div class="actions">
    		<div class="ui negative button">@lang('pos.pos_ok')</div>
  		</div>
	</div>

	<div id="notInStock" class="ui small modal">
    	<i class="close icon"></i>
    	<div class="header">@lang('pos.pos_info_header')</div>
		<div class="content">
			<p>@lang('pos.pos_not_in_stock')</p>
  		</div>
  		<div class="actions">
    		<div class="ui negative button">@lang('pos.pos_ok')</div>
  		</div>
	</div>
@stop