@extends('layouts.admin_dashboard')

@section('scripts')
  <script type="text/javascript" src="/uilib/semantic.min.js"></script>
  <script>
  $(document)
    .ready(function() {
      $('.ui.form')
        .form({
          fields: {
            company_name: {
              identifier  : 'company_name',
              rules: [
                {
                  type   : 'empty',
                  prompt : "@lang('jsvalidation.company_title_empty')"
                },
              ]
            },
          }
        })
      ;
    })
  ;
  </script>
@stop

@section('stylesheets')
  <link rel="stylesheet" type="text/css" href="/uilib/semantic.min.css"/>
@stop

@section('content')
   {!! Breadcrumbs::render('config_list') !!}
   {!! Form::open(array('url' => '/dashboard/admin/config/1', 'method'=>'PUT', 'class'=>'ui small equal width form')) !!}
      <h4 class="ui dividing header">@lang('config.config_app_setting')</h4>

      @if (count($errors) > 0)
        <div class="ui message" style="color:#9F3A38;font-size: 1em;box-shadow: 0px 0px 0px 1px #E0B4B4 inset, 0px 0px 0px 0px transparent; background-color: #FFF6F6;">
            <ul class="list">
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
      @endif
      
      <div class="fields">
        <div class="required field">
          <label>@lang('config.config_company_name')</label>
          <input name="company_name" value="{{ $config->company_name}}" disabled="disabled" placeholder="@lang('config.config_company_name_placeholder')" type="text">
        </div>
      </div>

      <div class="fields">
        <div class="field">
          <label>@lang('config.config_email_address')</label>
          <input name="email" value="{{ $config->email }}" disabled="disabled" placeholder="@lang('config.config_email_address_placeholder')" type="text">
        </div>
      </div>
      
      <div class="fields">
        <div class="required field">
          <label>@lang('config.config_mobile_number')</label>
          <input name="mobile_number" value="{{ $config->mobile_number }}" disabled="disabled" placeholder="@lang('config.config_mobile_number_placeholder')" type="text">
        </div> 
      </div>

      <div class="fields">
        <div class="required field">
          <label>@lang('config.config_currency_name')</label>
          <input name="currency_name" value="{{ $config->currency_name }}" disabled="disabled" placeholder="@lang('config.config_currency_name_placeholder')" type="text">
        </div>
      </div>

      <div class="fields">
        <div class="required field">
          <label>@lang('config.config_currency_symbol')</label>
          <input name="currency_symbol" value="{{ $config->currency_symbol }}" disabled="disabled" placeholder="@lang('config.config_currency_symbol_placeholder')" type="text">
        </div>
      </div>

      <div class="fields">
        <div class="field">
          <label>@lang('config.config_company_address')</label>
          <textarea name="description" disabled="disabled" type="text">{{ $config->address }}</textarea>
        </div>
      </div>
      
    <span><input type="submit" name="submit" class="ui primary button disabled" value="@lang('config.config_update')"></span>
    <div class="ui error message"> </div>
    {!! Form::close() !!}
    
    <script type="text/javascript">
    $( document ).ready(function() {
		  $('.ui.radio.checkbox').checkbox();
    });
    </script>
@stop