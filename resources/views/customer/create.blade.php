@extends(Auth::user()->access_role == "admin" ? 'layouts.admin_dashboard' : (Auth::user()->access_role == "cashier" ? 'layouts.cashier_dashboard' : 'layouts.default'))

@section('scripts')
  <script type="text/javascript" src="/uilib/semantic.min.js"></script>
  <script>
  $(document)
    .ready(function() {
      $('.ui.form')
        .form({
          fields: {
            title: {
              identifier  : 'title',
              rules: [
                {
                  type   : 'empty',
                  prompt : "@lang('jsvalidation.customer_title_empty')"
                },
              ]
            },
            mobile_number: {
              identifier  : 'mobile_number',
              rules: [
                {
                  type   : 'empty',
                  prompt : "@lang('jsvalidation.customer_mobile_number_empty')"
                },
              ]
            },
          }
        })
      ;
    })
  ;
  </script>
@stop

@section('stylesheets')
  <link rel="stylesheet" type="text/css" href="/uilib/semantic.min.css"/>
@stop

@section('content')
    {!! Breadcrumbs::render('customer_create', Auth::user()->access_role) !!}
   {!! Form::open(array('url' => '/dashboard/customer', 'method'=>'POST', 'class'=>'ui small equal width form')) !!}
      <h4 class="ui dividing header">@lang('form.customer_info_header')</h4>

      @if (count($errors) > 0)
        <div class="ui message" style="color:#9F3A38;font-size: 1em;box-shadow: 0px 0px 0px 1px #E0B4B4 inset, 0px 0px 0px 0px transparent; background-color: #FFF6F6;">
            <ul class="list">
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
      @endif
      
      <div class="fields">
        <div class="required field">
          <label>@lang('form.customer_title')</label>
          <input name="title" placeholder="@lang('form.customer_placeholder_title')" type="text">
        </div>
      </div>
      
      <div class="fields">
        <div class="field">
          <label>@lang('form.customer_address')</label>
          <textarea name="address" type="text"></textarea>
        </div>
      </div>
      
      <div class="fields">
        <div class="required field">
          <label>@lang('form.customer_mobile_number')</label>
          <input name="mobile_number" placeholder="@lang('form.customer_placeholder_mobile_number')" type="text">
        </div>
      </div>

      <div class="fields">
        <div class="field">
          <label>@lang('form.customer_email')</label>
          <input name="email" placeholder="@lang('form.customer_placeholder_email')" type="text">
        </div>
      </div>
      
    <span><input type="submit" name="submit" class="ui primary button" value="@lang('form.customer_add')"></span>
    <div class="ui error message"> </div>
    {!! Form::close() !!}
    
    <script type="text/javascript">
    $( document ).ready(function() {
		  $('.ui.radio.checkbox').checkbox();
    });
    </script>
@stop