@extends(Auth::user()->access_role == "admin" ? 'layouts.admin_dashboard' : (Auth::user()->access_role == "cashier" ? 'layouts.cashier_dashboard' : 'layouts.default'))

@section('scripts')
  <script type="text/javascript" src="/uilib/semantic.min.js"></script>
  <script>
  $(document)
    .ready(function() {
      $('.ui.form')
        .form({
          fields: {
            title: {
              identifier  : 'title',
              rules: [
                {
                  type   : 'empty',
                  prompt : "@lang('jsvalidation.category_title_empty')"
                },
              ]
            },
          }
        })
      ;
    })
  ;
  </script>
@stop

@section('stylesheets')
  <link rel="stylesheet" type="text/css" href="/uilib/semantic.min.css"/>
@stop

@section('content')
   {!! Breadcrumbs::render('category_edit', $data) !!}
   {!! Form::open(array('url' => '/dashboard/admin/category/' . $data->id, 'method'=>'PUT', 'class'=>'ui small equal width form')) !!}
      <h4 class="ui dividing header">@lang('form.category_edit_header')</h4>

      @if (count($errors) > 0)
        <div class="ui message" style="color:#9F3A38;font-size: 1em;box-shadow: 0px 0px 0px 1px #E0B4B4 inset, 0px 0px 0px 0px transparent; background-color: #FFF6F6;">
            <ul class="list">
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
      @endif
      
      <div class="fields">
        <div class="required field">
          <label>@lang('form.category_title')</label>
          <input value="{{ old('title') ? old('title') : $data->title }}" name="title" placeholder="@lang('form.category_placeholder_title')" type="text">
        </div>
      </div>
      
      <div class="fields">
        <div class="field">
          <label>@lang('general.description')</label>
          <textarea name="description" type="text">{{ old('description') ? old('description') : $data->description }}</textarea>
        </div>
      </div>
      
    <span><input type="submit" name="submit" class="ui primary button" value="@lang('form.category_update')"></span>
    <span><a href="/dashboard/admin/category/{{$data->id}}/delete" id="delete_record" class="ui red button">@lang('general.delete_record')</a></span>
    <div class="ui error message"> </div>
    {!! Form::close() !!}

    <div class="ui small modal">
  <i class="close icon"></i>
  <div class="header">
    @lang('general.delete_record_header')
  </div>
  <div class="content">
    <p>@lang('general.delete_record_content')</p>
  </div>
  <div class="actions">
    <div class="ui negative button">
        @lang('general.delete_record_no')
    </div>
    <a href="/dashboard/admin/category/{{$data->id}}/delete" class="ui positive button">
        <i class="checkmark icon"></i>
        @lang('general.delete_record_yes')
      </a>
  </div>
</div>
    
    <script type="text/javascript">
    $( document ).ready(function() {
		  $('.ui.radio.checkbox').checkbox();
      $('#delete_record').click(function(e) {
          e.preventDefault();
          $('.ui.small.modal').modal('show');
      });
    });
    </script>
@stop