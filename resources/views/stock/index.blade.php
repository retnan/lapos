@extends(Auth::user()->access_role == "admin" ? 'layouts.admin_dashboard' : (Auth::user()->access_role == "cashier" ? 'layouts.cashier_dashboard' : 'layouts.default'))

@section('scripts')
  <script type="text/javascript" src="/js/jquery.dataTables.min.js"></script>
  <script type="text/javascript" src="/js/dataTables.semanticui.min.js"></script>
  <script type="text/javascript" src="/uilib/semantic.min.js"></script>
@stop

@section('stylesheets')
  <link rel="stylesheet" type="text/css" href="/uilib/semantic.min.css"/>
  <link rel="stylesheet" type="text/css" href="/css/dataTables.semanticui.min.css"/>
@stop

@section('content')
    {!! Breadcrumbs::render('stock_list') !!}
    <h4 class="ui dividing header">@lang('general.stock_catalog')</h4>
    
    <table class="ui small celled table display" id="dtable">
      <thead>
        <th>@lang('general.title')</th>
        <th>@lang('general.category')</th>
        <th>@lang('general.qty')</th>
        <th>@lang('general.selling_price') ({{html_entity_decode($AppConfig->currency_symbol, 0, 'UTF-8')}})</th>
        <th>@lang('general.cost_price') ({{html_entity_decode($AppConfig->currency_symbol, 0, 'UTF-8')}})</th>
      </thead>
      <tbody>
          
        @foreach ($data as $item)
            <tr>
              <td><!--<a href="/user/{{ $item->id}}">-->{{ $item->product->title}}<!--</a>--></td>
              <td>{{ $item->product->category->title}}</td>
              <td>{{ $item->Qty}}</td>
              <td>{{number_format($item->product->unit_selling_price, 2)}}</td>
              <td>{{number_format($item->product->unit_cost_price, 2)}}</td>
            </tr>
        @endforeach
      </tbody>
    </table>
    
    <script type="text/javascript">
    $( document ).ready(function() {
      $('#dtable').DataTable();
    });
    </script>
@stop