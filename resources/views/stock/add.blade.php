@extends(Auth::user()->access_role == "admin" ? 'layouts.admin_dashboard' : (Auth::user()->access_role == "cashier" ? 'layouts.cashier_dashboard' : 'layouts.default'))


@section('scripts')
  <script type="text/javascript" src="/js/jquery.dataTables.min.js"></script>
  <script type="text/javascript" src="/js/dataTables.semanticui.min.js"></script>
  <script type="text/javascript" src="/uilib/semantic.min.js"></script>
  <script type="text/javascript" src="/js/lapos_add_stock.js"></script>

  <script>
  $(document)
    .ready(function() {
      $('.ui.form')
        .form({
          fields: {
            title: {
              identifier  : 'title',
              rules: [
                {
                  type   : 'empty',
                  prompt : "@lang('jsvalidation.product_title_empty')"
                },
              ]
            },
            quantity: {
              identifier  : 'quantity',
              rules: [
                {
                  type   : 'empty',
                  prompt : "@lang('jsvalidation.stock_quantity_empty')"
                },
                {
                  type   : 'number',
                  prompt : "@lang('jsvalidation.stock_quantity_number')"
                },
                {
                  type   : 'integer',
                  prompt : "@lang('jsvalidation.stock_quantity_number')"
                }
              ]
            },
          }
        })
      ;
    })
  ;
  </script>
@stop

@section('stylesheets')
  <link rel="stylesheet" type="text/css" href="/uilib/semantic.min.css"/>
  <link rel="stylesheet" type="text/css" href="/css/dataTables.semanticui.min.css"/>
@stop

@section('content')
    {!! Breadcrumbs::render('stock_create') !!}
    <h4 class="ui dividing header">Add stocks</h4>

    {!! Form::open(array('url' => '/dashboard/admin/stocks', 'method'=>'POST', 'class'=>'ui small equal width form', 'id'=>"form")) !!}

    @if (count($errors) > 0)
        <div class="ui message" style="color:#9F3A38;font-size: 1em;box-shadow: 0px 0px 0px 1px #E0B4B4 inset, 0px 0px 0px 0px transparent; background-color: #FFF6F6;">
            <ul class="list">
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif

    <div id="loader" class="ui page inverted dimmer">
      <div class="content">
        <div class="center">
            <div class="ui active text loader">@lang('general.loader')</div>
        </div>
      </div>
    </div>

    

  <div class="fields">
    <div class="required field">
      <label>@lang('general.product_title')</label>
      <input type="hidden" id="product_id" name="product_id">
      <input type="hidden" value="" id="title" name="title">
      <div id="product_search" class="ui fluid category search">
          <div class="ui icon input">
            <input class="prompt" type="text" placeholder="@lang('general.search_placeholder')">
            <i class="search icon"></i>
          </div>
          <div class="results"></div>
      </div>
    </div>
  </div>

  <div class="fields">
    <div class="field">
        <label>@lang('general.category')</label>
        <input id="category" type="text" disabled="disabled" value="" />
    </div>
   <!--<div class="field">
    <label>Current Stock</label>
        <input type="text" disabled="disabled" />
    </div>-->
   <div class="field">
        <label>@lang('general.cost_price') price ({{html_entity_decode($AppConfig->currency_symbol, 0, 'UTF-8')}})</label>
        <input  id="unit_cost_price" disabled="disabled" type="text" value="" />
    </div>
  </div>

  <!--<div class="field">
        <label>Description</label>
        <textarea id="description" rows="2" disabled="disabled"></textarea>
  </div>-->

  @if($AppConfig->supports_warehouse)

  <div id="warehouse_container" style="display: none;">
    <div class="ui secondary segment statistics field">
      
    </div>    
  </div>

  <div class="ui segment">
    <div class="field">
      <div id="directly" class="ui toggle checkbox">
          <input type="checkbox" name="direct" class="hidden" />
          <label>@lang('general.stock_selected')</label>
      </div>
    </div>
  </div>
  @else
    <input type="hidden" name="direct" value="on" checked/>
  @endif

  <div class="fields">
    <div class="required field">
      <label>@lang('general.quantity')</label>
        <input type="text" name="quantity"/>
    </div>
  </div>
    
    <div class="field">
    <button style="background-color: #497093;" class="ui blue right floated button" type="submit" name="add_product">@lang('general.add_product')</button>
    <div style="clear:right;"></div>
    </div>
    {!! Form::close() !!}
    <br/>
    
    <script type="text/javascript">
    $( document ).ready(function() {
      $('.ui.dropdown').dropdown({
          on: 'hover'
        });
		  $('#dtable').DataTable();
    });
    </script>
@stop