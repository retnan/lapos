@extends(Auth::user()->access_role == "admin" ? 'layouts.admin_dashboard' : (Auth::user()->access_role == "cashier" ? 'layouts.cashier_dashboard' : 'layouts.default'))

@section('scripts')
  <script type="text/javascript" src="/js/jquery.dataTables.min.js"></script>
  <script type="text/javascript" src="/js/dataTables.semanticui.min.js"></script>
  <script type="text/javascript" src="/uilib/semantic.min.js"></script>
  <script type="text/javascript" src="/date_picker/moment.js"></script>
  <script type="text/javascript" src="/date_picker/daterangepicker.js"></script>
@stop

@section('stylesheets')
  <link rel="stylesheet" type="text/css" href="/uilib/semantic.min.css"/>
  <link rel="stylesheet" type="text/css" href="/css/dataTables.semanticui.min.css"/>
  <link rel="stylesheet" type="text/css" href="/date_picker/daterangepicker.css">
@stop

@section('content')
    {!! Breadcrumbs::render('stocks_report') !!}
    <h4 class="ui dividing header">@lang('report.stocks_header') - <i>{{$from->toFormattedDateString()}} @lang('report.to') {{$to->toFormattedDateString()}}</i></h4>

    <div id="reportrange" style="float: right; background: #fff; cursor: pointer; padding: 5px 10px; border: 1px solid #ccc">
      <i class="calendar icon"></i>
      <span>{{$from}} - {{$to}}</span><i class="caret down icon" style="margin:0"></i>
    </div>
    <br/><br/>

    <table class="ui small celled table display">
      <thead>
        <th>@lang('general.product')</th>
        <th>@lang('general.category')</th>
        <th>@lang('general.quantity')</th>
        <th>@lang('general.created_by')</th>
        <th>@lang('general.date')</th>
      </thead>
      <tbody>
        <?php $total = 0; ?>
        @foreach ($data as $item)
            <tr>
              <td>{{ $item->product->title}}</td>
              <td>{{ $item->product->category->title}}</td>
              <td>{{ $item->quantity}}</td>
              <td>{{ $item->user->name}}</td>
              <td>{{ $item->created_at->format('d/m/Y')}}</td>
            </tr>
        @endforeach
      </tbody>
    </table>

    <script type="text/javascript">
      $( document ).ready(function() {
          var optionSet2 = {
          startDate: moment().subtract(7, 'days'),
          endDate: moment(),
          minDate: '2016-01-01',
          maxDate: '2020-12-31',
          format: 'YYYY-MM-DD',
          opens: 'left',
          ranges: {
            'Today': [moment(), moment()],
            'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
            'Last 7 Days': [moment().subtract(6, 'days'), moment()],
            'Last 30 Days': [moment().subtract(29, 'days'), moment()],
            'This Month': [moment().startOf('month'), moment().endOf('month')],
            'Last Month': [moment().subtract(1, 'month').startOf('month'), 
              moment().subtract(1, 'month').endOf('month')]
          }
        };
        
        $('#reportrange').daterangepicker(optionSet2, 
            function(start, end, label){
              // console.log('/dashboard/report/stocks?from=' + start.format('YYYY-MM-D') + '&to=' + end.format('YYYY-MM-D'));
              window.location = '/dashboard/report/stocks?from=' + start.format('YYYY-MM-D') + '&to=' + end.format('YYYY-MM-D');
          });

          // $('#reportrange').on('apply.daterangepicker', function(ev, picker) {
          //     console.log('/dashboard/report/stocks?from=' + picker.startDate.format('YYYY-MM-D') + '&to=' + picker.endDate.format('YYYY-MM-D'))
          //     window.location = '/dashboard/report/stocks?from=' + picker.startDate.format('YYYY-MM-D') + '&to=' + picker.endDate.format('YYYY-MM-D');
          // });
      });
    </script>
@stop