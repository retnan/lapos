@extends(Auth::user()->access_role == "admin" ? 'layouts.admin_dashboard' : (Auth::user()->access_role == "cashier" ? 'layouts.cashier_dashboard' : 'layouts.default'))

@section('scripts')
  <script type="text/javascript" src="/js/jquery.dataTables.min.js"></script>
  <script type="text/javascript" src="/js/dataTables.semanticui.min.js"></script>
  <script type="text/javascript" src="/uilib/semantic.min.js"></script>
@stop

@section('stylesheets')
  <link rel="stylesheet" type="text/css" href="/uilib/semantic.min.css"/>
  <link rel="stylesheet" type="text/css" href="/css/dataTables.semanticui.min.css"/>
@stop

@section('content')
    {!! Breadcrumbs::render('stock_list') !!}
    <h4 class="ui dividing header">@lang('general.stock_catalog')</h4>

    @foreach ($data as $key => $items)
    <h1>{{ $key == "" ? "Unclassified" : strtoupper($key)}}</h1>
    <table class="ui small celled table display">
      <thead>
        <th>@lang('general.title')</th>
        <th>@lang('general.category')</th>
        <th>@lang('general.qty')</th>
        <th>SP ({{html_entity_decode($AppConfig->currency_symbol, 0, 'UTF-8')}})</th>
        <th>CP ({{html_entity_decode($AppConfig->currency_symbol, 0, 'UTF-8')}})</th>
        <th>Total SP ({{html_entity_decode($AppConfig->currency_symbol, 0, 'UTF-8')}})</th>
        <th>Total CP ({{html_entity_decode($AppConfig->currency_symbol, 0, 'UTF-8')}})</th>
      </thead>
      <tbody>
        <?php $totalCP = 0; $totalSP = 0; ?>
        @foreach ($items as $item)
            <tr>
              <td>{{ $item->product->title}}</td>
              <td>{{ $item->product->category->title}}</td>
              <td>{{ $item->Qty}}</td>
              <td>{{number_format($item->product->unit_selling_price, 2)}}</td>
              <td>{{number_format($item->product->unit_cost_price, 2)}}</td>
              <td>{{number_format($item->product->unit_selling_price * $item->Qty, 2)}}</td>
              <td>{{number_format($item->product->unit_cost_price * $item->Qty, 2)}}</td>
            </tr>
            <?php
            $totalCP += $item->product->unit_cost_price * $item->Qty;
            $totalSP += $item->product->unit_selling_price * $item->Qty;
            ?>
        @endforeach
            <tr>
              <td></td>
              <td></td>
              <td></td>
              <td></td>
              <td></td>
              <td><h3>{{html_entity_decode($AppConfig->currency_symbol, 0, 'UTF-8')}}{{number_format($totalSP, 2)}}</h3></td>
              <td><h3>{{html_entity_decode($AppConfig->currency_symbol, 0, 'UTF-8')}}{{number_format($totalCP, 2)}}</h3></td>
            </tr>
      </tbody>
    </table>
    @endforeach
@stop