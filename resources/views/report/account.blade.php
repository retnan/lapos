@extends('layouts.admin_dashboard')

@section('scripts')
  <script type="text/javascript" src="/js/jquery.dataTables.min.js"></script>
  <script type="text/javascript" src="/js/dataTables.semanticui.min.js"></script>
  <script type="text/javascript" src="/uilib/semantic.min.js"></script>
  <script type="text/javascript" src="/date_picker/moment.js"></script>
  <script type="text/javascript" src="/date_picker/daterangepicker.js"></script>
@stop

@section('stylesheets')
  <link rel="stylesheet" type="text/css" href="/uilib/semantic.min.css"/>
  <link rel="stylesheet" type="text/css" href="/css/dataTables.semanticui.min.css"/>
  <link rel="stylesheet" type="text/css" href="/date_picker/daterangepicker.css">
@stop

@section('content')
    {!! Breadcrumbs::render('account_report') !!}
    <h4 class="ui dividing header">@lang('report.report_account_header')</h4>

    <table class="ui small celled table display">
      <thead>
        <th>@lang('report.report_account_hash')</th>
        <th>@lang('report.report_account_title')</th>
        <th>@lang('report.report_balance') {{html_entity_decode($AppConfig->currency_symbol, 0, 'UTF-8')}}</th>
        <th>@lang('report.report_previous_balance') {{html_entity_decode($AppConfig->currency_symbol, 0, 'UTF-8')}}</th>
        <th>@lang('report.report_last_updated')</th>
      </thead>
      <tbody>
          
        @foreach ($data as $item)
            <tr>
              <td>{{ $item->account_no}}</td>
              <td>{{ $item->title}}</td>
              <td>{{number_format($item->balance, 2)}}</td>
              <td>{{number_format($item->prev_balance, 2)}}</td>
              <td>{{ $item->updated_at->format('d/m/Y')}}</td>
            </tr>
        @endforeach
      </tbody>
    </table>
@stop