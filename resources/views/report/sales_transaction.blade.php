@extends(Auth::user()->access_role == "admin" ? 'layouts.admin_dashboard' : (Auth::user()->access_role == "cashier" ? 'layouts.cashier_dashboard' : 'layouts.default'))

@section('scripts')
  <script type="text/javascript" src="/js/jquery.dataTables.min.js"></script>
  <script type="text/javascript" src="/js/dataTables.semanticui.min.js"></script>
  <script type="text/javascript" src="/uilib/semantic.min.js"></script>
  <script type="text/javascript" src="/date_picker/moment.js"></script>
  <script type="text/javascript" src="/date_picker/daterangepicker.js"></script>
@stop

@section('stylesheets')
  <link rel="stylesheet" type="text/css" href="/uilib/semantic.min.css"/>
  <link rel="stylesheet" type="text/css" href="/css/dataTables.semanticui.min.css"/>
  <link rel="stylesheet" type="text/css" href="/date_picker/daterangepicker.css">
@stop
 
@section('content')
    {!! Breadcrumbs::render('sales_report') !!}
    <h4 class="ui dividing header">@lang('report.sales_header') <i>{{$from->toFormattedDateString()}} @lang('report.to') {{$to->toFormattedDateString()}}</i></h4>

    <div id="reportrange" style="float: right; background: #fff; cursor: pointer; padding: 5px 10px; border: 1px solid #ccc">
      <i class="calendar icon"></i>
      <span>{{$from}} - {{$to}}</span><i class="caret down icon" style="margin:0"></i>
    </div>
    <br/><br/>

    <table class="ui small celled table display">
      <thead>
        <th>@lang('report.sales_account')</th>
        <th>@lang('report.sales_naration')</th>
        <th>@lang('report.sales_user')</th>
        <th>@lang('report.sales_type')</th>
        <th>@lang('report.sales_date')</th>
        <th>@lang('general.amount') {{html_entity_decode($AppConfig->currency_symbol, 0, 'UTF-8')}}</th>
      </thead>
      <tbody>
        <?php $total = 0; ?>
        @foreach ($data as $item)
            <tr>
              <td>{{ $item->account->title}}</td>
              <td>{{ $item->naration}}</td>
              <td>{{ $item->user->name}}</td>
              <td>{{ $item->transaction_type}}</td>
              <td>{{ $item->created_at->format('d/m/Y')}}</td>
              <td>{{number_format($item->amount, 2)}}</td>
            </tr>
            <?php $total += $item->amount; ?>
        @endforeach
      </tbody>
    </table>

    <!-- <div class="ui centered grid">
      <h1>  </h1>      
    </div> -->
    <div class="ui centered grid statistics">
          <div class="statistic">
                <div class="value">
                  {{$AppConfig->currency_symbol}} {{number_format($total, 2)}}
                </div>
                <div class="label">
                  @lang('report.sales_grand_total')
                </div>
            </div>
    </div>

    <script type="text/javascript">
      $( document ).ready(function() {
          var optionSet2 = {
          startDate: moment().subtract(7, 'days'),
          endDate: moment(),
          minDate: '2016-01-01',
          maxDate: '2020-12-31',
          format: 'YYYY-MM-DD',
          opens: 'left',
          ranges: {
            'Today': [moment(), moment()],
            'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
            'Last 7 Days': [moment().subtract(6, 'days'), moment()],
            'Last 30 Days': [moment().subtract(29, 'days'), moment()],
            'This Month': [moment().startOf('month'), moment().endOf('month')],
            'Last Month': [moment().subtract(1, 'month').startOf('month'), 
              moment().subtract(1, 'month').endOf('month')]
          }
        };
        
          $('#reportrange').daterangepicker(optionSet2, 
            function(start, end, label){
              // console.log('/dashboard/report/sales?from=' + start.format('YYYY-MM-D') + '&to=' + end.format('YYYY-MM-D'));
              window.location = '/dashboard/report/sales?from=' + start.format('YYYY-MM-D') + '&to=' + end.format('YYYY-MM-D');
          });

          // $('#reportrange').on('apply.daterangepicker', function(ev, picker) {
          //     console.log('/dashboard/report/sales?from=' + picker.startDate.format('YYYY-MM-D') + '&to=' + picker.endDate.format('YYYY-MM-D'))
          //     window.location = '/dashboard/report/sales?from=' + picker.startDate.format('YYYY-MM-D') + '&to=' + picker.endDate.format('YYYY-MM-D');
          // });        
      });
    </script>
@stop