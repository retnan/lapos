<!DOCTYPE html>
<html>
<head>
  <!-- Standard Meta -->
  <meta charset="utf-8" />
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
  <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0">
  <meta name="csrf-token" content="{{ csrf_token() }}">

  <!-- Site Properities -->
  <title>Le Gastro - Beauty Salon and Spa</title>

  <link rel="stylesheet" type="text/css" href="/welcome/semantic-ui/semantic.css">
  <link rel="stylesheet" type="text/css" href="/welcome/homepage.css">
  <link rel="stylesheet" type="text/css" href="/welcome/iconfonts/flaticon.css">

  <script src="/js/jquery.js"></script>
  <script src="/welcome/semantic-ui/semantic.js"></script>
  <script src="/welcome/homepage.js"></script>
  <script src="/js/init.js"></script>
   <script>
    $(function(){
        $('.ui.card').popup();
    });
    </script>
      <script>

  function loginPage(){
      window.location = '/login?demo=1';
  }

  $(document)
    .ready(function() {

      // fix menu when passed
      $('.masthead')
        .visibility({
          once: false,
          onBottomPassed: function() {
            $('.fixed.menu').transition('fade in');
          },
          onBottomPassedReverse: function() {
            $('.fixed.menu').transition('fade out');
          }
        })
      ;

      // create sidebar and attach to menu open
      $('.ui.sidebar')
        .sidebar('attach events', '.toc.item')
      ;

    $('#get_started').click(function(e){
      loginPage();
    })

    })
  ;
  </script>
</head>
<body id="home">
  <div class="ui inverted masthead centered segment" style="background color: #983098">
    <div class="ui page grid">
      <div class="column">
        
 

<div class="ui secondary pointing menu">
   <a class="logo item">
    @lang('landing.landing_logo')
  </a>
  <!-- <a class="item">
    <i class="flaticon-heart"></i> Friends
  </a> -->
  <div class="right menu">
    <!-- <div class="item">
      <div class="ui icon input">
        <input placeholder="Search..." type="text">
        <i class="flaticon-position link icon"></i>
      </div>
    </div> -->
    <a href="/login?demo=1" class="ui item">
      @lang('landing.landing_login')
    </a>
  </div>
</div>
                   
        <div class="ui hidden transition information">
          <h1 class="ui inverted centered header">
            <!-- Make 10X your daily transactions -->
            @lang('landing.landing_pos_header')
          </h1>
          <p class="ui centered lead">@lang('landing.landing_lead_content')
          </p>
          <a href="#" class="large basic inverted animated fade ui button">
            <div id="get_started" class="hidden content">@lang('landing.landing_getstarted')</div>
          </a>
         <!--<div class="ui centerted image">
            <img src="/welcome/images/banner.png" />
            </div>-->
        </div>
      </div>
    </div>
  </div>


  <script type="text/javascript" src="/js/subscribe.js"></script>
</body>

</html>
