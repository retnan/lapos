<!--
@if ($breadcrumbs)
	<div class="ui breadcrumb">
		@foreach ($breadcrumbs as $breadcrumb)
            @if (!$breadcrumb->last)
                <a href="{{ $breadcrumb->url }}" class="section">{{ $breadcrumb->title }}</a>
				<span class="divider">/</span>
            @else
            	<div class="active section">{{ $breadcrumb->title }}</div>
            @endif
        @endforeach
    </div>
@endif
-->
<div class="sixteen wide column">
@if ($breadcrumbs)
	<div class="breadcrumbs">
		<ul class="breadcrumb-alt">
		@foreach ($breadcrumbs as $breadcrumb)
            @if (!$breadcrumb->last)
            	<li><a href="{{ $breadcrumb->url }}">{{ $breadcrumb->title }}</a></li>
            @else
            	<li><a href="#">{{ $breadcrumb->title }}</a></li>
            @endif
        @endforeach
    	</ul>
    </div>
@endif
</div>