@extends('layouts.admin_dashboard')

@section('scripts')
  <script type="text/javascript" src="/js/jquery.dataTables.min.js"></script>
  <script type="text/javascript" src="/js/dataTables.semanticui.min.js"></script>
  <script type="text/javascript" src="/uilib/semantic.min.js"></script>
@stop

@section('stylesheets')
  <link rel="stylesheet" type="text/css" href="/uilib/semantic.min.css"/>
  <link rel="stylesheet" type="text/css" href="/css/dataTables.semanticui.min.css"/>
@stop

@section('content')
    {!! Breadcrumbs::render('user_list') !!}
    <h4 class="ui dividing header">@lang('user.user_list_header')</h4>
    
    <table class="ui small celled table display" id="dtable">
      <thead>
        <th></th>
        <th>@lang('user.user_name')</th>
        <th>@lang('user.user_email')</th>
        <th>@lang('user.user_access_role')</th>
        <th>@lang('general.created_at')</th>
      </thead>
      <tbody>
          
        @foreach ($users as $item)
            <tr>
                @if($item->access_role != 'virtual')
                <td class="selectable positive collapsing">
                  <a href="{{ '/dashboard/admin/user/' . $item->id . '/edit' }}"><i class="green write icon"></i></a>
                </td>
                @else
                <td class="selectable blue collapsing">
                  <a href="#"><i class="blue browser icon"></i></a>
                </td>
                @endif
              <td><!--<a href="/user/{{ $item->id}}">-->{{ $item->name}}<!--</a>--></td>
              <td>{{ $item->email}}</td>
              <td>
                @if ($item->access_role == "admin")
                    {{ 'Admin' }}
                @elseif ($item->access_role == "cashier")
                    {{ 'Cashier' }}
                @else
                    {{ 'Virtual' }}
                @endif
                </td>
              <td>{{ $item->created_at}}</td>
            </tr>
        @endforeach
      </tbody>
    </table>
    
    <script type="text/javascript">
    $( document ).ready(function() {
		  $('#dtable').DataTable();
    });
    </script>
@stop