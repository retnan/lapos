@extends('layouts.admin_dashboard')

@section('scripts')
  <script type="text/javascript" src="/uilib/semantic.min.js"></script>
  <script>
  $(document)
    .ready(function() {
      $('.ui.form')
        .form({
          fields: {
            name: {
              identifier  : 'name',
              rules: [
                {
                  type   : 'empty',
                  prompt : "@lang('jsvalidation.user_name_empty')"
                },
              ]
            },
            email: {
              identifier  : 'email',
              rules: [
                {
                  type   : 'empty',
                  prompt : "@lang('jsvalidation.user_email_empty')"
                },
                {
                  type   : 'email',
                  prompt : "@lang('jsvalidation.user_email_invalid')"
                }
              ]
            },
            password: {
              identifier  : 'password',
              rules: [
                {
                  type   : 'empty',
                  prompt : "@lang('jsvalidation.user_password_empty')"
                },
                {
                  type   : 'length[6]',
                  prompt : "@lang('jsvalidation.user_password_lenght')"
                }
              ]
            }
          }
        })
      ;
    })
  ;
  </script>
@stop

@section('stylesheets')
  <link rel="stylesheet" type="text/css" href="/uilib/semantic.min.css"/>
@stop

@section('content')
  {!! Breadcrumbs::render('user_create') !!}
    
   {!! Form::open(array('url' => '/dashboard/admin/user/create', 'method'=>'POST', 'class'=>'ui small equal width form')) !!}
      <h4 class="ui dividing header">@lang('user.user_information_header')</h4>

      @if (count($errors) > 0)
        <div class="ui message" style="color:#9F3A38;font-size: 1em;box-shadow: 0px 0px 0px 1px #E0B4B4 inset, 0px 0px 0px 0px transparent; background-color: #FFF6F6;">
            <ul class="list">
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
      @endif
      
      <div class="fields">
        <div class="required field">
          <label>@lang('user.user_full_name')</label>
          <input name="name" placeholder="@lang('user.user_full_name_placeholder')" type="text">
        </div>
      </div>
      
      <div class="fields">
        <div class="required field">
          <label>@lang('user.user_email')</label>
          <input name="email" placeholder="@lang('user.user_email_placeholder')" type="text">
        </div>
      </div>
      
      <div class="fields">
        <div class="required field">
          <label>@lang('user.user_password')</label>
          <input name="password" placeholder="@lang('user.user_password_placeholder')" type="password">
        </div>
      </div>
      
      <div class="grouped fields">
        <label>@lang('user.user_access_role')</label>
    <div class="field">
      <div class="ui radio checkbox">
        <input value="admin" class="hidden" tabindex="0" name="access_role" type="radio">
        <label>@lang('user.user_admin')</label>
      </div>
    </div>
    <div class="field">
      <div class="ui radio checkbox">
        <input checked="checked" value="cashier" class="hidden" tabindex="0" name="access_role" type="radio">
        <label>@lang('user.user_cashier')</label>
      </div>
    </div>
  </div>
      
    <span><input type="submit" name="approve" class="ui primary button" value="@lang('user.user_add_user')"></span>
    <div class="ui error message"> </div>
    {!! Form::close() !!}
    
    <script type="text/javascript">
    $( document ).ready(function() {
		  $('.ui.radio.checkbox').checkbox();
    });
    </script>
@stop