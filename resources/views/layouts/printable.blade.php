<!DOCTYPE html>
<html>
<head>
  <!-- Standard Meta -->
  <meta charset="utf-8" />
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
  <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0">

  <!-- Site Properties -->
  <title>Le Gastro - Dashboard</title>

  <script src="/js/jquery.js"></script>
  @yield('stylesheets')
  @yield('scripts')
  <link rel="stylesheet" type="text/css" href="/css/printable.css">
</head>
<body>

  <div class="ui main">

    <div class="ui page grid">
        <div class="sixteen wide column">
        @if (Session::has('flash_error'))
          <div class="ui error message">{{ Session::get('flash_error') }}</div>
        @endif
        @if (Session::has('flash_message'))
          <div class="ui message">{{ Session::get('flash_message') }}</div>
        @endif
        @yield('content')
        </div>
    </div>
  </div>
</body>

</html>