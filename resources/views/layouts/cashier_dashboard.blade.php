<!DOCTYPE html>
<html>
<head>
  <!-- Standard Meta -->
  <meta charset="utf-8" />
  <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
  <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0">
  <meta name="csrf-token" content="{{ csrf_token() }}">

  <!-- Site Properties -->
  <title>Le Gastro - Dashboard</title>

  <script src="/js/jquery.js"></script>
  <script src="/js/init.js"></script>
  <script src="/js/universal_search.js"></script>
  @yield('stylesheets')
  @yield('scripts')
  <link rel="stylesheet" type="text/css" href="/css/style.css">
</head>
<body>

  <div class="ui blue inverted menu" style="background-color: #800080; border-color: #3e5f7d">
    <div class="ui container">
      <a href="#" class="header item">
        <!--<img class="logo" src="/assets/images/logo.png">-->
        {{$AppConfig->company_short_name}}
      </a>
      <a href="/dashboard/pos" class="item">POS</a>
      <div class="ui simple dropdown item">
        <i class="Idea icon"></i> Product <i class="dropdown icon"></i>
        <div class="menu">
          <a class="item" href="/dashboard/admin/product">Products</a>
          <!-- <a class="item" href="/dashboard/admin/discount">Discounts</a> -->
          <a class="item" href="/dashboard/admin/product/create">Create Product</a>
          <a class="item" href="/dashboard/admin/stocks">Stocks</a>
          <!-- <a class="item" href="/dashboard/admin/stocks/create">Add Stocks</a> -->
          <a class="item" href="/dashboard/report/stocks">Stock History</a>
          <a class="item" href="/dashboard/admin/category">Product Categories</a>
          <a class="item" href="/dashboard/admin/category/create">Create Category</a>
          <a class="item" href="/dashboard/admin/supplier">Suppliers</a>
          <a class="item" href="/dashboard/admin/supplier/create">Create Supplier</a>
        </div>
      </div>
      <!-- <a href="/dashboard/cashier/" class="item">Home</a> -->
      <div class="ui simple dropdown item">
        Customer <i class="dropdown icon"></i>
        <div class="menu">
          <a class="item" href="/dashboard/customer">Customer List</a>
          <a class="item" href="/dashboard/customer/create">Create Customer</a>
        </div>
      </div>
      <!-- <a href="/dashboard/pos" class="item">POS</a> -->
      <a href="/dashboard/purchase" class="item">Purchase</a>
      <div class="ui simple dropdown item">
        Extras <i class="dropdown icon"></i>
        <div class="menu">
        <a href="/dashboard/report/sales" class="item">Sales Transactions</a>
        <a href="/dashboard/report/transactions" class="item">All Transactions</a>
        <a class="item" href="/dashboard/createExpenses">Create Expenses</a>
        </div>
      </div>
      <div class="right menu">
        <div id="all_search" class="ui fluid category search item">
          <div class="ui icon large input">
            <input id="product_input" class="prompt" type="text" placeholder="Order #, Receipt #...">
            <i class="search icon"></i>
          </div>
          <div class="results"></div>
        </div>
        <div class="ui simple dropdown item">Welcome {{Auth::user()->name}}
          <div class="menu">
            <a class="item" href="/logout">Log out</a>
          </div>
      </div>
      </div>
    </div>
  </div>


  <div class="ui main">
    <!--<h1 class="ui header">List of names to approve</h1>-->
    <div class="ui page grid" style="min-height: 350px;margin-top: 10px;">
        <div class="sixteen wide column">
        @if (Session::has('flash_error'))
          <div class="ui error message">{{ Session::get('flash_error') }}</div>
        @endif
        @if (Session::has('flash_message'))
          <div class="ui message">{{ Session::get('flash_message') }}</div>
        @endif
        @yield('content')
        </div>
    </div>
  </div>
  @include('subviews.footer')
</body>

</html>
