@extends('layouts.admin_dashboard')

@section('scripts')
  <!-- <script type="text/javascript" src="/uilib/semantic.min.js"></script> -->
  <script type="text/javascript" src="/js/semanticAjaxHack.js"></script>
@stop

  <link rel="stylesheet" type="text/css" href="">

@section('stylesheets')
  <link rel="stylesheet" type="text/css" href="/uilib/semantic.min.css"/>
@stop

@section('content')
	{!! Breadcrumbs::render('admin_dashboard') !!} 
@stop