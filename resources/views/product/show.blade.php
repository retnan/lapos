@extends(Auth::user()->access_role == "admin" ? 'layouts.admin_dashboard' : (Auth::user()->access_role == "cashier" ? 'layouts.cashier_dashboard' : 'layouts.default'))


@section('scripts')
  <script type="text/javascript" src="/js/jquery.dataTables.min.js"></script>
  <script type="text/javascript" src="/js/dataTables.semanticui.min.js"></script>
  <script type="text/javascript" src="/uilib/semantic.min.js"></script>
@stop

@section('stylesheets')
  <link rel="stylesheet" type="text/css" href="/uilib/semantic.min.css"/>
  <link rel="stylesheet" type="text/css" href="/css/dataTables.semanticui.min.css"/>
@stop

@section('content')
    {!! Breadcrumbs::render('product_show', $data) !!}
    <h4 class="ui dividing header">@lang('form.product_detail')</h4>
    <table class="ui celled striped table">
      <thead>
        <tr>
          <th colspan="3">{{ $data->title}}</th>
        </tr>
      </thead>
    <tbody>
      <tr>
        <td class="collapsing">@lang('general.category')</td>
        <td>{{ $data->category->title}}</td>
      </tr>
      <tr>
        <td class="collapsing">@lang('general.selling_price')</td>
        <td>{{html_entity_decode($AppConfig->currency_symbol, 0, 'UTF-8')}}{{ number_format($data->unit_selling_price, 2)}}</td>
      </tr>
      <tr>
        <td class="collapsing">@lang('general.cost_price')</td>
        <td>{{html_entity_decode($AppConfig->currency_symbol, 0, 'UTF-8')}}{{ number_format($data->unit_cost_price, 2)}}</td>
      </tr>
      @if($AppConfig->supports_tax)
      <tr>
        <td class="collapsing">@lang('general.tax')</td>
        @if($data->tax_type == "absolute")
          <td>{{html_entity_decode($AppConfig->currency_symbol, 0, 'UTF-8')}}{{ $data->unit_cost_price}}</td>
        @else
          <td>{{ $data->tax}}%</td>
        @endif
      </tr>
      @endif
      <tr>
        <td class="collapsing">@lang('form.product_min_qantity')</td>
        <td>{{$data->min_threshold}}</td>
      </tr>
      <tr>
        <td class="collapsing">@lang('general.description')</td>
        <td>{{ $data->description}}</td>
      </tr>
    </tbody>
  </table>
       
    <script type="text/javascript">
    $( document ).ready(function() {
          
    });
    </script>
@stop