@extends($isPrintable ? 'layouts.printable' : (Auth::user()->access_role == "admin" ? 'layouts.admin_dashboard' : (Auth::user()->access_role == "cashier" ? 'layouts.cashier_dashboard' : 'layouts.default')))
@section('scripts')
	<script type="text/javascript" src="/uilib/semantic.min.js"></script>
	<script type="text/javascript">
    $( document ).ready(function() {
        $('#print').click(function(e){
          	var printWindow = window.open($(this).attr('href'), null, "height=650,width=900");
          	printWindow.focus();
          	printWindow.print();
          	e.preventDefault();
        });
    });
    </script>
</script>  
@stop

@section('stylesheets')
  <link rel="stylesheet" type="text/css" href="/uilib/semantic.min.css"/>
@stop

@section('content')
	@if(!$isPrintable)
		{!! Breadcrumbs::render('receiving_receipt', $data) !!}
	@endif
	<div class="column @if (!$isPrintable) ui segment @endif ">
		<div class="ui two column grid">
			<div class="eight wide column">
				<img src="/assets/images/lapos.png" height="80" style="padding-top: 10px;">
				<!-- <h1>{{$AppConfig->company_name}}</h1> -->
			</div>
			<div class="float eight right aligned wide column">
			<!-- BarCode Here -->
			<b>{{$AppConfig->company_name}}</b><br/>
			{{$AppConfig->address}}<br/>
			{{$AppConfig->email}}<br/>
			{{$AppConfig->mobile_number}}<br/>
			</div>
		</div>
		<h2>{{$data->payment_type == 'CASH' ? 'SUPPLIER RECEIPT' : 'SUPPLIER INVOICE'}}</h2>
		<div class="ui two column grid">
			<div class="eight wide column">
				{{$data->payment_type == 'CASH' ? 'Reciept' : 'Invoice'}} # {{$data->receipt_number}} <br/>
				Order # {{str_pad($data->id, 13, "0", STR_PAD_LEFT)}} <br/>
				{{$data->payment_type == 'CASH' ? 'Receipt Date' : 'Invoice Date'}} #: {{$data->created_at->format('d/m/Y')}} <br/>
				<br/>
				<b>@lang('general.payment_method')</b> <br/>
				<b>{{$data->payment_type}}</b>
			</div>
			<div class="eight wide column">
				<b>@lang('general.seller')</b>: <br/>
				{{$data->supplier->title}} <br/>
				{{$data->supplier->address}} <br/>
				{{$data->supplier->mobile_number}}
			</div>
		</div>
		<br/>
		<br/>
		<div class="ui centered grid">
			<table class="ui very basic table">
				<thead>
					<th>@lang('general.product')</th>
					<th>@lang('general.category')</th>
					<th>@lang('general.cost')</th>
					<th>@lang('general.qty')</th>
				</thead>
				<tbody>
				<?php $subtotal = 0; ?>
				@foreach($data->purchases as $item)
				<?php $subtotal += $item->unit_cost_price * $item->quantity; ?>
					<tr>
						<td>{{$item->product->title}}</td>
						<td>{{$item->product->category->title}}</td>
						<td>{{$AppConfig->currency_symbol}}{{number_format($item->unit_cost_price, 2)}}</td>
						<td>{{$item->quantity}}</td>
					</tr>
				@endforeach
				</tbody>
			</table>			
		</div>
		<div class="ui divider"></div>
		<div class="ui grid">
			<div class="floated left eight wide column">
				<?php echo DNS2D::getBarcodeSVG($data->receipt_number, "DATAMATRIX", 9, 9); ?>
			</div>
			<div class="right floated eight wide column">
				<h3>@lang('general.order_totals'):</h3>
				<table class="ui very basic right aligned table" style="border:none;">
					<tr>
						<td>@lang('general.order_subtotal'):</td>
						<td>{{$AppConfig->currency_symbol}}{{number_format($subtotal, 2)}}</td>
					</tr>
					@if($data->payment_type == 'CREDIT')
					<tr>
						<td>@lang('general.amount_paid'):</td>
						<td>{{$AppConfig->currency_symbol}}{{number_format(abs($data->deposit + 0), 2)}}</td>
					</tr>
					@endif
				</table>				
				<div class="ui divider"></div>
				@if($data->payment_type == 'CASH')
				<h4>@lang('general.total_amount_paid'): {{$AppConfig->currency_symbol}}{{number_format($subtotal, 2)}}</h4>
				@endif
				@if($data->payment_type == 'CREDIT')
				<!-- <h4>Amount Paid: {{$AppConfig->currency_symbol}}{{number_format(abs($data->deposit), 2)}}</h4> -->
				<h4>@lang('general.balance_due_for_payment'): {{$AppConfig->currency_symbol}}{{number_format($subtotal, 2)}}</h4>
				@endif
			</div>
		</div>
	</div>

	@if(!$isPrintable)
		<div class="ui centered grid" style="margin: 10px;">
			<a href="/dashboard/purchase" class="ui primary button">@lang('general.back')</a>
			<a href="/dashboard/purchase/{{$data->receipt_number}}/?q=printable" id="print" class="ui teal button">@lang('general.print')</a>
			@if($data->payment_type == 'CREDIT')
			{!! Form::open(array('url' => '/dashboard/admin/receivings', 'method'=>'PUT')) !!}
			<input type="hidden" value="{{$data->receipt_number}}" name="invoice_number">
			<input type="submit" name="submit" class="ui button" value="@lang('general.mark_as_paid')">
			{!! Form::close() !!}
			@endif
		</div>
	@endif
@stop