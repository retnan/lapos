@extends(Auth::user()->access_role == "admin" ? 'layouts.admin_dashboard' : (Auth::user()->access_role == "cashier" ? 'layouts.cashier_dashboard' : 'layouts.default'))

@section('scripts')
  <script type="text/javascript" src="/uilib/semantic.min.js"></script>
  <script>
  $(document)
    .ready(function() {
      $('.ui.form')
        .form({
          fields: {
            title: {
              identifier  : 'title',
              rules: [
                {
                  type   : 'empty',
                  prompt : "@lang('jsvalidation.product_title_empty')"
                },
              ]
            },
            unit_selling_price: {
              identifier  : 'unit_selling_price',
              rules: [
                {
                  type   : 'empty',
                  prompt : "@lang('jsvalidation.product_unit_selling_price_empty')"
                },
                {
                  type   : 'number',
                  prompt : "@lang('jsvalidation.product_unit_selling_price_number')"
                }
              ]
            },
            unit_cost_price: {
              identifier  : 'unit_cost_price',
              rules: [
                {
                  type   : 'empty',
                  prompt : "@lang('jsvalidation.product_unit_cost_price_empty')"
                },
                {
                  type   : 'number',
                  prompt : "@lang('jsvalidation.product_unit_cost_price_number')"
                }
              ]
            }
          }
        })
      ;
    })
  ;
  </script>
@stop

@section('stylesheets')
  <link rel="stylesheet" type="text/css" href="/uilib/semantic.min.css"/>
@stop

@section('content')
   {!! Breadcrumbs::render('product_create') !!}
   {!! Form::open(array('route' => 'product_post', 'method'=>'POST', 'class'=>'ui small equal width form')) !!}
      <h4 class="ui dividing header">@lang('form.product_header')</h4>

      @if (count($errors) > 0)
        <div class="ui message" style="color:#9F3A38;font-size: 1em;box-shadow: 0px 0px 0px 1px #E0B4B4 inset, 0px 0px 0px 0px transparent; background-color: #FFF6F6;">
            <ul class="list">
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
      @endif
      
      <div class="fields">
        <div class="required field">
          <label>@lang('form.product_title'):</label>
          <input name="title" placeholder="@lang('form.product_placeholder_title')" type="text">
        </div>
      </div>

      <div class="fields">
        <div class="field">
          <label>@lang('form.product_category'):</label>
          {!! Form::select('category_id', $categories, null, array('class' => 'ui fluid dropdown')) !!}
        </div>
      </div>
      
      <div cgitlass="fields">
        <div class="required field">
          <label>@lang('form.product_unit_cost_price') ({{html_entity_decode($AppConfig->currency_symbol, 0, 'UTF-8')}}):</label>
          <input name="unit_cost_price" placeholder="256390.00" type="text">
        </div>
        <div class="required field">
          <label>@lang('form.product_unit_selling_price') ({{html_entity_decode($AppConfig->currency_symbol, 0, 'UTF-8')}}):</label>
          <input name="unit_selling_price" placeholder="256342.00" type="text">
        </div>
      </div>
      
      <div class="fields">
        <div class="field">
          <label>@lang('form.product_alert_threshold'):</label>
          <input value="0" name="min_threshold" placeholder="0" type="text">
        </div>
        @if($AppConfig->supports_tax)
        <div class="field">
          <label>@lang('form.product_tax'):</label>
          <div class="ui right labeled input">
            <input name="tax" value="0" placeholder="0" type="text">
            <input name="tax_type" value="percentage" type="hidden">
            <div id="taxType" class="ui dropdown label">
              <div class="text">@lang('form.product_tax_type')</div>
              <i class="dropdown icon"></i>
              <div class="menu">
                <div class="item">@lang('general.percentage') [@lang('general.default')]</div>
                <!-- <div class="item">{{$AppConfig->currency_name}} Cost [Default]</div> -->
              </div>
            </div>
        </div>
        </div>
        @endif
      </div>

      <div class="fields">
        <div class="field">
          <label>Stock Tag:</label>
          <input name="tag" placeholder="bar" type="text">
        </div>
      </div>

      <div class="fields">
        <div class="field">
          <label>@lang('general.description'):</label>
          <textarea name="description" type="text"></textarea>
        </div>
      </div>
      
    <span><input type="submit" name="submit" class="ui primary button" value="@lang('form.product_add')"></span>
    <div class="ui error message"> </div>
    {!! Form::close() !!}
    
    <script type="text/javascript">
    $( document ).ready(function() {
      $('.ui.dropdown').dropdown({
          on: 'hover'
        });
      
		  $('.ui.radio.checkbox').checkbox();
      $('#taxType').dropdown({ on: 'click'});
    });
    </script>
@stop