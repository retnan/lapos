@extends(Auth::user()->access_role == "admin" ? 'layouts.admin_dashboard' : (Auth::user()->access_role == "cashier" ? 'layouts.cashier_dashboard' : 'layouts.default'))

@section('scripts')
  <script type="text/javascript" src="/uilib/semantic.min.js"></script>
  <script>
  $(document)
    .ready(function() {
      $('.ui.form')
        .form({
          fields: {
            title: {
              identifier  : 'title',
              rules: [
                {
                  type   : 'empty',
                  prompt : "@lang('jsvalidation.product_title_empty')"
                },
              ]
            },
            unit_selling_price: {
              identifier  : 'unit_selling_price',
              rules: [
                {
                  type   : 'empty',
                  prompt : "@lang('jsvalidation.product_unit_selling_price_empty')"
                },
                {
                  type   : 'number',
                  prompt : "@lang('jsvalidation.product_unit_selling_price_number')"
                }
              ]
            },
            unit_cost_price: {
              identifier  : 'unit_cost_price',
              rules: [
                {
                  type   : 'empty',
                  prompt : "@lang('jsvalidation.product_unit_cost_price_empty')"
                },
                {
                  type   : 'number',
                  prompt : "@lang('jsvalidation.product_unit_cost_price_number')"
                }
              ]
            }
          }
        })
      ;
    })
  ;
  </script>
@stop

@section('stylesheets')
  <link rel="stylesheet" type="text/css" href="/uilib/semantic.min.css"/>
@stop

@section('content')
   {!! Breadcrumbs::render('product_edit', $data) !!}
   {!! Form::open(array('url' => '/dashboard/admin/product/' . $data->id, 'method'=>'PUT', 'class'=>'ui small equal width form')) !!}
      <h4 class="ui dividing header">@lang('form.product_header')</h4>

      @if (count($errors) > 0)
        <div class="ui message" style="color:#9F3A38;font-size: 1em;box-shadow: 0px 0px 0px 1px #E0B4B4 inset, 0px 0px 0px 0px transparent; background-color: #FFF6F6;">
            <ul class="list">
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
      @endif
      
      <div class="fields">
        <div class="required field">
          <label>@lang('form.product_title')</label>
          <input value="{{ old('title') ? old('title') : $data->title }}" name="title" placeholder="@lang('form.product_placeholder_title')" type="text">
        </div>
      </div>

      <div class="fields">
        <div class="field">
          <label>@lang('form.product_category')</label>
          {!! Form::select('category_id', $categories, $data->category_id, array('class' => 'ui fluid dropdown')) !!}
        </div>
      </div>
      
      <div class="fields">
        <div class="required field">
          <label>@lang('form.product_unit_cost_price')({{html_entity_decode($AppConfig->currency_symbol, 0, 'UTF-8')}}):</label>
          <input value="{{ old('unit_cost_price') ? old('unit_cost_price') : $data->unit_cost_price }}" name="unit_cost_price" placeholder="@lang('form.product_placeholder_unit_cost_price')" type="text">
        </div>
        <div class="required field">
          <label>@lang('form.product_unit_selling_price')({{html_entity_decode($AppConfig->currency_symbol, 0, 'UTF-8')}}):</label>
          <input value="{{ old('unit_selling_price') ? old('unit_selling_price') : $data->unit_selling_price }}" name="unit_selling_price" placeholder="@lang('form.product_placeholder_unit_selling_price')" type="text">
        </div>
      </div>
      
      <div class="fields">
        <div class="field">
          <label>@lang('form.product_alert_threshold')</label>
          <input value="{{ old('min_threshold') ? old('min_threshold') : $data->min_threshold }}" name="min_threshold" placeholder="@lang('form.product_alert_placeholder_threshold')" type="text">
        </div>
        @if($AppConfig->supports_tax)
        <div class="field">
          <label>@lang('form.product_tax')</label>
          <div class="ui right labeled input">
            <input value="{{ old('tax') ? old('tax') : $data->tax }}" name="tax" value="0" placeholder="@lang('form.product_placeholder_tax')" type="text">
            <input name="tax_type" value="{{ old('tax_type') ? old('tax_type') : $data->tax_type }}" type="hidden">
            <div id="taxType" class="ui dropdown label">
              <div class="text">@lang('form.product_tax_type')</div>
              <i class="dropdown icon"></i>
              <div class="menu">
                <div class="item">@lang('general.percentage') <?php echo ($data->tax_type == "percentage" ? "[SELECTED]" : ""); ?></div>
                <div class="item">{{$AppConfig->currency_name}} Cost <?php echo ($data->tax_type == "percentage" ? "" : "[SELECTED]"); ?></div>
              </div>
            </div>
          </div>
        </div>
        @endif
      </div>

      <div class="fields">
        <div class="field">
          <label>Stock Tag:</label>
          <input value="{{ old('tag') ? old('tag') : $data->tag }}" name="tag" placeholder="bar" type="text">
        </div>
      </div>

      <div class="fields">
        <div class="field">
          <label>@lang('form.product_description')</label>
          <textarea name="description" type="text">{{ old('description') ? old('description') : $data->description }}</textarea>
        </div>
      </div>
      
    <span><input type="submit" name="submit" class="ui primary button" value="@lang('form.product_update')"></span>
    <span><a href="/dashboard/admin/product/{{$data->id}}/delete" id="delete_record" class="ui red button">@lang('general.delete_record')</a></span>
    <div class="ui error message"> </div>
    {!! Form::close() !!}


    <div class="ui small modal">
  <i class="close icon"></i>
  <div class="header">
    @lang('general.delete_record_header')
  </div>
  <div class="content">
    <p>@lang('general.delete_record_content')</p>
  </div>
  <div class="actions">
    <div class="ui negative button">
        @lang('general.delete_record_no')
    </div>
    <a href="/dashboard/admin/product/{{$data->id}}/delete" class="ui positive button">
        <i class="checkmark icon"></i>
        @lang('general.delete_record_yes')
      </a>
  </div>
</div>

    
    <script type="text/javascript">
    $( document ).ready(function() {
      $('.ui.dropdown').dropdown({
          on: 'hover'
        });
      
		  $('.ui.radio.checkbox').checkbox();
      $('#taxType').dropdown({ on: 'click'});
      $('#delete_record').click(function(e) {
          e.preventDefault();
          $('.ui.small.modal').modal('show');
      });
    });
    </script>
@stop