@extends(Auth::user()->access_role == "admin" ? 'layouts.admin_dashboard' : (Auth::user()->access_role == "cashier" ? 'layouts.cashier_dashboard' : 'layouts.default'))

@section('scripts')
	<script type="text/javascript">
  		var App = {
  			Currency: "{{$AppConfig->currency_symbol}}",
  		};
  	</script>
  <!-- <script type="text/javascript" src="/uilib/semantic.min.js"></script> -->
  <script type="text/javascript" src="/js/semanticAjaxHack.js"></script>
  <script type="text/javascript" src="/js/knockout-3.4.0.js"></script>
  <script type="text/javascript" src="/js/accounting.min.js"></script>
  <script type="text/javascript" src="/js/receivings.js"></script>
@stop

@section('stylesheets')
  <link rel="stylesheet" type="text/css" href="/uilib/semantic.min.css"/>
  <style type="text/css">
  .product_details{
  	font-style: italic;
  	font-size: 11px;
  	font-weight: bold;
  	color: green;
  }
   .paper-cut:after {
	  content: " ";
	  display: block;
	  position: relative;
	  top: 0px;
	  left: 0px;
	  width: 100%;
	  height: 36px;
	  background: -webkit-linear-gradient(#FFFFFF 0%, transparent 0%), -webkit-linear-gradient(135deg, #e8ebf1 33.33%, transparent 33.33%) 0 0%, #e8ebf1 -webkit-linear-gradient(45deg, #e8ebf1 33.33%, #FFFFFF 33.33%) 0 0%;
	  background: -o-linear-gradient(#FFFFFF 0%, transparent 0%), -o-linear-gradient(135deg, #e8ebf1 33.33%, transparent 33.33%) 0 0%, #e8ebf1 -o-linear-gradient(45deg, #e8ebf1 33.33%, #FFFFFF 33.33%) 0 0%;
	  background: -moz-linear-gradient(#FFFFFF 0%, transparent 0%), -moz-linear-gradient(135deg, #e8ebf1 33.33%, transparent 33.33%) 0 0%, #e8ebf1 -moz-linear-gradient(45deg, #e8ebf1 33.33%, #FFFFFF 33.33%) 0 0%;
	  background-repeat: repeat-x;
	  background-size: 0px 100%, 14px 27px, 14px 27px;
}
  </style>
@stop

@section('content')
	{!! Breadcrumbs::render('get_receivings') !!}

	@if (count($errors) > 0)
        <div class="ui message" style="color:#9F3A38;font-size: 1em;box-shadow: 0px 0px 0px 1px #E0B4B4 inset, 0px 0px 0px 0px transparent; background-color: #FFF6F6;">
            <ul class="list">
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif

    <div id="loader" class="ui page inverted dimmer">
      <div class="content">
        <div class="center">
            <div class="ui active text loader">@lang('receiving.receiving_loader')</div>
        </div>
      </div>
    </div>

    <br/>


	<div class="column">
		<div class="ui two column grid">
			<div class="ten wide column">
				<form id="shopping_cart" class="ui small equal width form">
				<!-- <div class="fields"> -->
				    <div class="field">
				      <!-- <label>Product Name <span style="font-size: 10px;">(Start typing)</span></label> -->
				      <div id="product_search" class="ui fluid category search">
				          <div class="ui icon large input">
				            <input id="product_input" class="prompt" type="text" placeholder="@lang('receiving.receiving_search_placeholder')" class="width">
				            <i class="search icon"></i>
				          </div>
				          <div class="results"></div>
				      </div>
				    </div>
				<!-- </div> -->
				</form>
				<div class="paper-cut">
				<div class="ui segment">
				<table class="ui very basic center aligned table">
				<thead>
					<th>@lang('general.hash')</th>
					<th>@lang('general.item')</th>
					<th>@lang('general.qty')</th>
					<th>@lang('general.cost')</th>
					<!-- <th>Discount</th> -->
					<th>@lang('receiving.receiving_total')</th>
				</thead>
				<tbody data-bind='visible: lines().length > 0, foreach: lines'>
					<tr>
						<td class="selectable positive collapsing">
				            <a href="#" data-bind='click: $parent.removeLine'><i class="green delete icon"></i></a>
				        </td>
						<td><span data-bind='text: title' > </span>
						<div class="product_details">@lang('receiving.receiving_in_stock') <span data-bind='text: stockQuantity'></span></div></td>
						<td>
						<div class="ui small input">
							<input style="width: 50px;" data-bind='textInput: quantity' type="text"/>
						</div>
						</td>
						<td>{{$AppConfig->currency_symbol}}<span data-bind='text: accounting.formatMoney(unit_cost_price,"")' > </span></td>
						<!-- <td><span data-bind='text: discount' ></span>%</td> -->
						<td>{{$AppConfig->currency_symbol}}<span data-bind='text: accounting.formatMoney(subtotal(),"")' > </span></td>
					</tr>
				</tbody>
				<tbody data-bind='visible: lines().length == 0'>
					<tr>
						<td colspan="6">@lang('receiving.receiving_cart')</td>
					</tr>
				</tbody>
				</table>
				</div>
				</div>
			</div>
			<div class="six wide column">
				<div class="ui segment">

				<!-- <div class="column" style="margin-bottom: 10px;">
					<button data-bind="enable: lines().length > 0, click: resetCart" class="ui tiny orange button">Reset  Cart</button>
					<button data-bind="enable: showSupplier, click: removeSupplier" class="ui tiny teal button">Detach Supplier</button>
				</div> -->

				<div class="ui centered grid" style="margin: 10px;">
				<div class="ui buttons">
					<button data-bind="enable: lines().length > 0, click: resetCart" class="ui mini orange button">@lang('general.reset')</button>
			      	<div class="or"></div>
			      	<button data-bind="enable: showSupplier, click: removeSupplier" class="ui mini teal button">@lang('general.detach')</button>
			    </div>
				</div>

				<div class="column" data-bind="ifnot: showSupplier">
					<form class="ui small equal width form">
				    <div class="field">
				      <div id="supplier_search" class="ui fluid search">
				          <div class="ui left icon large input">
				            <input class="prompt" type="text" placeholder="@lang('receiving.receiving_type_supplier_placeholder')" class="width">
				            <i class="search icon"></i>
				          </div>
				          <div class="results"></div>
				      </div>
				    </div>
					</form>
				<br/>
				</div>

				<div data-bind="visible: showSupplier">
					<div class="ui divider"></div>
						<div class="column">
							<div class="ui two column grid">
								<div class="four wide column">
									<img data-bind='visible: supplierName' src="/images/johndoe.jpg" class="ui medium circular left aligned image">
								</div>
								<div class="twelve wide column">
									<b data-bind='text: supplierName'></b> <br/>
									<address data-bind='text: supplierAddress'></address>
									<address data-bind='text: supplierMobile'></address>
								</div>
							</div>
					</div>
					<div class="ui divider"></div>
					<br/>
				</div>

				<div class="row">
					<table class="ui very basic table">
						<thead>
							<th>@lang('receiving.receiving_item_tiers') </th>
							<th>@lang('receiving.receiving_amount')</th>
						</thead>
						<tbody>
							<tr>
								<td>@lang('receiving.receiving_sub_total')</td>
								<td>{{$AppConfig->currency_symbol}}<span data-bind='text: accounting.formatMoney(grandSubTotal(),"")'></span></td>
							</tr>
							<!-- <tr>
								<td>Discount</td>
								<td>{{$AppConfig->currency_symbol}}<span data-bind='text: getDiscount()'></span></td>
							</tr> -->
						</tbody>
					</table>
				</div>
				<div class="ui small center statistics">
					<div class="statistic">
				      	<div class="value">
				      		{{$AppConfig->currency_symbol}}<span data-bind='text: accounting.formatMoney(grandTotal(), "")'> </span>
				      	</div>
				      	<div class="label">
				      		@lang('general.total')
				      	</div>
				    </div>
			    </div>

				<!-- <div class="row">
					<form class="ui small equal width form">
				    <div class="field">
				      <div class="ui right action input">
				      	<input type="text" placeholder="Discount Code">
				      	<div class="ui teal button">
				        	Apply
				      	</div>
				    </div>
				    </div>
					</form>
			    </div> -->
			    @if($AppConfig->supports_warehouse)
			    <div class="ui red segment">
				    <h5 class="ui horizontal divider">@lang('receiving.receiving_location')</h5>
				    <span data-bind="visible: false, text: location_type" class=''></span>
				    <div class="ui centered grid" style="margin: 10px;">
						<div class="ui buttons">
							<button id="stock" data-bind="click: toogleLocation" class="ui mini positive button">@lang('receiving.receivings_stock')</button>
					      	<div class="or"></div>
					      	<button id="warehouse" data-bind="click: toogleLocation" class="ui mini button">@lang('receiving.receiving_store')</button>
					    </div>
					</div>
					<div style="margin-top: 30px;" data-bind="visible: location_type() == 'WAREHOUSE'">
						{!! Form::open(array('url' => '#', 'method'=>'GET', 'class'=>'ui small equal width form')) !!}
				      	<div class="field">
				          <!-- <label>Warehouse</label> -->
				          {!! Form::select('warehouse_id', $warehouses, null, array('data-bind' => 'value: selectedLocation', 'placeholder' => Lang::get('receiving.receiving_pick_a_store'), 'id' => 'warehouse_id', 'class' => 'ui fluid dropdown')) !!}
				        </div>
				        {!! Form::close() !!}
				    </div>
				</div>
				@endif

			    <div class="ui blue segment" style="padding-left: 0px !important;">
				    <h5 class="ui horizontal divider">@lang('receiving.receiving_payment_type')</h5>
				    <span data-bind="visible: false, text: payment_type" class=''></span>
				    <div class="ui centered grid" style="margin: 10px;">
						<div class="ui buttons" style="padding-left: 5px;">
							<button id="cash" data-bind="click: tooglePaytypeCash" class="ui tiny positive button">
							<i class="icon bitcoin"></i>
							@lang('receiving.receiving_cash')</button>
							<button id="transfer" data-bind="click: tooglePaytypeTransfer"  class="ui tiny button">
							<i class="paper plane outline icon"></i>
							USSD
							</button>
							<button id="debt" data-bind="click: tooglePaytypeCredit" class="ui tiny button">
							<i class="icon money"></i>
							</button>
						</div>
						<!--
						<div class="ui buttons">
							<button id="cash" data-bind="click: tooglePaytype" class="ui mini positive button">
							<i class="icon bitcoin"></i>
							@lang('receiving.receiving_cash')</button>
							<button id="debt" data-bind="click: tooglePaytype" class="ui mini button">
							<i class="icon money"></i>
							@lang('receiving.receiving_credit')</button>
						</div>
						-->
					</div>
				</div>

			    <div class="field">
			    	<button data-bind="enable: (showSupplier() && lines().length > 0) && (location_type() == 'WAREHOUSE' && selectedLocation || location_type() == 'STOCK'), click: save" class="ui blue right floated button"  style="background-color: #497093; border-color: #3e5f7d">@lang('receiving.receiving_submit_receiving')</button>
			    	<div style="clear:right;"></div>
			    </div>


			    <!-- Version 2.0
			    <div class="ui centered grid" style="margin-bottom: 10px;">
				    <div class="ui buttons">
						<button data-bind="enable: showSupplier() && lines().length > 0, click: save" class="ui mini teal button">Purchase Order</button>
				      	<div class="or"></div>
				      	<button data-bind="enable: showSupplier() && lines().length > 0, click: save" class="ui mini teal button">Receive Order</button>
				    </div>
			    </div> 
			    -->

				</div>
			</div>
		</div>
    </div>

    <div id="validation" class="ui small modal">
    	<i class="close icon"></i>
    	<div class="header">@lang('receiving.receiving_modal_header')</div>
		<div class="content">
			<p>@lang('receiving.receiving_modal_content')</p>
  		</div>
  		<div class="actions">
    		<div class="ui negative button">@lang('receiving.receiving_action')</div>
  		</div>
	</div>

	<div id="qtyExceeded" class="ui small modal">
    	<i class="close icon"></i>
    	<div class="header">@lang('receiving.receiving_modal_header')</div>
		<div class="content">
			<p>@lang('receiving.receiving_modal_content')</p>
  		</div>
  		<div class="actions">
    		<div class="ui negative button">@lang('receiving.receiving_action')</div>
  		</div>
	</div>

	<div id="notInStock" class="ui small modal">
    	<i class="close icon"></i>
    	<div class="header">@lang('receiving.receiving_modal_header')</div>
		<div class="content">
			<p>@lang('receiving.receiving_modal_stock_content')</p>
  		</div>
  		<div class="actions">
    		<div class="ui negative button">@lang('receiving.receiving_action')</div>
  		</div>
	</div>

	<script type="text/javascript">
    $( document ).ready(function() {
      $('.ui.dropdown').dropdown({
          on: 'hover'
        });
      
		// $('.ui.radio.checkbox').checkbox();
  //     	$('#taxType').dropdown({ on: 'click'});
    });
    </script>
@stop