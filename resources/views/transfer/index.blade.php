@extends('layouts.admin_dashboard')

@section('scripts')
  <script type="text/javascript" src="/js/jquery.dataTables.min.js"></script>
  <script type="text/javascript" src="/js/dataTables.semanticui.min.js"></script>
  <script type="text/javascript" src="/uilib/semantic.min.js"></script>
    <script>
  $(document)
    .ready(function() {
      $('.ui.form')
        .form({
          fields: {
            source_account: {
              identifier  : 'source_account',
              rules: [
                {
                  type   : 'empty',
                  prompt : "@lang('jsvalidation.transfer_source_empty')"
                },
              ]
            },
            destination_account: {
              identifier  : 'destination_account',
              rules: [
                {
                  type   : 'empty',
                  prompt : "@lang('jsvalidation.transfer_destination_empty')"
                },
              ]
            },
            amount: {
              identifier  : 'amount',
              rules: [
                {
                  type   : 'empty',
                  prompt : "@lang('jsvalidation.transfer_amount_empty')"
                },
                {
                  type   : 'number',
                  prompt : "@lang('jsvalidation.transfer_number_empty')"
                }
              ]
            },
            comment: {
              identifier  : 'comment',
              rules: [
                {
                  type   : 'empty',
                  prompt : "@lang('jsvalidation.transfer_comment_empty')"
                }
              ]
            }
          }
        })
      ;
    })
  ;
  </script>
@stop

@section('stylesheets')
  <link rel="stylesheet" type="text/css" href="/uilib/semantic.min.css"/>
  <link rel="stylesheet" type="text/css" href="/css/dataTables.semanticui.min.css"/>
@stop

@section('content')
    {!! Breadcrumbs::render('transfer_index') !!}
    <h4 class="ui dividing header">@Lang('transfer.transfer_header')</h4>

    {!! Form::open(array('url' => '/dashboard/admin/transfer', 'method'=>'POST', 'class'=>'ui small equal width form')) !!}
    
      @if (count($errors) > 0)
        <div class="ui message" style="color:#9F3A38;font-size: 1em;box-shadow: 0px 0px 0px 1px #E0B4B4 inset, 0px 0px 0px 0px transparent; background-color: #FFF6F6;">
            <ul class="list">
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
      @endif
      
      <div class="fields">
        <div class="required field">
          <label>@lang('transfer.transfer_source_account')</label>
          {!! Form::select('source_account', $accounts, null, array('placeholder' => Lang::get('transfer.transfer_source_account_placeholder'), 'class' => 'ui fluid dropdown')) !!}
        </div>
        <div class="required field">
          <label>@lang('transfer.transfer_destination_account')</label>
          {!! Form::select('destination_account', $accounts, null, array('placeholder' => Lang::get('transfer.transfer_source_account_placeholder'), 'class' => 'ui fluid dropdown')) !!}
        </div>
      </div>

      <div class="fields">
        <div class="required field">
          <label>@lang('general.amount') ({{html_entity_decode($AppConfig->currency_symbol, 0, 'UTF-8')}}):</label>
          <input name="amount" placeholder="256390.00" type="text">
        </div>
      </div>

      <div class="fields">
        <div class="required field">
          <label>@lang('transfer.transfer_comment')</label>
          <textarea name="comment" type="text"></textarea>
        </div>
      </div>

      <span><input type="submit" name="submit" class="ui primary button" value="@lang('transfer.transfer')"></span>
    <div class="ui error message"> </div>
    {!! Form::close() !!}

    <h4 class="ui dividing header">@lang('transfer.transfer_history_header')</h4>
    
    <table class="ui small celled table display" id="dtable">
      <thead>
        <th>@lang('transfer.transfer_source_account')</th>
        <th>@lang('transfer.transfer_destination_account')</th>
        <th>@lang('general.amount') ({{html_entity_decode($AppConfig->currency_symbol, 0, 'UTF-8')}})</th>
        <th>@lang('transfer.transfer_comment')</th>
        <th>@lang('general.created_by')</th>
        <th>@lang('general.created_at')</th>
      </thead>
      <tbody>
          
        @foreach ($data as $item)
            <tr>
              <td>{{ $item->source_account->title}}</td>
              <td>{{ $item->destination_account->title}}</td>
              <td>{{number_format($item->amount, 2)}}</td>
              <td>{{ $item->comment}}</td>
              <td>{{ $item->user->name}}</td>
              <td>{{ $item->created_at}}</td>
            </tr>
        @endforeach
      </tbody>
    </table>
    
    <script type="text/javascript">
    $( document ).ready(function() {
      $('.ui.dropdown').dropdown({
          on: 'hover'
        });
      $('#dtable').DataTable();
    });
    </script>
    </script>
@stop