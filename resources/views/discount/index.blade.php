@extends('layouts.admin_dashboard')

@section('scripts')
  <script type="text/javascript" src="/js/jquery.dataTables.min.js"></script>
  <script type="text/javascript" src="/js/dataTables.semanticui.min.js"></script>
  <script type="text/javascript" src="/uilib/semantic.min.js"></script>
@stop

@section('stylesheets')
  <link rel="stylesheet" type="text/css" href="/uilib/semantic.min.css"/>
  <link rel="stylesheet" type="text/css" href="/css/dataTables.semanticui.min.css"/>
@stop

@section('content')
    {!! Breadcrumbs::render('discount_list') !!}
    <h4 class="ui dividing header">Apply Discounts</h4>
    {!! Form::open(array('url' => '/dashboard/admin/discount', 'method'=>'POST', 'class'=>'ui small equal width form', 'id'=>"couponForm")) !!}

    @if (count($errors) > 0)
        <div class="ui message" style="color:#9F3A38;font-size: 1em;box-shadow: 0px 0px 0px 1px #E0B4B4 inset, 0px 0px 0px 0px transparent; background-color: #FFF6F6;">
            <ul class="list">
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif

    <div class="fields">
    <div class="field">
      <label>Coupon <span style="font-size: 10px;">(optional)</span></label>
        <input type="text" name="coupon"/>
    </div>
    <div class="field">
        <label>Frequency (optional)</label>
        <input type="text" name="frequency"/>
    </div>
   <div class="required field">
        <label>Validity (no. of days)</label>
        <input type="text" name="expired_in"/>
    </div>
  </div>

  <div class="fields">
    <div class="field">
      <label>Min. Qty <span style="font-size: 10px;">(Leave empty for any amount)</span></label>
        <input type="text" name="threshold_quantity"/>
    </div>

   <div class="required field">
          <label>Discount Amount (< 1 = %):</label>
          <div class="ui right labeled input">
            <input type="text" name="amount"/>
            <input name="type" value="percentage" type="hidden">
        </div>
    </div>
  </div>

    <div class="field">
        <label>Description</label>
        <textarea rows="2" name="description"></textarea>
    </div>
    <p>Please select the products to apply discounts</p>
    <table class="ui small celled table display" id="dtable">
      <thead>
        <th>
          <div class="ui checkbox">
            <input class="hidden" name="selectAll" type="checkbox" id="checkAll"/>
            <label for="">All</label>
          </div>
        </th>
        <th>Title</th>
        <th>Category</th>
        <!-- <th>Description</th> -->
        <th>Selling Price</th>
        <th>Cost Price</th>
        <!-- <th>Created By</th> -->
        <!-- <th>Created At</th> -->
        <!-- <th>Updated At</th> -->
      </thead>
      <tbody>
          
        @foreach ($data as $item)
            <tr>
              <td>
                  <div class="inline field">
                    <div class="ui checkbox">
                      <input class="hidden" type="checkbox" name="selected[]" value="{{ $item->id }}" />
                    </div>
                  </div>
              </td>
              <td><!--<a href="/user/{{ $item->id}}">-->{{ $item->title}}<!--</a>--></td>
              <td>{{ $item->category->title}}</td>
              <!-- <td>{{ $item->description}}</td> -->
              <td>{{ $item->unit_selling_price}}</td>
              <td>{{ $item->unit_cost_price}}</td>
              <!-- <td>{{ $item->user->name}}</td> -->
              <!-- <td>{{ $item->created_at}}</td> -->
              <!-- <td>{{ $item->updated_at}}</td> -->
            </tr>
        @endforeach
      </tbody>
    </table>

    <br/>
    <button class="ui positive button" type="submit" name="apply_discount" value="Approve">Apply Discount</button>
    <input type="hidden" name="action" value="" id="action"/>

    {!! Form::close() !!}

    <div class="ui small modal positive">
      <i class="close icon"></i>
      <div class="header">
        Confirm Submission
      </div>
      <div class="content">
        <div class="description">
          <div class="ui header">Apply Discounts</div>
          <p>Are you sure you want to apply discounts on the selected products </p>
        </div>
      </div>
      <div class="actions">
        <div class="ui black deny button">
          Cancel
        </div>
        <div id="positive_continue" class="ui positive right labeled icon button">
          Continue >>
          <i class="checkmark icon"></i>
        </div>
      </div>
    </div>

    <script type="text/javascript">
    $( document ).ready(function() {
      
        $('.ui.dropdown').dropdown({
          on: 'hover'
        });

        $("#checkAll").change(function () {
            $("input:checkbox").prop('checked', $(this).prop("checked"));
        });
        
        $('.ui.checkbox').checkbox();
        $('#dtable').DataTable();
        // $("#couponForm").submit(function(e){
        //   e.preventDefault();
        //   var btnClicked = $('input[type="submit"], button[type="submit"]',this).filter(':focus').attr('name');
        //   if(btnClicked == "apply_discount"){
        //     $('.ui.small.modal.positive').modal('show');
        //   }
        // });
        
        // $('#positive_continue').click(function(e){
        //   $('#action').val('approve');
        //   document.getElementById('form').submit();
        // });
    });
    </script>
@stop