@extends('layouts.cashier_dashboard')

@section('scripts')
  <!-- <script type="text/javascript" src="/uilib/semantic.min.js"></script> -->
  <script type="text/javascript" src="/js/semanticAjaxHack.js"></script>
@stop

@section('stylesheets')
  <link rel="stylesheet" type="text/css" href="/uilib/semantic.min.css"/>
@stop

@section('content')
    {!! Breadcrumbs::render('cashier_dashboard') !!} 
@stop