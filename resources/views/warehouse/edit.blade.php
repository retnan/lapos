@extends('layouts.admin_dashboard')

@section('scripts')
  <script type="text/javascript" src="/uilib/semantic.min.js"></script>
  <script>
  $(document)
    .ready(function() {
      $('.ui.form')
        .form({
          fields: {
            title: {
              identifier  : 'title',
              rules: [
                {
                  type   : 'empty',
                  prompt : "@lang('jsvalidation.warehouse_name_empty')"
                },
              ]
            },
            address: {
              identifier  : 'address',
              rules: [
                {
                  type   : 'empty',
                  prompt : "@lang('jsvalidation.warehouse_address')"
                },
              ]
            },
          }
        })
      ;
    })
  ;
  </script>
@stop

@section('stylesheets')
  <link rel="stylesheet" type="text/css" href="/uilib/semantic.min.css"/>
@stop

@section('content')
    {!! Breadcrumbs::render('warehouse_edit', $data) !!}
   {!! Form::open(array('url' => '/dashboard/admin/warehouse/' . $data->id, 'method'=>'PUT', 'class'=>'ui small equal width form')) !!}
      <h4 class="ui dividing header">@lang('warehouse.warehouse_edit_information_header')</h4>

      @if (count($errors) > 0)
        <div class="ui message" style="color:#9F3A38;font-size: 1em;box-shadow: 0px 0px 0px 1px #E0B4B4 inset, 0px 0px 0px 0px transparent; background-color: #FFF6F6;">
            <ul class="list">
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
      @endif
      
      <div class="fields">
        <div class="required field">
          <label>@lang('warehouse.warehouse_name')</label>
          <input value="{{ old('title') ? old('title') : $data->title }}" name="title" placeholder="@lang('warehouse.warehouse_name_placeholder')" type="text">
        </div>
      </div>
      
      <div class="fields">
        <div class="required field">
          <label>@lang('general.address')</label>
          <textarea name="address" type="text">{{ old('address') ? old('address') : $data->address }}</textarea>
        </div>
      </div>
      
    <span><input type="submit" name="submit" class="ui primary button" value="@lang('warehouse.warehouse_update')"></span>
    <span><a href="/dashboard/admin/warehouse/{{$data->id}}/delete" id="delete_record" class="ui red button">@lang('general.delete_record')</a></span>
    <div class="ui error message"> </div>
    {!! Form::close() !!}


    <div class="ui small modal">
  <i class="close icon"></i>
  <div class="header">
    @lang('general.delete_record_header')
  </div>
  <div class="content">
    <p>@lang('general.delete_record_content')</p>
  </div>
  <div class="actions">
    <div class="ui negative button">
        @lang('general.delete_record_no')
    </div>
    <a href="/dashboard/admin/warehouse/{{$data->id}}/delete" class="ui positive button">
        <i class="checkmark icon"></i>
        @lang('general.delete_record_yes')
      </a>
  </div>
</div>
    
    <script type="text/javascript">
    $( document ).ready(function() {
		  $('.ui.radio.checkbox').checkbox();
      $('#delete_record').click(function(e) {
          e.preventDefault();
          $('.ui.small.modal').modal('show');
      });
    });
    </script>
@stop