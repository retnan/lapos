@extends('layouts.admin_dashboard')

@section('scripts')
  <script type="text/javascript" src="/js/jquery.dataTables.min.js"></script>
  <script type="text/javascript" src="/js/dataTables.semanticui.min.js"></script>
  <script type="text/javascript" src="/uilib/semantic.min.js"></script>
  <script type="text/javascript" src="/js/lapos_autocomplete.js"></script>
@stop

@section('stylesheets')
  <link rel="stylesheet" type="text/css" href="/uilib/semantic.min.css"/>
  <link rel="stylesheet" type="text/css" href="/css/dataTables.semanticui.min.css"/>
@stop

@section('content')
    {!! Breadcrumbs::render('warehouse_show', $warehouse) !!}
    <h4 class="ui dividing header">@lang('warehouse.warehouse_items_header')</h4>
<!--
    {!! Form::open(array('url' => '/dashboard/admin/warehouse/add_product', 'method'=>'POST', 'class'=>'ui small equal width form', 'id'=>"form")) !!}

    @if (count($errors) > 0)
        <div class="ui message" style="color:#9F3A38;font-size: 1em;box-shadow: 0px 0px 0px 1px #E0B4B4 inset, 0px 0px 0px 0px transparent; background-color: #FFF6F6;">
            <ul class="list">
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif

  <div class="fields">
    <div class="required field">
      <label>Product Name <span style="font-size: 10px;">(Start typing)</span></label>
      <input type="hidden" value="" id="product_id" name="product_id">
      <input type="hidden" value="" id="title" name="title">
      <div id="product_search" class="ui fluid category search">
          <div class="ui icon input">
            <input class="prompt" type="text" placeholder="Search product...">
            <i class="search icon"></i>
          </div>
          <div class="results"></div>
      </div>
    </div>
  </div>

  <div class="fields">
    <div class="field">
        <label>Category</label>
        <input id="category" type="text" disabled="disabled" value="" />
    </div>
   <div class="required field">
        <label>Warehouse</label>
        <select name="type" disabled="disabled">
          <option value="">{{$warehouse->title}}</option>
        </select>
        <input type="hidden" name="warehouse_id" value="{{$warehouse->id}}">
    </div>
   <div class="field">
        <label>Cost price ({{html_entity_decode($AppConfig->currency_symbol, 0, 'UTF-8')}})</label>
        <input  id="unit_cost_price" disabled="disabled" type="text" value="" />
    </div>
  </div>

  <div class="field">
        <label>Description</label>
        <textarea id="description" rows="2" disabled="disabled"></textarea>
    </div>

  <div class="fields">
    <div class="required field">
      <label>Quantity</label>
        <input type="text" name="quantity"/>
    </div>
    <div class="required field">
        <label>Supplier <span style="font-size: 10px;">(Start typing)</span></label>
      <input type="hidden" value="" id="supplier_id" name="supplier_id">
      <input type="hidden" value="" id="supplier_title">
      <div id="supplier_search" class="ui fluid search">
          <div class="ui icon input">
            <input class="prompt" type="text" placeholder="Search Supplier...">
            <i class="search icon"></i>
          </div>
          <div class="results"></div>
      </div>
    </div>
  </div>
    
    <div class="field">
    <button style="background-color: #497093;" class="ui blue right floated button" type="submit" name="add_product" value="Approve">Add Product</button>
    <div style="clear:right;"></div>
    </div>
    {!! Form::close() !!}
    <br/>
    -->
    <table class="ui small celled table display" id="dtable">
      <thead>
        <th>@lang('general.product')</th>
        <th>@lang('general.category')</th>
        <th>@lang('general.quantity')</th>
        <th>@lang('general.supplier')</th>
        <th>@lang('general.cost_price')</th>
        <th>@lang('general.created_by')</th>
        <th>@lang('general.created_at')</th>
        <!-- <th>Updated At</th> -->
      </thead>
      <tbody>
          
        @foreach ($data as $item)
            <tr>
              <td><!--<a href="/user/{{ $item->id}}">-->{{ $item->product->title}}<!--</a>--></td>
              <td>{{ $item->product->category->title}}</td>
              <td>{{ $item->quantity}}</td>
              <td>{{ $item->supplier->title}}</td>
              <td>{{ $item->product->unit_cost_price}}</td>
              <td>{{ $item->user->name}}</td>
              <td>{{ $item->created_at}}</td>
              <!-- <td>{{ $item->updated_at}}</td> -->
            </tr>
        @endforeach
      </tbody>
    </table>
    
    <script type="text/javascript">
    $( document ).ready(function() {
      $('.ui.dropdown').dropdown({
          on: 'hover'
        });
		  $('#dtable').DataTable();
    });
    </script>
@stop