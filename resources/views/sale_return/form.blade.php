@extends('layouts.admin_dashboard')

@section('scripts')
	<script type="text/javascript">
  		var App = {
  			Currency: "{{$AppConfig->currency_symbol}}",
  		};
  	</script>
  <script type="text/javascript" src="/uilib/semantic.min.js"></script>
  <script type="text/javascript" src="/js/knockout-3.4.0.js"></script>
  <script type="text/javascript" src="/js/accounting.min.js"></script>
  <script type="text/javascript" src="/js/sale_return.js"></script>
@stop

@section('stylesheets')
  <link rel="stylesheet" type="text/css" href="/uilib/semantic.min.css"/>
  <style type="text/css">
  .product_details{
  	font-style: italic;
  	font-size: 11px;
  	font-weight: bold;
  	color: green;
  }
   .paper-cut:after {
	  content: " ";
	  display: block;
	  position: relative;
	  top: 0px;
	  left: 0px;
	  width: 100%;
	  height: 36px;
	  background: -webkit-linear-gradient(#FFFFFF 0%, transparent 0%), -webkit-linear-gradient(135deg, #e8ebf1 33.33%, transparent 33.33%) 0 0%, #e8ebf1 -webkit-linear-gradient(45deg, #e8ebf1 33.33%, #FFFFFF 33.33%) 0 0%;
	  background: -o-linear-gradient(#FFFFFF 0%, transparent 0%), -o-linear-gradient(135deg, #e8ebf1 33.33%, transparent 33.33%) 0 0%, #e8ebf1 -o-linear-gradient(45deg, #e8ebf1 33.33%, #FFFFFF 33.33%) 0 0%;
	  background: -moz-linear-gradient(#FFFFFF 0%, transparent 0%), -moz-linear-gradient(135deg, #e8ebf1 33.33%, transparent 33.33%) 0 0%, #e8ebf1 -moz-linear-gradient(45deg, #e8ebf1 33.33%, #FFFFFF 33.33%) 0 0%;
	  background-repeat: repeat-x;
	  background-size: 0px 100%, 14px 27px, 14px 27px;
}
  </style>
@stop

@section('content')
	{!! Breadcrumbs::render('get_sale_return') !!}

	@if (count($errors) > 0)
        <div class="ui message" style="color:#9F3A38;font-size: 1em;box-shadow: 0px 0px 0px 1px #E0B4B4 inset, 0px 0px 0px 0px transparent; background-color: #FFF6F6;">
            <ul class="list">
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif

    <div id="loader" class="ui page inverted dimmer">
      <div class="content">
        <div class="center">
            <div class="ui active text loader">@lang('general.loader')</div>
        </div>
      </div>
    </div>

    <br/>


	<div class="column">
		<div class="ui two column grid">
			<div class="ten wide column">
				<form id="shopping_cart" class="ui small equal width form">
				<!-- <div class="fields"> -->
				    <div class="field">
				      <!-- <label>Product Name <span style="font-size: 10px;">(Start typing)</span></label> -->
				      <div id="order_search" class="ui fluid category search">
				          <div class="ui icon large input">
				            <input id="order_receipt_input" class="prompt" type="text" placeholder="Start typing Receipt # or Order #..." class="width">
				            <i class="search icon"></i>
				          </div>
				          <div class="results"></div>
				      </div>
				    </div>
				<!-- </div> -->
				</form>
				<div class="paper-cut">
				<div class="ui segment">
				<table class="ui very basic center aligned table">
				<thead>
					<!-- <th>#</th> -->
					<th>@lang('general.item')</th>
					<th>@lang('form.return_qty')</th>
					<th>@lang('general.price')</th>
					<!-- <th>Discount</th> -->
					<th>@lang('general.total')</th>
					<th>@lang('general.refund')</th>
				</thead>
				<tbody data-bind='visible: lines().length > 0, foreach: lines'>
					<tr>
						<!-- <td class="selectable positive collapsing">
				            <a href="#" data-bind='enable: false'><i class="green delete icon"></i></a>
				        </td> -->
						<td><span data-bind='text: title' > </span>
						<div class="product_details">@lang('general.in_order') <span data-bind='text: orderedQuantity'></span></div></td>
						<td>
						<div class="ui small input">
							<input style="width: 50px;" data-bind='enable: $parent.actual_payment_type() == "CASH" && $parent.returned_items().length == 0, textInput: newQuantity' type="text"/>
						</div>
						</td>
						<td>{{$AppConfig->currency_symbol}}<span data-bind='text: accounting.formatMoney(unit_selling_price,"")' > </span></td>
						<td>{{$AppConfig->currency_symbol}}<span data-bind='text: accounting.formatMoney(subtotal(),"")' > </span></td>
						<!-- <td><span data-bind='text: discount' ></span>%</td> -->
						<td>{{$AppConfig->currency_symbol}}<span data-bind='text: accounting.formatMoney(newSubtotal(),"")' > </span></td>
					</tr>
				</tbody>
				<tbody data-bind='visible: lines().length == 0'>
					<tr>
						<td colspan="6">@lang('general.no_cart_items')</td>
					</tr>
				</tbody>
				</table>
				</div>
				</div>
				<!-- NEXT SEGMENT -->
				<div class="ui segment" data-bind='visible: returned_items().length > 0'>
				<h3>@lang('general.items_return')</h3>
				<table class="ui very basic center aligned table">
				<thead>
					<th>@lang('general.item')</th>
					<th>@lang('general.qty')</th>
					<th>@lang('general.refund')</th>
					<th>@lang('general.date')</th>
				</thead>
				<tbody data-bind='visible: returned_items().length > 0, foreach: returned_items'>
					<tr>
						<td><span data-bind='text: title' > </span></td>
						<td><span data-bind='text: quantity'></span></td>
						<td>{{$AppConfig->currency_symbol}}<span data-bind='text: accounting.formatMoney(quantity * unit_selling_price,"")' > </span></td>
						<td><span data-bind='text: createdAt'></span></td>
					</tr>
				</tbody>
				</table>
				<div class="ui centered grid" style="margin: 10px;">
					<a href="/dashboard/admin/return/sales/receipt_number/?q=printable" data-bind='' id="print" class="tiny ui button">@lang('general.print')</a>
				</div>
				</div>
				<!-- END SEGMENT -->

			</div>
			<div class="six wide column">
				<div class="ui segment">

				<div data-bind="visible: showCustomer">
					<div class="ui divider"></div>
						<div class="column">
							<div class="ui two column grid">
								<div class="four wide column">
									<img data-bind='visible: customerName' src="/images/johndoe.jpg" class="ui medium circular left aligned image">
								</div>
								<div class="twelve wide column">
									<b data-bind='text: customerName'></b> <br/>
									<address data-bind='text: customerAddress'></address>
									<address data-bind='text: customerMobile'></address>
								</div>
							</div>
					</div>
					<div class="ui divider"></div>
					<br/>
				</div>

				<div class="row">
					<table class="ui very basic table">
						<thead>
							<th>@lang('general.item_tiers') Tiers</th>
							<th>@lang('general.amount')</th>
						</thead>
						<tbody>
							<tr>
								<td>@lang('general.sub_total')</td>
								<td>{{$AppConfig->currency_symbol}}<span data-bind='text: accounting.formatMoney(grandSubTotal(),"")'></span></td>
							</tr>
							<!-- <tr>
								<td>Discount</td>
								<td>{{$AppConfig->currency_symbol}}<span data-bind='text: getDiscount()'></span></td>
							</tr> -->
						</tbody>
					</table>
				</div>

			    <div class="ui small center statistics">
					<div class="statistic">
				      	<div class="value">
				      		{{$AppConfig->currency_symbol}}<span data-bind="style: { 'text-decoration': deposit_amount() > 0 ? 'line-through' : 'none'}, text: accounting.formatMoney(grandTotalAfterDiscount(), '')"> </span>
				      	</div>
				      	<div class="label">
				      		@lang('general.total')
				      	</div>
				    </div>
			    </div>

			    <div class="ui mini center statistics" data-bind="visible: actual_payment_type() == 'CREDIT'">
					<div class="statistic">
				      	<div class="value">
				      		{{$AppConfig->currency_symbol}}<span data-bind='text: accounting.formatMoney(grandTotalAfterDiscount() - deposit_amount(), "")'> </span>
				      	</div>
				      	<div class="label">
				      		@lang('general.total_credit')
				      	</div>
				    </div>
			    </div>

			    @if($AppConfig->supports_warehouse)
			    <div class="ui red segment">
				    <h5 class="ui horizontal divider">@lang('form.return_item') </h5>
				    <span data-bind="visible: false, text: location_type" class=''></span>
				    <div class="ui centered grid" style="margin: 10px;">
						<div class="ui buttons">
							<button id="stock" data-bind="enable: returned_items().length == 0 && actual_payment_type() == 'CASH' && lines().length > 0, click: toogleLocation" class="ui mini positive button">@lang('general.stock')</button>
					      	<div class="or"></div>
					      	<button id="warehouse" data-bind="enable: false && actual_payment_type() == 'CASH' && lines().length > 0, click: toogleLocation" class="ui mini button">@lang('general.store')</button>
					    </div>
					</div>
					<div style="margin-top: 30px;" data-bind="visible: location_type() == 'WAREHOUSE'">
						{!! Form::open(array('url' => '#', 'method'=>'GET', 'class'=>'ui small equal width form')) !!}
				      	<div class="field">
				          <!-- <label>Warehouse</label> -->
				          {!! Form::select('warehouse_id', $warehouses, null, array('data-bind' => 'value: selectedLocation', 'placeholder' => Lang::get('receiving.receiving_pick_a_store'), 'id' => 'warehouse_id', 'class' => 'ui fluid dropdown')) !!}
				        </div>
				        {!! Form::close() !!}
				    </div>
				</div>
				@endif

			    <div class="ui blue segment">
				    <h5 class="ui horizontal divider">@lang('general.return_method')</h5>
				    <span data-bind="visible: false, text: payment_type()"></span>
				    <div class="ui centered grid" style="margin-top: 10px;">
						<div class="ui buttons">
							<button id="cash" data-bind="enable: returned_items().length == 0 && actual_payment_type() == 'CASH' && lines().length > 0, click: tooglePaytype" class="ui mini positive button">
							<i class="icon bitcoin"></i>
							@lang('general.cash')</button>
							<button id="debt" data-bind="enable: false && actual_payment_type() == 'CASH' && lines().length > 0, click: tooglePaytype" class="ui mini button">
							<i class="icon money"></i>
							@lang('general.credit')</button>
						</div>
						<div class="ui mini center statistics">
							<div class="statistic">
						      	<div class="value">
						      		{{$AppConfig->currency_symbol}}<span data-bind='text: totalRefund() != 0 ? accounting.formatMoney(totalRefund(), "") : accounting.formatMoney(grandNewTotalAfterDiscount(), "")'> </span>
						      	</div>
						      	<div class="label">
						      		@lang('general.refund_total')
						      	</div>
						    </div>
					    </div>
					</div>
				</div>

			    <div class="ui centered grid" style="margin: 10px;">
			    	<button data-bind="enable: (actual_payment_type() == 'CASH' &&grandNewTotalAfterDiscount() > 0 && showCustomer() && lines().length > 0) && (location_type() == 'WAREHOUSE' && selectedLocation || location_type() == 'STOCK'), click: save" class="ui blue right floated button"  style="background-color: #497093; border-color: #3e5f7d">@lang('general.return_items')</button>
			    	<div style="clear:right;"></div>
			    </div>

			    <div data-bind="visible: actual_payment_type() == 'CREDIT'">
			    	<div class="ui divider"></div>
			    	<div class="ui label" style="margin-top: 10px;">@lang('general.sales_return')</div>
			    </div>

				</div>
			</div>
		</div>
    </div>

    <div id="validation" class="ui small modal">
    	<i class="close icon"></i>
    	<div class="header">@lang('pos.pos_info_header')</div>
		<div class="content">
			<p>@lang('pos.pos_info_content')</p>
  		</div>
  		<div class="actions">
    		<div class="ui negative button">@lang('pos.pos_ok')</div>
  		</div>
	</div>

	<div id="qtyExceeded" class="ui small modal">
    	<i class="close icon"></i>
    	<div class="header">@lang('pos.pos_info_header')</div>
		<div class="content">
			<p>@lang('pos.pos_info_content')</p>
  		</div>
  		<div class="actions">
    		<div class="ui negative button">@lang('pos.pos_ok')</div>
  		</div>
	</div>

	<div id="notInStock" class="ui small modal">
    	<i class="close icon"></i>
    	<div class="header">@lang('pos.pos_info_header')</div>
		<div class="content">
			<p>@lang('pos.pos_not_in_stock')</p>
  		</div>
  		<div class="actions">
    		<div class="ui negative button">@lang('pos.pos_ok')</div>
  		</div>
	</div>

	<script type="text/javascript">
    $( document ).ready(function() {
      $('.ui.dropdown').dropdown({
          on: 'hover'
        });
      
		// $('.ui.radio.checkbox').checkbox();
  //     	$('#taxType').dropdown({ on: 'click'});
    });
    </script>
@stop