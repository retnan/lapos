@extends($isPrintable ? 'layouts.printable' : 'layouts.admin_dashboard')

@section('scripts')
	<script type="text/javascript" src="/uilib/semantic.min.js"></script>
	<script type="text/javascript">
    $( document ).ready(function() {
        $('#print').click(function(e){
          	var printWindow = window.open($(this).attr('href'), null, "height=650,width=900");
          	printWindow.focus();
          	printWindow.print();
          	e.preventDefault();
        });
    });
    </script>
</script>  
@stop

@section('stylesheets')
  <link rel="stylesheet" type="text/css" href="/uilib/semantic.min.css"/>
@stop

@section('content')
	@if(!$isPrintable)
		{!! Breadcrumbs::render('sale_return_receipt', $data, Auth::user()->access_role) !!}
	@endif
	<div class="column @if (!$isPrintable) ui segment @endif ">
		<div class="ui two column grid">
			<div class="eight wide column">
				<img src="/assets/images/lapos.png" height="80" style="padding-top: 10px;">
				<!-- <h1>{{$AppConfig->company_name}}</h1> -->
			</div>
			<div class="eight float right aligned wide column">
			<!-- BarCode Here -->
			<b>{{$AppConfig->company_name}}</b><br/> 
			{{$AppConfig->address}}<br/>
			{{$AppConfig->email}}<br/>
			{{$AppConfig->mobile_number}}<br/>
			</div>
		</div>
		<h2>@lang('general.sales_return_reciept')</h2>
		<div class="ui two column grid">
			<div class="eight wide column">
				<?php echo ($data->payment_type == "CASH" ? trans('general.reciept') : 'Invoice' ); ?> # {{$data->receipt_number}} <br/>
				Order # {{str_pad($data->id, 13, "0", STR_PAD_LEFT)}} <br/>
				{{ trans('pos.reciept_date') }} #: {{$data->created_at->format('d/m/Y')}} <br/>
				<br/>
				<b>@lang('pos.receipt_refund_method')</b> <br/>
				<b>{{$data->payment_type}}</b>
			</div>
			<div class="eight wide column">
				<b>@lang('pos.receipt_sold_to')</b>: <br/>
				{{$data->customer->title}} <br/>
				{{$data->customer->address}} <br/>
				{{$data->customer->mobile_number}}
			</div>
		</div>
		
		<div class="ui centered grid">
			<h4>@lang('general.return_items')</h4>
			<table class="ui very basic table">
				<thead>
					<th>@lang('general.product')</th>
					<th>@lang('general.category')</th>
					<th>@lang('general.price')</th>
					@if($AppConfig->supports_tax)
					<th>@lang('general.tax')</th>
					@endif
					<th>@lang('general.qty')</th>
				</thead>
				<tbody>
				<?php $subtotal = 0; ?>
				<?php $taxtotal = 0; ?>
				@foreach($data->sale_returns as $item)
				<?php $subtotal += $item->unit_selling_price * $item->quantity; ?>
				<?php $taxtotal += ($item->tax * ($item->unit_selling_price * $item->quantity)) / 100; ?>
					<tr>
						<td>{{$item->product->title}}</td>
						<td>{{$item->product->category->title}}</td>
						<td>{{$AppConfig->currency_symbol}}{{number_format($item->unit_selling_price, 2)}}</td>
						@if($AppConfig->supports_tax)
						<td>{{$item->tax}}%</td>
						@endif
						<td>{{$item->quantity}}</td>
					</tr>
				@endforeach
				</tbody>
			</table>			
		</div>

		<div class="ui divider"></div>
		<div class="ui grid">
			<div class="floated left eight wide column">
				<?php echo DNS2D::getBarcodeSVG($data->id, "DATAMATRIX", 9, 9); ?>		
			</div>
			<div class="right floated eight wide column">
				<h3>@lang('general.order_totals')</h3>
				<table class="ui very basic right aligned table" style="border:none;">
					<tr>
						<td>@lang('general.order_subtotal')</td>
						<td>{{$AppConfig->currency_symbol}}{{number_format($subtotal, 2)}}</td>
					</tr>
					@if($AppConfig->supports_tax)
					<tr>
						<td>@lang('pos.reciept_sales_tax')</td>
						<td>{{$AppConfig->currency_symbol}}{{number_format($taxtotal, 2)}}</td>
					</tr>
					@endif
				</table>				
				<div class="ui divider"></div>
				@if($data->payment_type == 'CASH')
				<h4>@lang('general.total_amount_refund') {{$AppConfig->currency_symbol}}{{number_format($taxtotal + $subtotal, 2)}}</h4>
				@endif
				@if($data->payment_type == 'CREDIT')
				<!-- <h4>Amount Paid: {{$AppConfig->currency_symbol}}{{number_format(abs($data->deposit), 2)}}</h4> -->
				<h4>@lang('general.customer_account_credited') {{$AppConfig->currency_symbol}}{{number_format($taxtotal + $subtotal, 2)}}</h4>
				@endif

			</div>
		</div>

		<div class="ui divider"></div>
		<br/>
		<div class="ui centered grid">
			<h4>@lang('general.original_order')<i>{{$data->sale_order->receipt_number}}</i></h4>
			<table class="ui very basic table">
				<thead>
					<th>@lang('general.product')</th>
					<th>@lang('general.category')</th>
					<th>@lang('general.price')</th>
					@if($AppConfig->supports_tax)
					<th>@lang('general.tax')</th>
					@endif
					<th>@lang('general.qty')</th>
				</thead>
				<tbody>
				<?php $subtotal = 0; ?>
				<?php $taxtotal = 0; ?>
				@foreach($data->sale_order->sales as $item)
				<?php $subtotal += $item->unit_selling_price * $item->quantity; ?>
				<?php $taxtotal += ($item->tax * ($item->unit_selling_price * $item->quantity)) / 100; ?>
					<tr>
						<td>{{$item->product->title}}</td>
						<td>{{$item->product->category->title}}</td>
						<td>{{$AppConfig->currency_symbol}}{{number_format($item->unit_selling_price, 2)}}</td>
						@if($AppConfig->supports_tax)
						<td>{{$item->tax}}%</td>
						@endif
						<td>{{$item->quantity}}</td>
					</tr>
				@endforeach
				</tbody>
			</table>			
		</div>

		<div class="ui divider"></div>
		<div class="ui grid">
			<div class="floated left eight wide column">
				<?php echo DNS2D::getBarcodeSVG($data->sale_order->id, "DATAMATRIX", 9, 9); ?>		
			</div>
			<div class="right floated eight wide column">
				<h3>@lang('general.order_totals')</h3>
				<table class="ui very basic right aligned table" style="border:none;">
					<tr>
						<td>@lang('general.order_subtotal')</td>
						<td>{{$AppConfig->currency_symbol}}{{number_format($subtotal, 2)}}</td>
					</tr>
					@if($AppConfig->supports_tax)
					<tr>
						<td>@lang('pos.reciept_sales_tax')</td>
						<td>{{$AppConfig->currency_symbol}}{{number_format($taxtotal, 2)}}</td>
					</tr>
					@endif
					
					@if($data->sale_order->payment_type == 'CREDIT')
					<tr>
						<td>@lang('general.amount_paid')</td>
						<td>{{$AppConfig->currency_symbol}}{{number_format(abs($data->deposit), 2)}}</td>
					</tr>
					@endif
				</table>				
				<div class="ui divider"></div>
				@if($data->sale_order->payment_type == 'CASH')
				<h4>@lang('general.total_amount_paid') {{$AppConfig->currency_symbol}}{{number_format($taxtotal + $subtotal, 2)}}</h4>
				@endif
				@if($data->sale_order->payment_type == 'CREDIT')
				<!-- <h4>Amount Paid: {{$AppConfig->currency_symbol}}{{number_format(abs($data->deposit), 2)}}</h4> -->
				<h4>@lang('general.balance_due_for_payment') {{$AppConfig->currency_symbol}}{{number_format(abs($data->deposit - ($taxtotal + $subtotal)), 2)}}</h4>
				@endif

			</div>
		</div>
	</div>

	@if(!$isPrintable)
		<div class="ui centered grid" style="margin: 10px;">
			<a href="/dashboard/pos" class="ui primary button">@lang('general.back')</a>
			<a href="/dashboard/admin/return/sales/{{$data->receipt_number}}/?q=printable" id="print" class="ui teal button">Print</a>
		</div>
	@endif
@stop