$( document ).ready(function() {

$('#shopping_cart').submit(function(e){
    e.preventDefault();
});

$('#order_search').search({
    apiSettings: {
      url: '/sale_order/search/?q={query}'
    },
    // type: 'category',
    searchFullText: false,
    searchFields: [
	  'title',
	  'description'
	],
	onSelect: function(orderDetails, response){
        populateModel(orderDetails);
	},
});

var ReturnedItem = function(){
    var self = this;
    self.product_id = null;
    self.tax = 100;
    self.title = '';
    self.unit_selling_price = 0;
    self.quantity = 0;
    self.createdAt = '';
}

var CartLine = function() {
    var self = this;
    self.product_id = null
    self.maxQtyAllowed = null;
    self.tax = 100;
    self.discount = 0;
    self.title = '';
    self.unit_selling_price = 0;
    self.quantity = ko.observable(1); //no external factor to change it.
    self.newQuantity = ko.observable(0);
    self.orderedQuantity;

    self.subtotal = ko.pureComputed(function() {
        return self.title ? parseFloat( (self.unit_selling_price * parseInt('0'+self.quantity()) ).toFixed(2)) : '0'.toFixed(2);
    });
    self.taxAddition = ko.pureComputed(function() {
        return self.tax == 0 ? 0 : (self.subtotal() * self.tax * 0.01);
    });
    self.newSubtotal = ko.pureComputed(function() {
        // if(self.newQuantity() > self.orderedQuantity){
        //     self.newQuantity(parseInt(self.orderedQuantity));
        //     return;
        // }
        return self.title ? parseFloat( (self.unit_selling_price * parseInt('0'+self.newQuantity()) ).toFixed(2)) : '0'.toFixed(2);
    });
    self.newTaxAddition = ko.pureComputed(function() {
        return self.tax == 0 ? 0 : (self.newSubtotal() * self.tax * 0.01);
    });
};

var CartModel = function() {
    var self = this;
    self.showCustomer = ko.observable(false);
    self.customerId = ko.observable('');
    self.customerName = ko.observable('');
    self.customerAddress = ko.observable('');
    self.customerMobile = ko.observable('');
    self.payment_type = ko.observable('CASH'); //or CREDIT
    self.actual_payment_type =  ko.observable('CASH');
    self.location_type = ko.observable('STOCK');//or WAREHOUSE
    self.selectedLocation = ko.observable();
    self.lines = ko.observableArray();
    self.discount = 0;
    self.deposit_amount = ko.observable(0);
    self.receipt_number = -1;
    self.returned_items = ko.observableArray();
    self.totalRefund = ko.observable(0);
    self.grandSubTotal = ko.pureComputed(function() {
        var total = 0;
        $.each(self.lines(), function() { total += this.subtotal() })
        return parseFloat(total.toFixed(2));
    });
    self.grandTaxAddition = ko.pureComputed(function() {
        var total = 0;
        $.each(self.lines(), function() { total += this.taxAddition() })
        return parseFloat(total.toFixed(2));
    });
    self.grandTotal = ko.pureComputed(function() {
        return Math.abs(self.grandSubTotal() + self.grandTaxAddition());
    });

    self.getDiscount = ko.pureComputed(function(){
        return self.discount == 0 ? 0 : (self.grandTotal() * self.discount * 0.01);
    });

    self.grandTotalAfterDiscount = ko.pureComputed(function() {
        return Math.abs(self.grandSubTotal() + self.grandTaxAddition()) - self.getDiscount();
    });
    /*Return Specific Method*/
    self.grandNewSubTotal = ko.pureComputed(function() {
        var total = 0;
        $.each(self.lines(), function() { total += this.newSubtotal() })
        return parseFloat(total.toFixed(2));
    });

    self.grandNewTaxAddition = ko.pureComputed(function() {
        var total = 0;
        $.each(self.lines(), function() { total += this.newTaxAddition() })
        return parseFloat(total.toFixed(2));
    });

    self.grandNewTotal = ko.pureComputed(function() {
        return Math.abs(self.grandNewSubTotal() + self.grandNewTaxAddition());
    });

    self.getNewDiscount = ko.pureComputed(function(){
        return self.discount == 0 ? 0 : (self.grandNewTotal() * self.discount * 0.01);
    });

    self.grandNewTotalAfterDiscount = ko.pureComputed(function(){
        return Math.abs(self.grandNewSubTotal() + self.grandNewTaxAddition()) - self.getNewDiscount();
    });
 
    // Operations
    self.setCustomer = function(customer) {
        self.customerId(customer.id);
        self.customerName(customer.name);
        self.customerAddress(customer.address);
        self.customerMobile(customer.mobile);
        self.showCustomer(true);
    };
    self.addLine = function(line) { 
        var lineExists = false;
        $.map(self.lines(), function(tmpLine) {
            if(tmpLine.product_id == line.product_id){
                lineExists = true;
                return;
            };
        });
        if(!lineExists) self.lines.push(line);
    };
    self.addReturnedItem = function(item) { 
        self.returned_items.push(item);
    };
    self.removeLine = function(line) { self.lines.remove(line) };
    self.resetCart = function() { self.lines.removeAll(); }
    self.resetReturnedItems = function() { self.returned_items.removeAll(); }
    self.removeCustomer = function(){
        self.showCustomer(false);
        self.deposit_amount(0);
    }
    self.save = function() {
    	if (!self.showCustomer()) { 
            $('#validation').modal('show');
            return;
        };
        if(self.lines().length == 0){
            $('#validation').modal('show');
            return;
        }
        var proceed = true;
        var products = $.map(self.lines(), function(line) {
            if (parseInt(line.newQuantity()) > parseInt(line.orderedQuantity) || 
                parseInt(line.newQuantity()) < 0) {
                proceed = false;
            }else if(line.newQuantity() == 0) return;
            return {
                product_id: line.product_id,
                quantity: line.newQuantity()
            };
        });
        if(!proceed) return $('#validation').modal('show');
        var request = {
            customer: { customer_id: self.customerId() },
            products: products,
            payment_type: self.payment_type(),
        };
        if(self.location_type() == "WAREHOUSE") {
            request.warehouse = { warehouse_id: self.selectedLocation() };
        }
        request.receipt_number = self.receipt_number;
        process(request);
    };
    self.tooglePaytype = function(){
        // if(self.payment_type() == 'CASH'){
        //     $('#cash').removeClass('positive');
        //     $('#debt').addClass('positive');
        //     self.payment_type('CREDIT');
        // }else if(self.payment_type() == 'CREDIT'){
        //     $('#cash').addClass('positive');
        //     $('#debt').removeClass('positive');
        //     self.payment_type('CASH');
        // }else{
        //     alert("Invalid Payment Type");
        // }
    };
    self.toogleLocation = function(){
        // if(self.location_type() == 'STOCK'){
        //     $('#stock').removeClass('positive');
        //     $('#warehouse').addClass('positive');
        //     self.location_type('WAREHOUSE');
        // }else if(self.location_type() == 'WAREHOUSE'){
        //     $('#stock').addClass('positive');
        //     $('#warehouse').removeClass('positive');
        //     self.location_type('STOCK');
        // }else{
        //     alert("Invalid Payment Type");
        // }
    }
    self.setDeposit = function(input) {
        self.deposit_amount(input);
    };
    self.setpaymentType = function(input) {
        self.payment_type(input);
    };

    self.resetUI = function(){
        self.resetCart();
        self.removeCustomer();
        self.resetReturnedItems();
    }
};

var cartModel = new CartModel();
ko.applyBindings(cartModel);

function populateModel(orderDetails){
    console.log(orderDetails);
    cartModel.resetUI();
    var cartLine;
    for (var i = 0; i < orderDetails.sales.length; i++) {
        cartLine = new CartLine();
        cartLine.product_id = orderDetails.sales[i].product_id;
        cartLine.title = orderDetails.sales[i].product.title;
        cartLine.quantity(parseInt(orderDetails.sales[i].quantity));
        cartLine.tax = (orderDetails.sales[i].tax === '0.00' ? 0 : parseFloat(orderDetails.sales[i].tax));
        cartLine.orderedQuantity = orderDetails.sales[i].quantity;
        cartLine.unit_selling_price = orderDetails.sales[i].unit_selling_price;
        cartLine.unit_cost_price = orderDetails.sales[i].unit_cost_price;
        cartModel.addLine(cartLine);
    };
    populate_customer_info(orderDetails.customer);
    if(/*orderDetails.deposit != '0.0000' && */orderDetails.payment_type != 'CASH'){
        cartModel.setDeposit(orderDetails.deposit);
    }

    if(orderDetails.sale_return_orders.length > 0){
        var totalRefund = 0;
        for (var i = 0; i < orderDetails.sale_return_orders[0].sale_returns.length; i++) {
            var retItem = new ReturnedItem();
            retItem.product_id = orderDetails.sale_return_orders[0].sale_returns[i].product_id;
            retItem.title = orderDetails.sale_return_orders[0].sale_returns[i].product.title;
            retItem.quantity = orderDetails.sale_return_orders[0].sale_returns[i].quantity;
            retItem.tax = orderDetails.sale_return_orders[0].sale_returns[i].tax;
            retItem.unit_selling_price = orderDetails.sale_return_orders[0].sale_returns[i].unit_selling_price;
            retItem.createdAt = orderDetails.sale_return_orders[0].sale_returns[i].created_at;
            cartModel.addReturnedItem(retItem);
            var subtotal = retItem.quantity * retItem.unit_selling_price;
            totalRefund += (retItem.tax / 100 * subtotal) + subtotal;
        };
        cartModel.totalRefund(totalRefund);
        $('#print').attr('href', '/dashboard/admin/return/sales/' + orderDetails.sale_return_orders[0].receipt_number +'?q=printable')
    }
    cartModel.actual_payment_type(orderDetails.payment_type);
    cartModel.receipt_number = orderDetails.receipt_number;
}

function process(request){
    console.log(request);
    request = JSON.stringify(request);
    $('#loader').dimmer('show');
    // setTimeout(function() {
    $.ajax({
        url: "/dashboard/admin/return/sales", 
        method: 'POST',
        data: request,
        dataType: 'json',
        contentType: 'application/json',
    }).done(function( data ) {
        $('#loader').dimmer('hide');
        if(data.status){
            window.location = '/dashboard/admin/return/sales/' + data.receipt_number
        }else{
            alert('An error occured');
        }
    });
  // }, 0);
} 

function populate_customer_info(data){
    var customer = { id: data.id, name: data.title, address: data.address, 
        mobile: data.mobile_number };
    cartModel.setCustomer(customer);
}

});