$( document ).ready(function() {

initSupplierSearch();

$('.select-payment').on('click',selectPaymentType);

$('#product_search').search({
    apiSettings: {
      url: '/product/search/?type=withCategory&q={query}'
    },
    type: 'category',
    searchFullText: false,
    searchFields: [
	  'title',
	  'description'
	],
	onSelect: function(product, response){
		getStockDetails(product.id, product);
	},
});

function getStockDetails(product_id, product){
	$('#loader').dimmer('show');
	setTimeout(function() {
	$.ajax({url: "/stocks/search/?q=" + product_id})
  	.done(function( data ) {
  		$('#loader').dimmer('hide');
  		$('#product_input').val('');
    	populateModel( data.results, product);
  	});
  }, 0);
} 

var CartLine = function() {
    var self = this;
    self.product_id = null
    self.title = '';
    self.unit_cost_price = 0;
    self.quantity = ko.observable(1);
    self.subtotal = ko.pureComputed(function() {
        return self.title ? parseFloat( (self.unit_cost_price * parseInt('0'+self.quantity()) ).toFixed(2)) : '0'.toFixed(2);
    });
};

var CartModel = function() {
    var self = this;
    self.showSupplier = ko.observable(false);
    self.supplierId = ko.observable('');
    self.supplierName = ko.observable('');
    self.supplierAddress = ko.observable('');
    self.supplierMobile = ko.observable('');
    self.lines = ko.observableArray();
    self.payment_type = ko.observable('CASH'); //or CREDIT
    self.location_type = ko.observable('STOCK');//or WAREHOUSE
    self.selectedLocation = ko.observable();
    self.stockQuantity = ko.observable(0);

    self.grandSubTotal = ko.pureComputed(function() {
        var total = 0;
        $.each(self.lines(), function() { total += this.subtotal() })
        return parseFloat(total.toFixed(2));
    });

    self.grandTotal = ko.pureComputed(function() {
        return self.grandSubTotal();
    });
 
    // Operations
    self.setSupplier = function(supplier) {
    	self.supplierId(supplier.id);
    	self.supplierName(supplier.name);
    	self.supplierAddress(supplier.address);
    	self.supplierMobile(supplier.mobile);
    	self.showSupplier(true);
    };
    self.addLine = function(line) { 
    	var lineExists = false;
    	$.map(self.lines(), function(tmpLine) {
    		if(tmpLine.product_id == line.product_id){
    			lineExists = true;
    			return;
    		};
    	});
    	if(!lineExists) self.lines.push(line);
    };
    self.removeLine = function(line) { self.lines.remove(line) };
    self.resetCart = function() { self.lines.removeAll(); }
    self.removeSupplier = function(){
    	self.showSupplier(false);
    	initSupplierSearch();
    }
    self.save = function() {
    	if (!self.showSupplier()) { 
    		$('#validation').modal('show');
    		return;
    	};
    	if(self.lines().length == 0){
    		$('#validation').modal('show');
    		return;
    	}
        var proceed = true;
        var products = $.map(self.lines(), function(line) {
            if (parseInt(line.quantity()) <= 0) proceed = false;
            return {
                product_id: line.product_id,
                quantity: line.quantity()
            };
        });
        if(!proceed) return $('#validation').modal('show');
        var request = {
        	supplier: {	supplier_id: self.supplierId() },
        	products: products,
            payment_type: self.payment_type(),
        };
        if(self.location_type() == "WAREHOUSE") {
            request.warehouse = { warehouse_id: self.selectedLocation() };
        }
        processOrder(request);
    };

    self.tooglePaytypeCash = function(){
        $('#cash').addClass('positive');
        $('#transfer').removeClass('positive');
        $('#debt').removeClass('positive');
        self.deposit_amount(0);
        self.payment_type('CASH');
    };
    self.tooglePaytypeTransfer = function(){
        $('#cash').removeClass('positive');
        $('#transfer').addClass('positive');
        $('#debt').removeClass('positive');
        self.payment_type('TRANSFER');
    };
    self.tooglePaytypeCredit = function(){
        $('#cash').removeClass('positive');
        $('#transfer').removeClass('positive');
        $('#debt').addClass('positive');
        self.payment_type('CREDIT');
    };

    // self.tooglePaytype = function(){
    //     if(self.payment_type() == 'CASH'){
    //         $('#cash').removeClass('positive');
    //         $('#debt').addClass('positive');
    //         self.payment_type('CREDIT');
    //     }else if(self.payment_type() == 'CREDIT'){
    //         $('#cash').addClass('positive');
    //         $('#debt').removeClass('positive');
    //         self.payment_type('CASH');
    //     }else{
    //         alert("Invalid Payment Type");
    //     }
    // };
    self.toogleLocation = function(){
        if(self.location_type() == 'STOCK'){
            $('#stock').removeClass('positive');
            $('#warehouse').addClass('positive');
            self.location_type('WAREHOUSE');
        }else if(self.location_type() == 'WAREHOUSE'){
            $('#stock').addClass('positive');
            $('#warehouse').removeClass('positive');
            self.location_type('STOCK');
        }else{
            alert("Invalid Payment Type");
        }
    }
};

var cartModel = new CartModel();
ko.applyBindings(cartModel);

function populateModel(inStock, product){
	var cartLine = new CartLine();
	cartLine.product_id = product.id;
	cartLine.title = product.title;	
	cartLine.quantity(1);
    if(inStock){
        cartLine.stockQuantity = inStock.stock_quantity;
    }else{
        cartLine.stockQuantity = 0;
    }
	cartLine.unit_cost_price = parseFloat(parseFloat(product.unit_cost_price).toFixed(2));
	cartModel.addLine(cartLine);
}

function processOrder(request){
    request = JSON.stringify(request);
    $('#loader').dimmer('show');
    // setTimeout(function() {
    $.ajax({
        url: "/dashboard/admin/receivings", 
        method: 'POST',
        data: request,
        dataType: 'json',
        contentType: 'application/json',
    }).done(function( data ) {
        $('#loader').dimmer('hide');
        if(data.status){
            // cartModel.resetCart();
            // cartModel.removeSupplier();
            window.location = '/dashboard/purchase/' + data.receipt_number
        }else{
            alert('An error occured');
        }
    });
  // }, 0);
} 

function populate_supplier_info(data){
	var supplier = { id: data.id, name: data.title, address: data.address, 
		mobile: data.mobile_number };
	cartModel.setSupplier(supplier);
}

function initSupplierSearch(){
	$('#supplier_search').search({
	    apiSettings: {
	      url: '/supplier/search/?q={query}'
	    },
	    searchFullText: false,
	    searchFields: [
		  'title',
		  'description'
		],
		onSelect: function(result, response){
			populate_supplier_info(result);
		},
	});
}

function selectPaymentType(e){
    e.preventDefault();
    $.post('http://demo.phppointofsale.com/index.php/sales/set_selected_payment', {payment: $(this).data('payment')});
    $('#payment_types').val($(this).data('payment'));
    $('.select-payment').removeClass('active');
    $(this).addClass('active');
    // $("#amount_tendered").focus();
    // $("#amount_tendered").select();
    // $("#amount_tendered").attr('placeholder','');
    // checkPaymentTypeGiftcard();
    // checkPaymentTypePoints();
}

});