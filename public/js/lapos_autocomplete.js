$( document ).ready(function() {

// $('.ui.search').search({
//     apiSettings: {
//       url: '/product/search/{query}'
//     },
//     type: 'category'
// });

var domTitle = $('#title');
var domProductId = $('#product_id');
var domCategory = $('#category');
var domDescription = $('#description');
var domUnitCostPrice = $('#unit_cost_price');

var domSupplierTitle = $('#supplier_title');
var domSupplierId = $('#supplier_id');

function populate_product_info(product){
	domTitle.val(product['title']);
	domProductId.val(product['id']);
	domCategory.val(product['category']);
	domDescription.val(product['description']);
	domUnitCostPrice.val(product['unit_cost_price']);
}

function populate_supplier_info(supplier){
	domSupplierTitle.val(supplier['title']);
	domSupplierId.val(supplier['id']);
}

$('#product_search').search({
    apiSettings: {
      url: '/product/search/?q={query}&type=withCategory'
    },
    type: 'category',
    searchFullText: false,
    searchFields: [
	  'title',
	  'description'
	],
	onSelect: function(result, response){
		/*
		Callback on element selection by user. 
		The first parameter includes the filtered response results 
		for that element. The function should return false to 
		prevent default action (closing search results and selecting value).
		*/
		populate_product_info(result);
	},
	onResultsAdd: function(html){
		/*
		Callback after processing element template to add HTML to results. 
		Function should return false to prevent default actions.
		*/
	},
	onSearchQuery: function(query){
		// Callback on search query
		// console.log(query);
	},
	onResults: function(response){
		// Callback on server response
	},
	onResultsOpen: function(){
		// Callback when results are opened
	},
	onResultsClose: function(){
		// Callback when results are closed
	}
});


$('#supplier_search').search({
    apiSettings: {
      url: '/supplier/search/?q={query}'
    },
    searchFullText: false,
    searchFields: [
	  'title',
	  'description'
	],
	onSelect: function(result, response){
		populate_supplier_info(result);
	},
});
		  
});