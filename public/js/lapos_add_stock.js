$( document ).ready(function() {


var domTitle = $('#title');
var domProductId = $('#product_id');
var domCategory = $('#category');
var domDescription = $('#description');
var domUnitCostPrice = $('#unit_cost_price');

var domSupplierTitle = $('#supplier_title');
var domSupplierId = $('#supplier_id');
var togglerFlag = false;

$('#directly').checkbox({
	onChecked: function(){
		$('#warehouse_container').css('display', 'none');
		togglerFlag = true;
	},
	onUnchecked: function(){
		togglerFlag = false;
	}
});

function populate_product_info(product){
	domTitle.val(product['title']);
	domProductId.val(product['id']);
	domCategory.val(product['category']);
	domDescription.val(product['description']);
	domUnitCostPrice.val(product['unit_cost_price']);
	getProductDetails(product['id']);
}

$('#product_search').search({
    apiSettings: {
      url: '/product/search/?type=withCategory&q={query}'
    },
    type: 'category',
    searchFullText: false,
    searchFields: [
	  'title',
	  'description'
	],
	onSelect: function(result, response){
		/*
		Callback on element selection by user. 
		The first parameter includes the filtered response results 
		for that element. The function should return false to 
		prevent default action (closing search results and selecting value).
		*/
		populate_product_info(result);
	},
	onResultsAdd: function(html){
		/*
		Callback after processing element template to add HTML to results. 
		Function should return false to prevent default actions.
		*/
	},
	onSearchQuery: function(query){
		// Callback on search query
		// console.log(query);
	},
	onResults: function(response){
		// Callback on server response
	},
	onResultsOpen: function(){
		// Callback when results are opened
	},
	onResultsClose: function(){
		// Callback when results are closed
	}
});

var tpl = '<div class="statistic"> \
	    <div class="value">QUANTITY_LEFT</div> \
	    <div class="label"> \
	      <div class="ui sampling checkbox"> \
	        <input class="hidden" value="WAREHOUSEID" name="warehouse_id" type="radio"/> \
	        <label for="">TITLE</label> \
	      </div> \
	    </div> \
  	</div>';

function populateUI(data){
	if (data['warehouse_support'] && data['warehouses'].constructor === Array && 
		data['warehouse_items'].constructor === Array) {
		$('.ui.statistics').html('');
		for (var i = 0; i < data['warehouses'].length; i++) {
			var flag = false;
			for (var j = 0; j < data['warehouse_items'].length; j++) {
				if(data['warehouses'][i].id != data['warehouse_items'][j].warehouse_id) continue;
				var statistic = tpl.replace('QUANTITY_LEFT',
					data['warehouse_items'][j].Qty).replace('TITLE', 
					data['warehouse_items'][j].warehouse.title).replace('WAREHOUSEID', 
					data['warehouse_items'][j].warehouse_id);
				$('.ui.statistics').append(statistic);
				flag = true;
			}
			if(!flag){
				var statistic = tpl.replace('QUANTITY_LEFT', 0).replace('TITLE', 
					data['warehouses'][i].title).replace('WAREHOUSEID', 
					data['warehouses'][i].id);
				$('.ui.statistics').append(statistic);
			}
		}
		$('.ui.sampling.checkbox').checkbox();
		if (!togglerFlag) $('#warehouse_container').css('display', 'inline');
	}
}

function getProductDetails(product_id){
	$('#loader').dimmer('show');
	setTimeout(function() {
	$.ajax({
  		url: "/warehouse/product?product_id=" + product_id,
  		beforeSend: function( xhr ) {
    		// xhr.overrideMimeType( "text/plain; charset=x-user-defined" );
  		}
	})
  	.done(function( data ) {
  		$('#loader').dimmer('hide');
    	populateUI(data);
  	});
  }, 1000);
}

		  
});