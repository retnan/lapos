$( document ).ready(function() {

// $('.ui.search').search({
//     apiSettings: {
//       url: '/product/search/{query}'
//     },
//     type: 'category'
// });

$('#all_search').search({
    apiSettings: {
      url: '/search?q={query}&type=withCategory'
    },
    type: 'category',
    searchFullText: false,
    searchFields: [
	  'title',
	  'description'
	],
	onSelect: function(result, response){
		/*
		Callback on element selection by user. 
		The first parameter includes the filtered response results 
		for that element. The function should return false to 
		prevent default action (closing search results and selecting value).
		*/
		//Enter Key Not Working....
		window.location = result.url;
		return false;// therefore
	},
});
		  
});