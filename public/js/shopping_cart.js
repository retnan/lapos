$( document ).ready(function() {

initCustomerSearch();

$('#product_search').search({
    apiSettings: {
      url: '/product/search/?type=withCategory&q={query}'
    },
    type: 'category',
    searchFullText: false,
    cache: false,
    searchFields: [
	  'title',
	  'description'
	],
	onSelect: function(result, response){
		getStockDetails(result.id);
	},
});

function getStockDetails(product_id){
	$('#loader').dimmer('show');
	setTimeout(function() {
	$.ajax({url: "/stocks/search/?q=" + product_id})
  	.done(function( data ) {
  		$('#loader').dimmer('hide');
  		$('#product_input').val('');
    	populateModel( data.results);
  	});
  }, 0);
} 

var CartLine = function() {
    var self = this;
    self.product_id = null
    self.maxQtyAllowed = null;
    self.tax = 100;
    self.discount = ko.observable(0);
    self.tax_type = 'percentage';
    self.title = '';
    self.unit_selling_price = 0;
    self.quantity = ko.observable(1);
    self.subtotal = ko.pureComputed(function() {
        var subtotal = self.unit_selling_price * parseInt('0'+self.quantity()),
        subTotalWithDiscount = subtotal - (self.discount() * subtotal);
        console.log({subtotal, subTotalWithDiscount});
        return self.title ? parseFloat( (subTotalWithDiscount).toFixed(2)) : '0'.toFixed(2);
    });
    self.taxAddition = ko.pureComputed(function() {
        return self.tax == 0 ? 0 : (self.subtotal() * self.tax * 0.01);
    });
};

var CartModel = function() {
    var self = this;
    self.showCustomer = ko.observable(false);
    self.customerId = ko.observable('');
    self.customerName = ko.observable('');
    self.customerAddress = ko.observable('');
    self.customerMobile = ko.observable('');
    self.payment_type = ko.observable('CASH'); //or CREDIT
    self.lines = ko.observableArray();
    self.discount = ko.observable(0);
    self.discount_code = null;
    self.deposit_amount = ko.observable(0);
    self.grandSubTotal = ko.pureComputed(function() {
        var total = 0;
        $.each(self.lines(), function() { total += this.subtotal() })
        return parseFloat(total.toFixed(2));
    });
    self.grandTaxAddition = ko.pureComputed(function() {
        var total = 0;
        $.each(self.lines(), function() { total += this.taxAddition() })
        return parseFloat(total.toFixed(2));
    });
    self.grandTotal = ko.pureComputed(function() {
        return Math.abs(self.grandSubTotal() + self.grandTaxAddition());
    });

    self.getDiscount = ko.pureComputed(function(){
    	return self.discount() == 0 ? 0 : self.discount();
    });

    self.grandTotalAfterDiscount = ko.pureComputed(function() {
        return Math.abs(self.grandSubTotal() + self.grandTaxAddition()) - self.getDiscount();
    });
 
    // Operations
    self.setCustomer = function(customer) {
    	self.customerId(customer.id);
    	self.customerName(customer.name);
    	self.customerAddress(customer.address);
    	self.customerMobile(customer.mobile);
    	self.showCustomer(true);
    };
    self.unsetDiscounts = function() {
        self.discount(0);
        for(var i = 0; i < self.lines().length; i++){
            self.lines()[i].discount(0);
        }
        self.discount_code = null;
    }
    self.setDiscountCode = function(discount_code){
        self.discount_code = discount_code;
    }
    self.setDiscounts = function(data) {
        if(data.length === 1 && data[0].product_id === null && data[0].amount > 0){ 
            //only absolute is applicable for total
            if(data[0].frequency === null || data[0].usage_counter < data[0].frequency){
                self.discount(data[0].amount);
                console.log("absolute discount",self.discount());
            }
            return;
        }
        for(var i = 0; i < self.lines().length; i++){
            for(var j = 0;  j < data.length; j++){
                if(data[j].product_id === null || self.lines()[i].product_id === data[j].product_id){
                    if(data[j].frequency === null || data[j].usage_counter < data[j].frequency){
                        if(data[j].amount < 1){ //only percentage is applicable per
                            self.lines()[i].discount(data[j].amount);
                            console.log("percentage discount per #item", self.lines()[i].discount());
                        }
                        break;
                    }
                }
            }
        }
    }
    self.addLine = function(line) { 
    	var lineExists = false;
    	$.map(self.lines(), function(tmpLine) {
    		if(tmpLine.product_id == line.product_id){
    			lineExists = true;
    			return;
    		};
    	});
    	if(!lineExists) self.lines.push(line);
    };
    self.removeLine = function(line) { self.lines.remove(line) };
    self.resetCart = function() { self.lines.removeAll(); }
    self.removeCustomer = function(){
    	self.showCustomer(false);
    	initCustomerSearch();
        self.deposit_amount(0);
    }
    self.save = function() {
    	if (!self.showCustomer()) { 
    		$('#validation').modal('show');
    		return;
    	};
    	if(self.lines().length == 0){
    		$('#validation').modal('show');
    		return;
    	}
        var proceed = true;
        var products = $.map(self.lines(), function(line) {
            if (parseInt(line.quantity()) > parseInt(line.maxQtyAllowed) || 
                parseInt(line.quantity()) <= 0) {
                proceed = false;
            }
            return {
                product_id: line.product_id,
                quantity: line.quantity()
            };
        });
        if(!proceed) return $('#validation').modal('show');
        var request = {
        	customer: {	customer_id: self.customerId() },
        	products: products,
            payment_type: self.payment_type(),
            discount_code: self.discount_code,
        };
        if(self.payment_type() == "CREDIT") {
            request.deposit_amount = self.deposit_amount();
        }
        processOrder(request);
    };
    self.tooglePaytypeCash = function(){
        $('#cash').addClass('positive');
        $('#transfer').removeClass('positive');
        $('#debt').removeClass('positive');
        self.deposit_amount(0);
        self.payment_type('CASH');
    };
    self.tooglePaytypeTransfer = function(){
        $('#cash').removeClass('positive');
        $('#transfer').addClass('positive');
        $('#debt').removeClass('positive');
        self.payment_type('TRANSFER');
    };
    self.tooglePaytypeCredit = function(){
        $('#cash').removeClass('positive');
        $('#transfer').removeClass('positive');
        $('#debt').addClass('positive');
        self.payment_type('CREDIT');
    };
    self.applyDiscountCode = function(){
        $('#loader').dimmer('show');
        $.ajax({
            url: "/dashboard/discount?code="+$('#coupon').val(), 
            method: 'GET',
            dataType: 'json',
            contentType: 'application/json',
        }).done(function( data ) {
            $('#loader').dimmer('hide');
            if(data.status){
                applyDiscounts(data.data, $('#coupon').val().trim());
            }else{
                removeDiscounts();
            }
        });
    };
    self.setDeposit = function() {        
        var input = Math.abs(parseFloat('0'+$('#cash_at_hand').val()));
        if(input >= self.grandTotal()){
            return self.tooglePaytype(); //You don't know what you are doing.
        }
        self.deposit_amount(input);
    };
};

var cartModel = new CartModel();
ko.applyBindings(cartModel);

function populateModel(data){
	if (!data) return $('#notInStock').modal('show');
	var cartLine = new CartLine();
	cartLine.product_id = data.product_id;
	cartLine.title = data.product.title;	
	cartLine.tax = (data.product.tax === '0.00' ? 0 : parseFloat(data.product.tax));
	cartLine.quantity(1);
	cartLine.tax_type = data.product.tax_type;
	cartLine.unit_selling_price = parseFloat(parseFloat(data.product.unit_selling_price).toFixed(2));
	cartLine.maxQtyAllowed = data.stock_quantity;
	cartModel.addLine(cartLine);
}

function processOrder(request){
    request = JSON.stringify(request);
    $('#loader').dimmer('show');
    // setTimeout(function() {
    $.ajax({
        url: "/dashboard/pos", 
        method: 'POST',
        data: request,
        dataType: 'json',
        contentType: 'application/json',
    }).done(function( data ) {
        $('#loader').dimmer('hide');
        if(data.status){
            // cartModel.resetCart();
            // cartModel.removeCustomer();
            window.location = '/dashboard/pos/' + data.receipt_number
        }else{
            alert('An error occured');
        }
    });
  // }, 0);
} 

function populate_customer_info(data){
	var customer = { id: data.id, name: data.title, address: data.address, 
		mobile: data.mobile_number };
	cartModel.setCustomer(customer);
}

function applyDiscounts(data, discount_code){
    cartModel.setDiscounts(data);
    cartModel.setDiscountCode(discount_code);
}

function removeDiscounts(){
	cartModel.unsetDiscounts();
}

function initCustomerSearch(){
	$('#customer_search').search({
	    apiSettings: {
	      url: '/customer/search/?q={query}'
	    },
	    searchFullText: false,
	    searchFields: [
		  'title',
		  'description'
		],
		onSelect: function(result, response){
			populate_customer_info(result);
		},
	});
}

});