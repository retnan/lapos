$( document ).ready(function() {
    $('#subscribe_email').on('click', function(){
       $('#subscribe_email').addClass('disabled');
       $.ajax({
            url: "/subscribe", 
            method: 'POST',
            data: JSON.stringify({	email:  $('#email_to_subscribe').val(), }),
            dataType: 'json',
            contentType: 'application/json',
        }).done(function( data ) {
            if(data.status){
                alert('Thank you for subscribing');
            }else{
                $('#email_to_subscribe').val('');
                alert('Oopse! An error occured, please try again.');
            }
        }); 
    });
});