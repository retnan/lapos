$( document ).ready(function() {

// $('.ui.search').search({
//     apiSettings: {
//       url: '/product/search/{query}'
//     },
//     type: 'category'
// });

$('#product_search').search({
    apiSettings: {
      url: '/product/search/{query}'
    },
    type: 'category',
    searchFullText: false,
    searchFields: [
	  'title',
	  'description'
	],
	onSelect: function(result, response){
		/*
		Callback on element selection by user. 
		The first parameter includes the filtered response results 
		for that element. The function should return false to 
		prevent default action (closing search results and selecting value).
		*/
		console.log(result);
		console.log(response);
	},
	onResultsAdd: function(html){
		/*
		Callback after processing element template to add HTML to results. 
		Function should return false to prevent default actions.
		*/
	},
	onSearchQuery: function(query){
		// Callback on search query
		console.log(query);
	},
	onResults: function(response){
		// Callback on server response
	},
	onResultsOpen: function(){
		// Callback when results are opened
	},
	onResultsClose: function(){
		// Callback when results are closed
	}
});
		  
});