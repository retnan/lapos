<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SaleReturn extends Model
{
	public function sale_return_order()
    {
        return $this->belongsTo('App\SaleReturnOrder');
    }

    public function product()
    {
    	return $this->belongsTo('App\Product');
    }
    
}
