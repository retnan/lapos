<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    public function user()
    {
        return $this->belongsTo('App\User');
    }

    public function category()
    {
        return $this->belongsTo('App\Category');
    }

    public function sales()
    {
        return $this->hasMany('App\Sale');
    }

    public function barcodes()
    {
        return $this->hasMany('App\Barcode');
    }
}
