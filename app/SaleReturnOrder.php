<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SaleReturnOrder extends Model
{
    public function sale_order()
    {
        return $this->belongsTo('App\SaleOrder');
    }

    public function sale_returns()
    {
        return $this->hasMany('App\SaleReturn');
    }

    public function customer()
    {
        return $this->belongsTo('App\Customer');
    }

    public function user()
    {
        return $this->belongsTo('App\User');
    }
}
