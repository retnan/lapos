<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

use App\Config;
use Cache;
use Schema;

class AppConfigServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        if (Cache::has('AppConfig')){
            $config = Cache::get('AppConfig'); 
        }else{
            if (!Schema::hasTable('configs')) return;
            $config = Config::find(1);
            Cache::forever('AppConfig', $config);
        }

        view()->composer('*', function($view) use ($config){
            $view->with('AppConfig', $config);
        });

        // view()->composer('layouts.admin_dashboard', function($view) use ($config){
        //     $view->with('AppConfig', $config);
        // });
        
        // view()->composer('layouts.cashier_dashboard', function($view) use ($config){
        //     $view->with('AppConfig', $config);
        // });

        // view()->composer(['product.index', 'product.create'], function($view) use ($config){
        //     $view->with('AppConfig', $config);
        // });

        // view()->composer('warehouse.items', function($view) use ($config){
        //     $view->with('AppConfig', $config);
        // });
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
