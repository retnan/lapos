<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class WarehouseMovedItem extends Model
{
    public function user()
    {
        return $this->belongsTo('App\User');
    }

    public function product()
    {
        return $this->belongsTo('App\Product');
    }

    public function warehouse()
    {
        return $this->belongsTo('App\Warehouse');
    }
}
