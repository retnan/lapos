<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PurchaseOrder extends Model
{
    public function purchases()
    {
        return $this->hasMany('App\Purchase');
    }

    public function supplier()
    {
        return $this->belongsTo('App\Supplier');
    }

    public function user()
    {
        return $this->belongsTo('App\User');
    }

    public function purchase_return_orders(){
        return $this->hasMany('App\PurchaseReturnOrder');
    }
}
