<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use Auth;
use Redirect;
use App\Customer;
use App\User;
use App\Account;
use Session;
use DB;

class CustomerController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function show($id){
        $customer = Customer::with(['sale_orders'])->find($id);
        if(!$customer){
            Session::flash('flash_error', 'Record does not exists');
            return back();
        }
        return view('customer.show', ['data' => $customer]);
    }
    
    public function getCustomers(){
    	$user = Auth::user();
        if($user->access_role != "admin" && 
            Auth::user()->access_role != "cashier"){
            Session::flash('flash_error', 'You do not have the permission to access the page.');
            return back();
        }
        return view('customer.index', ['data' => Customer::with(['user','author'])->get()]);
    }

    public function getAddCustomer(){
    	$user = Auth::user();
        if($user->access_role != "admin" &&
            Auth::user()->access_role != "cashier"){
            Session::flash('flash_error', 'You do not have the permission to access the page.');
            return back();
        }
        return view('customer.create');
    }

    public function getEditCustomer($id){
        $user = Auth::user();
        if($user->access_role != "admin" && 
            Auth::user()->access_role != "cashier"){
            Session::flash('flash_error', 'You do not have the permission to access the page.');
            return back();
        }
        return view('customer.edit', ['data' => Customer::find($id)]);
    }

    public function updateCustomer(Request $request, $id){
        if(Auth::user()->access_role != "admin" && 
            Auth::user()->access_role != "cashier"){
            Session::flash('flash_error', 'You do not have the permission to access the page.');
            return back();
        }
        
        $this->validate($request, [
            'title' => 'required',
            'mobile_number' => 'required',
            'email' =>'sometimes|email'
        ]);

        try{
            
            DB::transaction(function() use($request, $id)
            {

                $nCustomer = Customer::with(['user'])->find($id);
                $nCustomer->title = $request->input('title');
                $nCustomer->address = $request->input('address');
                $nCustomer->email = $request->input('email');
                $nCustomer->mobile_number = $request->input('mobile_number');
                $nCustomer->user->name = $request->input('title');
                $nCustomer->user->save();
                // $nCustomer->account->title = $request->input('title');
                // $nCustomer->account->save();
                $nCustomer->save();
            });

        } catch(\Exception $e){
            die($e->getMessage());
                
            Session::flash('flash_message', 'Sorry, an Error Occured');
            return back();
            
        }

        Session::flash('flash_message', 'Successfuly Updated');
        return back();
    }

    public function addCustomer(Request $request){
        if(Auth::user()->access_role != "admin" && 
            Auth::user()->access_role != "cashier"){
            Session::flash('flash_error', 'You do not have the permission to access the page.');
            return back();
        }
        
        $this->validate($request, [
        	'title' => 'required',
            'mobile_number' => 'required',
            'email' => 'sometimes|email',
        ]);
        
        //the validate method ensures we don't get here on failure

        /** Generator TODO: Fire an Event Instead and do it from single source */

        try {
            DB::transaction(function() use($request)
            {


                $faker = \Faker\Factory::create();

                $mUser = new User;
                $mUser->name = $request->input('title');
                $mUser->email = $faker->email;
                $mUser->password = bcrypt($faker->password);
                $mUser->access_role = 'virtual';
                $mUser->save();

                $nCustomer = new Customer;
                $nCustomer->title = $request->input('title');
                $nCustomer->address = $request->input('address');
                $nCustomer->email = $request->input('email');
                $nCustomer->mobile_number = $request->input('mobile_number');
                $nCustomer->user()->associate(User::find($mUser->id));
                $nCustomer->author_id = Auth::user()->id;

                $nCustomer->save();
            });
        }catch(\Exception $e) {
            Session::flash('flash_message', 'Sorry, an Error Occurred');
            return back();
        }
        
        Session::flash('flash_message', 'Successfuly Saved');
        return back();
    }
}
