<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use Auth;
use Redirect;
use App\Bank;
use App\User;
use App\Account;
use Session;
use DB;
use LaPOS\AccountCode;

class BankController extends Controller
{   
    public function getBanks(){
    	$user = Auth::user();
        if($user->access_role != "admin"){
            Session::flash('flash_error', 'You do not have the permission to access the page.');
            return back();
        }
        return view('bank.index', ['data' => Bank::with(['user'])->get()]);
    }

    public function getAddBank(){
    	$user = Auth::user();
        if($user->access_role != "admin"){
            Session::flash('flash_error', 'You do not have the permission to access the page.');
            return back();
        }
        return view('bank.create');
    }

    public function getEditBank($id){
        $user = Auth::user();
        if($user->access_role != "admin"){
            Session::flash('flash_error', 'You do not have the permission to access the page.');
            return back();
        }
        return view('bank.edit', ['data' => Bank::find($id)]);
    }

    public function updateBank(Request $request, $id){

        if(Auth::user()->access_role != "admin"){
            Session::flash('flash_error', 'You do not have the permission to access the page.');
            return back();
        }
        
        $this->validate($request, [
            'title' => 'required',
            // 'sort_code' => 'required',
            'address' => 'required',
        ]);

        try{
            
            DB::transaction(function() use($request, $id)
            {

                $nBank = Bank::with(['user','account'])->find($id);
                $nBank->title = $request->input('title');
                $nBank->address = $request->input('address');
                $nBank->sort_code = $request->input('sort_code');
                $nBank->user->name = $request->input('title');
                $nBank->user->save();
                $nBank->account->title = $request->input('title');
                $nBank->account->save();
                $nBank->save();

            });

        } catch(\Exception $e){
            // die(var_dump($e));
                
            Session::flash('flash_message', 'Sorry, an Error Occured');
            return back();
            
        }

        Session::flash('flash_message', 'Successfuly Updated');
        return back();
    }

    public function addBank(Request $request){
        if(Auth::user()->access_role != "admin"){
            Session::flash('flash_error', 'You do not have the permission to access the page.');
            return back();
        }
        
        $this->validate($request, [
        	'title' => 'required|unique:banks',
            // 'sort_code' => 'required',
            'address' => 'required',
        ]);
        
        //the validate method ensures we don't get here on failure

        /** Generator TODO: Fire an Event Instead and do it from single source */

        try{
            DB::transaction(function() use($request)
            {
                $lastBank = Account::select('account_no')->where('account_no', 'like', 
                    AccountCode::$ASSETS_BANK . '%')->orderBy('account_no')->get()->last();
                $nextNewBank = (int) $lastBank->account_no + 1;
                $mAccount = new Account;
                $mAccount->title = strtoUpper($request->input('title'));
                $mAccount->account_no = $nextNewBank;
                $mAccount->user()->associate(User::find(Auth::user()->id));
                $mAccount->save();

                $nBank = new Bank;
                $nBank->title = $request->input('title');
                $nBank->address = $request->input('address');
                $nBank->sort_code = $request->input('sort_code');
                $nBank->account()->associate(Account::find($mAccount->id));
                $nBank->user_id = Auth::user()->id;

                $nBank->save();
            });
        } catch(\Exception $e) {
            die($e);
            Session::flash('Flash_message', 'Sorry, an Error Occured');
            return back();
        }

        Session::flash('flash_message', 'Successfuly Created');
        return back();
    }
}