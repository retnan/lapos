<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use Newsletter;
use Response;

class NewsLetterController extends Controller
{
    public function subscribe(Request $request){
        $request = $request->all();
        $email = $request['email'];
        if(!$this->isEmail($email)) return Response::json(['status' => false, 'data' => $email]);
        if(!Newsletter::hasMember($email)){
            Newsletter::subscribe($email);
        }
        return Response::json(['status' => true]);
    }
    
    private function isEmail($email){
        if(strlen($email) == 0){
            return false;
        }
        return true;
    }
}
