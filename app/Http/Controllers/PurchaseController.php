<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use Response;
use Auth;
use App\Product;
use App\Category;
use Redirect;
use Session;
use DB;
use App\PurchaseOrder;
use App\Purchase;
use App\Supplier;
use App\Stock;
use App\WarehouseItem;
use Cache;
use Carbon\Carbon;
use App\Warehouse;
use App\Account;
use LaPOS\AccountCode;
use App\Transaction;

class PurchaseController extends Controller
{

    private $AppConfig;

    public function __construct()
    {
        $this->middleware('auth');
        $this->AppConfig = Cache::get('AppConfig');
    }

    public function getReceivingReceipt(Request $request, $receiptId){
        $printable = false;
        if($request->has('q')){
            $printable = true;
        }
        $data = PurchaseOrder::with(['purchases', 'supplier', 'purchases.product'])
        ->where('receipt_number', $receiptId)->get()->first();
        return view('product.receipt', ['data' => $data, 'isPrintable' => $printable]);
    }

    public function getReceivings(Request $request){
        // if(Auth::user()->access_role != "admin"){
        if(Auth::user()->access_role != "admin" && 
            Auth::user()->access_role != "cashier"){
            Session::flash('flash_error', 'You do not have the permission to access the page.');
            return back();
        }
        return view('product.receivings', ['warehouses' => Warehouse::get()->pluck('title','id')]);
    }

    private function genReceiptNumber(){
        $result = 'SRPT';
        for ($i=0; $i < 10; $i++) $result .= mt_rand(0, 9);
        return trim($result);
    }

    private function genInvoiceNumber(){
        $result = 'SINV';
        for ($i=0; $i < 10; $i++) $result .= mt_rand(0, 9);
        return trim($result);
    }

    private function validate_cart($request){
        if ($request->has('supplier') && $request->has('products') &&
            count($request->input('products')) > 0) return true;
        return false;
    }

    public function markReceivingsForPayment(Request $request){
        $user = Auth::user();
        if(Auth::user()->access_role != "admin" && 
            Auth::user()->access_role != "cashier"){
            Session::flash('flash_error', 'You do not have the permission to access the page.');
            return back();
        }

        $order = PurchaseOrder::where('receipt_number', $request->input('invoice_number'))
        ->where('payment_type', 'CREDIT')->get()->first();
        if(!$order){
            Session::flash('flash_error', "This invoice does not exist.");
            return back();
        }

        try {
            DB::transaction(function() use($order) {

                $order->previous_invoice_number = $order->receipt_number;
                $order->receipt_number = $this->genReceiptNumber();
                $order->payment_type = 'CASH';
                $order->user()->associate(Auth::user());
                $order->save();
                /* Update Accounting Information */

                $supplier = Supplier::find($order->supplier_id);
                $supplier->prev_balance = $supplier->balance;
                $supplier->balance -= abs($order->total_amount/* - $order->deposit*/);
                $supplier->save();


                $payable = Account::where('account_no', AccountCode::$LIABILITY_PAYABLE)->get()->first();
                $payable->prev_balance = $payable->balance;
                $payable->balance += abs($order->total_amount/* - $order->deposit*/);
                $payable->save();

                $transaction = new Transaction;
                $transaction->amount = abs($order->total_amount/* - $order->deposit*/);
                $transaction->account()->associate($payable);
                $transaction->naration = 'Paid purchase owed debt obligation #' . $order->receipt_number;
                $transaction->transaction_reference = $order->receipt_number;
                $transaction->transaction_type = 'dr';
                $transaction->user()->associate(Auth::user());
                $transaction->save();

                $cash = Account::where('account_no', AccountCode::$ASSETS_CASH)->get()->first();
                $cash->prev_balance = $cash->balance;
                $cash->balance += abs($order->total_amount/* - $order->deposit*/);
                $cash->save();

                $transaction = new Transaction;
                $transaction->amount = abs($order->total_amount - $order->deposit);
                $transaction->account()->associate($cash);
                $transaction->naration = 'Paid out purchase debt owed #' . $order->receipt_number;
                $transaction->transaction_reference = $order->receipt_number;
                $transaction->transaction_type = 'cr';
                $transaction->user()->associate(Auth::user());
                $transaction->save();
            });
        } catch(\Exception $e) {
            die($e);
            Session::flash('flash_error', 'An error occured');
            return back();
        }

        Session::flash('flash_message', 'Sucessfully Updated');
        return redirect()->route('receiving_receipt', $order->receipt_number);
    }

    public function processReceivings(Request $request){
        $user = Auth::user();
        if(Auth::user()->access_role != "admin" && 
            Auth::user()->access_role != "cashier"){
            Session::flash('flash_error', 'You do not have the permission to access the page.');
            return back();
        }
        if(!$this->validate_cart($request)){
            return Response::json(['status' => false, 'error_code' => 1001]);
        }

        $order = new PurchaseOrder;
        try {
            DB::transaction(function() use($request, $user, $order) {

                $request = $request->all();
                $supplier = $request['supplier'];
                $products = $request['products'];
                $warehouse = null;
                if($this->AppConfig->supports_warehouse && array_key_exists('warehouse', $request)){
                    $warehouse = $request['warehouse'];
                }
                if($request['payment_type'] == 'CASH'){
                    $order->receipt_number = $this->genReceiptNumber();
                    $order->payment_type = 'CASH';
                }else if($request['payment_type'] == 'TRANSFER'){
                    $order->receipt_number = $this->genReceiptNumber();
                    $order->payment_type = 'TRANSFER';
                }else{
                    $order->receipt_number = $this->genInvoiceNumber();
                    $order->payment_type = 'CREDIT';
                }
                $order->supplier()->associate(Supplier::find($supplier['supplier_id']));
                $order->user()->associate(Auth::user());
                $order->save();

                $cart = array();
                $stocks = array();
                $warehouseItems = array();
                $totalCost = 0;
                foreach ($products as $product) {
                    $response = Product::find($product['product_id']);
                    $cartIn = array('quantity' => $product['quantity'], 'unit_cost_price' => $response->unit_cost_price, 
                        'unit_selling_price' => $response->unit_selling_price, 'product_id' => $product['product_id'], 'purchase_order_id' => $order->id,
                        'user_id' => $user->id, 'tax' => $response->tax, 'created_at' => Carbon::now(), 'updated_at' => Carbon::now());
                    $stockIn = array('quantity' => $product['quantity'], 'unit_cost_price' => $response->unit_cost_price, 
                        'unit_selling_price' => $response->unit_selling_price, 'product_id' => $product['product_id'], 'user_id' => $user->id, 
                        'tax' => $response->tax, 'created_at' => Carbon::now(), 'updated_at' => Carbon::now());
                    if($warehouse != null && array_key_exists('warehouse_id', $warehouse)){
                        $wItemIn = array('quantity' => $product['quantity'], 'unit_cost_price' => $response->unit_cost_price, 
                            'unit_selling_price' => $response->unit_selling_price, 'product_id' => $product['product_id'], 
                            'user_id' => $user->id, 'tax' => $response->tax, 'supplier_id' => $supplier['supplier_id'], 
                            'warehouse_id' => $warehouse['warehouse_id'], 'created_at' => Carbon::now(), 'updated_at' => Carbon::now());
                        array_push($warehouseItems, $wItemIn);
                    }
                    array_push($cart, $cartIn);
                    array_push($stocks, $stockIn);
                    $subTotal = $product['quantity'] * $response->unit_cost_price;
                    //TODO: TAX
                    $totalCost += /*(($response->tax / 100)  * $subTotal) + */ $subTotal;
                }
                //finally cache the final order total
                $order->total_amount = $totalCost;
                $order->save();

                Purchase::insert($cart);
                if(!$this->AppConfig->supports_warehouse){
                    Stock::insert($stocks);
                }else{
                    if($warehouse != null && array_key_exists('warehouse_id', $warehouse)){
                        WarehouseItem::insert($warehouseItems);
                    }else{
                        Stock::insert($stocks);
                    }
                }

                /* TODO: Update Accounting Information */
                if($request['payment_type'] == 'CREDIT'){
                    if(true/*abs($order->deposit - $totalCost) == $totalCost*/){
                        // No Fund deposited
                        $purchase = Account::where('account_no', AccountCode::$ASSETS_PURCHASE)->get()->first();
                        $purchase->prev_balance = $purchase->balance;
                        $purchase->balance += $totalCost;
                        $purchase->save();

                        $transaction = new Transaction;
                        $transaction->amount = $totalCost;
                        $transaction->account()->associate($purchase);
                        $transaction->naration = '!Credit Purchase #' . $order->receipt_number;
                        $transaction->transaction_reference = $order->receipt_number;
                        $transaction->transaction_type = 'dr';
                        $transaction->user()->associate(Auth::user());
                        $transaction->save();

                        $payable = Account::where('account_no', AccountCode::$LIABILITY_PAYABLE)->get()->first();
                        $payable->prev_balance = $payable->balance;
                        $payable->balance += $totalCost;
                        $payable->save();

                        $transaction = new Transaction;
                        $transaction->amount = $totalCost;
                        $transaction->account()->associate($payable);
                        $transaction->naration = '!Credit Purchase #' . $order->receipt_number;
                        $transaction->transaction_reference = $order->receipt_number;
                        $transaction->transaction_type = 'cr';
                        $transaction->user()->associate(Auth::user());
                        $transaction->save();
                    }
                    $supplier = Supplier::find($order->supplier_id);
                    $supplier->prev_balance = $supplier->balance;
                    $supplier->balance += abs($totalCost/* - $order->deposit*/);
                    $supplier->save();
                }else if($request['payment_type'] == 'CASH'){
                    $cash = Account::where('account_no', AccountCode::$ASSETS_CASH)->get()->first();
                    $cash->prev_balance = $cash->balance;
                    $cash->balance -= $totalCost;
                    $cash->save();

                    $transaction = new Transaction;
                    $transaction->amount = $totalCost;
                    $transaction->account()->associate($cash);
                    $transaction->naration = 'Cash Purchase #' . $order->receipt_number;
                    $transaction->transaction_reference = $order->receipt_number;
                    $transaction->transaction_type = 'cr';
                    $transaction->user()->associate(Auth::user());
                    $transaction->save();

                    $purchase = Account::where('account_no', AccountCode::$ASSETS_PURCHASE)->get()->first();
                    $purchase->prev_balance = $purchase->balance;
                    $purchase->balance += $totalCost;
                    $purchase->save();

                    $transaction = new Transaction;
                    $transaction->amount = $totalCost;
                    $transaction->account()->associate($purchase);
                    $transaction->naration = 'Cash Purchase #' . $order->receipt_number;
                    $transaction->transaction_reference = $order->receipt_number;
                    $transaction->transaction_type = 'dr';
                    $transaction->user()->associate(Auth::user());
                    $transaction->save();
                }else if($request['payment_type'] == 'TRANSFER'){
                    $transfer = Account::where('account_no', AccountCode::$ASSETS_BANK . '02')->get()->first();
                    $transfer->prev_balance = $transfer->balance;
                    $transfer->balance -= $totalCost;
                    $transfer->save();

                    $transaction = new Transaction;
                    $transaction->amount = $totalCost;
                    $transaction->account()->associate($transfer);
                    $transaction->naration = 'Transfer/USSD #' . $order->receipt_number;
                    $transaction->transaction_reference = $order->receipt_number;
                    $transaction->transaction_type = 'cr';
                    $transaction->user()->associate(Auth::user());
                    $transaction->save();

                    $purchase = Account::where('account_no', AccountCode::$ASSETS_PURCHASE)->get()->first();
                    $purchase->prev_balance = $purchase->balance;
                    $purchase->balance += $totalCost;
                    $purchase->save();

                    $transaction = new Transaction;
                    $transaction->amount = $totalCost;
                    $transaction->account()->associate($purchase);
                    $transaction->naration = 'Transfer/USSD  #' . $order->receipt_number;
                    $transaction->transaction_reference = $order->receipt_number;
                    $transaction->transaction_type = 'dr';
                    $transaction->user()->associate(Auth::user());
                    $transaction->save();
                }
            });
        } catch(\Exception $e) {
            return Response::json(['status' => false, 'error_code' => 1002]);
        }
        $response = array('status' => true, 'receipt_number' => $order->receipt_number, 'order_id' => $order->id);
        return Response::json($response);
    }

}