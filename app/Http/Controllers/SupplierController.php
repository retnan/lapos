<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use Auth;
use Redirect;
use App\Supplier;
use App\User;
use Session;
use DB;

class SupplierController extends Controller
{    
    public function getSuppliers(){
    	$user = Auth::user();
        // if($user->access_role != "admin"){
        if(Auth::user()->access_role != "admin" && 
            Auth::user()->access_role != "cashier"){
            Session::flash('flash_error', 'You do not have the permission to access the page.');
            return back();
        }
        return view('supplier.index', ['data' => Supplier::with(['user'])->get()]);
    }

    public function show($id){
        $supplier = Supplier::with(['purchase_orders'])->find($id);
        if(!$supplier){
            Session::flash('flash_error', 'Record does not exists');
            return back();
        }
        return view('supplier.show', ['data' => $supplier]);
    }

    public function getAddSupplier(){
    	$user = Auth::user();
        // if($user->access_role != "admin"){
        if(Auth::user()->access_role != "admin" && 
            Auth::user()->access_role != "cashier"){
            Session::flash('flash_error', 'You do not have the permission to access the page.');
            return back();
        }
        return view('supplier.create');
    }

    public function getEditSupplier($id){
        $user = Auth::user();
        // if($user->access_role != "admin"){
        if(Auth::user()->access_role != "admin" && 
            Auth::user()->access_role != "cashier"){
            Session::flash('flash_error', 'You do not have the permission to access the page.');
            return back();
        }
        return view('supplier.edit', ['data' => Supplier::find($id)]);
    }

    public function updateSupplier(Request $request, $id){
        //  if(Auth::user()->access_role != "admin"){
        if(Auth::user()->access_role != "admin" && 
            Auth::user()->access_role != "cashier"){
            Session::flash('flash_error', 'You do not have the permission to access the page.');
            return back();
        }
        
        $this->validate($request, [
            'title' => 'required',
            'mobile_number' => 'required',
            'address' => 'required',
        ]);
        
        try {
            DB::transaction(function() use($request, $id)
            {

                $nSupplier = Supplier::find($id);
                $nSupplier->title = $request->input('title');
                $nSupplier->address = $request->input('address');
                $nSupplier->mobile_number = $request->input('mobile_number');
                $nSupplier->save();
            
            });
        } catch(\Exception $e) {
            Session::flash('flash_message', 'Sorry an Error Occurred');
            return back();
        }
                Session::flash('flash_message', 'Successfuly Updated');
        return back();
    }

    public function addSupplier(Request $request){
        // if(Auth::user()->access_role != "admin"){
        if(Auth::user()->access_role != "admin" && 
            Auth::user()->access_role != "cashier"){
            Session::flash('flash_error', 'You do not have the permission to access the page.');
            return back();
        }
        
        $this->validate($request, [
        	'title' => 'required|unique:suppliers',
            'mobile_number' => 'required',
            'address' => 'required',
        ]);
        
        //the validate method ensures we don't get here on failure

        /** Generator TODO: Fire an Event Instead and do it from single source */

        try {
            DB::transaction(function() use($request)
            {
                $nSupplier = new Supplier;
                $nSupplier->title = $request->input('title');
                $nSupplier->address = $request->input('address');
                $nSupplier->mobile_number = $request->input('mobile_number');
                $nSupplier->user_id = Auth::user()->id;
                $nSupplier->save();

            });
        } catch(\Exception $e) {
            Session::flash('flash_message', 'Sorry, an Error Occured');
            return back();
        }
                Session::flash('flash_message', 'Successfuly Saved');
        return back();
    }
}
