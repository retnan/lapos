<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Discount;
use Carbon\Carbon;
use Auth;
use App\Product;
use Session;
use Response;

class DiscountController extends Controller
{
    public function getAddDiscount(){
    	$user = Auth::user();
        if($user->access_role  != "admin"){
            Session::flash('flash_error', 'You do not have the permission to access the page.');
            return back();
        }
        return view('discount.create');
    }

    public function queryDiscount(){
        $query = isset($_GET['code']) ? $_GET['code'] : '';
        $expired_in_days =  (0 * 24 * 60 * 60) + time();
        $data = Discount::where('coupon', '=', $query)
            ->where('expired_in', '>', $expired_in_days)->get();
        if($data && count($data) > 0){
            $response = array('status' => true, 'data' => $data);
            return Response::json($response);
        }else{
            return Response::json(['status' => false, 'error_code' => 1002]);
        }
    }

    public function getDiscounts(){
    	$user = Auth::user();
        if($user->access_role != "admin"){
            Session::flash('flash_error', 'You do not have the permission to access the page.');
            return back();
        }
        return view('discount.index', ['data' => Product::get()]);
    }

    public function addDiscount(Request $request){
        if(Auth::user()->access_role != "admin"){
            Session::flash('flash_error', 'You do not have the permission to access the page.');
            return back();
        }

        $messages = [
    		'expired_in.required' => 'The validity field is required!',
    		'expired_in.numeric' => 'The validity field represents the number of days from now that this discount will expire.',
    		'expired_in.integer' => 'The validity field must be an integer!',
    		'amount.required' => 'The amount field is required!',
    		'amount.numeric' => 'The amount field must be a number e.g if the discount is 1.5% enter 1.5 or for N500 enter 500 or 500.00',
		];
        
        $this->validate($request, [
        	'amount' => 'required|numeric',
        	'expired_in' => 'required|integer|min:1',
        	'type' => 'required|in:percentage,absolute',
            'threshold_quantity' => 'sometimes|integer|min:0',
            'frequency' => 'sometimes|integer|min:1',
        	'coupon' => 'sometimes|string|'
        ], $messages);

        if(strlen(trim($request->input('coupon'))) < 3){
			$couponCode = uniqid();
		}else{
			$couponCode = trim($request->input('coupon'));
        }

        $inputdate = (int) $request->input('expired_in');
        $expired_in_days =  ($inputdate * 24 * 60 * 60) + time();
        $data = array();
        $selected = array();
        $selected = $request->get('selected');
        if($selected == null || count($selected) == 0){
            $data = array(
                'amount' => $request->input('amount'),
                'coupon' => $couponCode,
                'description' => $request->input('description'),
                'type' => $request->input('amount') < 1 ? 'percentage' : 'absolute',
                'expired_in' => $expired_in_days,
                'threshold_quantity' => $request->input('threshold_quantity'),
                'frequency' => strlen($request->input('frequency')) ? $request->input('frequency') : null,
                'user_id'=> Auth::user()->id,
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
            ) ;
        }else{
            for($i = 0; $i < count($selected); $i++){
                $data[$i] = array(
                    'amount' => $request->input('amount'),
                    'coupon' => $couponCode,
                    'description' => $request->input('description'),
                    'type' => $request->input('amount') < 1 ? 'percentage' : 'absolute',
                    'expired_in' => $expired_in_days,
                    'threshold_quantity' => $request->input('threshold_quantity'),
                    'frequency' => strlen($request->input('frequency')) ? $request->input('frequency') : null,
                    'product_id' => $selected[$i],
                    'user_id'=> Auth::user()->id,
                    'created_at' => Carbon::now(),
                    'updated_at' => Carbon::now(),
                );
            }
        }
        // dd($data);
        Discount::insert($data);
        Session::flash('flash_message', 'Successfuly Saved');
        return back();
    }
}
