<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use DB;
use Response;
use App\Product;
use App\Supplier;
use App\Stock;
use App\Customer;
use App\SaleOrder;
use App\SaleReturn;
use App\SaleReturnOrder;
use App\PurchaseOrder;
use App\PurchaseReturn;
use App\PurchaseReturnOrder;
use Auth;


class SearchController extends Controller
{
    public function search(Request $request){
        $search_term = $request->input('q');
        $searchType = $request->input('type');
        $results = array();
        $results['results'] = array();
        $queries = Product::leftJoin('categories','products.category_id','=','categories.id')
            ->where('products.title', 'LIKE', '%' . $search_term .'%')
            ->orWhere('products.description', 'LIKE', '%' . $search_term .'%')
            ->select([
            	'products.id as id',
            	'products.title as title',
              'products.tax',
            	'products.description as description',
            	'products.unit_cost_price',
            	'categories.title as category',])
            ->take(5)
            ->get();
        if($searchType == "withCategory"){
          $groupedItems = $queries->groupBy('category');
          foreach ($groupedItems as $key => $value) {
            $results['results'][$key] = new \stdClass();
            $results['results'][$key]->name = $key;
            $results['results'][$key]->results = $groupedItems[$key];
          }
        }else{
          $results['results'] = $queries;
        }
        return Response::json($results);
    }

    public function universalSearch(Request $request){

        $user = Auth::user();

        $products = json_decode(json_encode($this->search($request)->getData()), true);//TODO: inject withCategory instead of from js
        $saleOrders = json_decode(json_encode($this->searchSaleOrder($request)->getData()), true);
        $purchaseOrders = json_decode(json_encode($this->searchPurchaseOrder($request)->getData()), true);
        $suppliers = json_decode(json_encode($this->searchSupplier($request)->getData()), true);
        $customers = json_decode(json_encode($this->searchCustomer($request)->getData()), true);
        
        if($user->access_role == "admin"){
            foreach ($products['results'] as $key => $category) {
                foreach ($products['results'][$key]['results'] as $mey => $item) {
                    $products['results'][$key]['results'][$mey]['url'] = '/dashboard/admin/product/' . $item['id'];
                }
            }
        }else{
            $products['results'] = array();
        }

        $products['results']['SALE_ORDER'] = array('name' => 'Sale order', 'results' => array());
        foreach ($saleOrders['results'] as $key => $item) {
            $item['url'] = '/dashboard/pos/' . $item['receipt_number'];
            array_push($products['results']['SALE_ORDER']['results'], $item);
        }

        if($user->access_role == "admin"){
        $products['results']['PURCHASE_ORDER'] = array('name' => 'Purchase order', 'results' => array());
        foreach ($purchaseOrders['results'] as $key => $item) {
            $item['url'] = '/dashboard/admin/receivings/' . $item['receipt_number'];
            array_push($products['results']['PURCHASE_ORDER']['results'], $item);
        }
        }

        $products['results']['CUSTOMER'] = array('name' => 'Customer', 'results' => array());
        foreach ($customers['results'] as $key => $item) {
            $item['url'] = '/dashboard/customer/' . $item['id'];
            array_push($products['results']['CUSTOMER']['results'], $item);
        }

        $user = Auth::user();
        if($user->access_role == "admin"){
            $products['results']['SUPPLIER'] = array('name' => 'Supplier', 'results' => array());
            foreach ($suppliers['results'] as $key => $item) {
                $item['url'] = '/dashboard/admin/supplier/' . $item['id'];
                array_push($products['results']['SUPPLIER']['results'], $item);
            }
        }
        return Response::json($products);
    }

    public function searchSaleOrder(Request $request){
        $search_term = $request->input('q'); //order # or Invoice/Receipt #
        $saleOrder = SaleOrder::with(['customer', 'sales', 'sales.product', 
            'sale_return_orders', 'sale_return_orders.sale_returns', 'sale_return_orders.sale_returns.product'])->where('id', 'LIKE', '%' . $search_term . '%')
        ->orWhere('receipt_number', 'LIKE', '%' . $search_term . '%')
        ->take(5)
        ->get(); 
        $results = array();
        $results['results'] = array();
        $results['results'] = $saleOrder->map(function ($item, $key) {
            $item->title = $item->receipt_number;
            $item->description = $item->customer->title . ' (' . $item->customer->mobile_number .')';
            return $item;
        });
        return Response::json($results);
    }

    public function searchPurchaseOrder(Request $request){
        $search_term = $request->input('q'); //order # or Invoice/Receipt #
        $purchaseOrder = PurchaseOrder::with(['supplier', 'purchases', 'purchases.product', 
            'purchase_return_orders', 'purchase_return_orders.purchase_returns', 'purchase_return_orders.purchase_returns.product'])->where('id', 'LIKE', '%' . $search_term . '%')
        ->orWhere('receipt_number', 'LIKE', '%' . $search_term . '%')
        ->take(5)
        ->get(); 
        $results = array();
        $results['results'] = array();
        $results['results'] = $purchaseOrder->map(function ($item, $key) {
            $item->title = $item->receipt_number;
            $item->description = $item->supplier->title . ' (' . $item->supplier->mobile_number .')';
            return $item;
        });
        return Response::json($results);
    }

    public function searchStocks(Request $request){
        $search_term = $request->input('q');
        $results = array();
        $results['results'] = array();
        $queries = Stock::with(['product'])
        ->select(DB::raw("product_id, COALESCE((SUM(quantity)), 0) - COALESCE(S.SQty, 0) AS stock_quantity FROM stocks LEFT JOIN (SELECT product_id AS a, SUM(quantity) as SQty FROM sales WHERE product_id = $search_term GROUP BY a) AS S ON (product_id = a) WHERE product_id = $search_term GROUP BY product_id --"))
        ->get()->first();
        $results['results'] = $queries;
        return Response::json($results);
    }

    public function searchSupplier(Request $request){
        $search_term = $request->input('q');
        $results = array();
        $results['results'] = array();
        $queries = Supplier::where('title', 'LIKE', '%' . $search_term .'%')
            ->orWhere('Address', 'LIKE', '%' . $search_term .'%')
            ->select([
                'id',
                'address as description',
                'title',
                'mobile_number'])
            ->take(5)
            ->get();
        $results['results'] = $queries->map(function ($item, $key) {
            if (strlen($item->mobile_number) > 8){
                $item->address = $item->description;
                $item->description .= ' (' . $item->mobile_number .')';
            }
            return $item;
        });
        return Response::json($results);
    }

    public function searchCustomer(Request $request){
        $search_term = $request->input('q');
        $results = array();
        $results['results'] = array();
        $queries = Customer::where('title', 'LIKE', '%' . $search_term .'%')
            ->orWhere('Address', 'LIKE', '%' . $search_term .'%')
            ->select([
                'id',
                'address as description',
                'title',
                'mobile_number'])
            ->take(5)
            ->get();
        $results['results'] = $queries->map(function ($item, $key) {
            if (strlen($item->mobile_number) > 8){
                $item->address = $item->description;
                $item->description .= ' (' . $item->mobile_number .')';
            }
            return $item;
        });
        return Response::json($results);
    }
}