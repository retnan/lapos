<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use App\Category;
use Auth;
use Redirect;
use Session;
use DB;

class CategoryController extends Controller
{
    
    public function getAddCategory(){
    	$user = Auth::user();
        // if($user->access_role  != "admin"){
        if(Auth::user()->access_role != "admin" && 
            Auth::user()->access_role != "cashier"){
            Session::flash('flash_error', 'You do not have the permission to access the page.');
            return back();
        }
        return view('category.create');
    }

    public function getCategories(){
    	$user = Auth::user();
        // if($user->access_role != "admin"){
        if(Auth::user()->access_role != "admin" && 
            Auth::user()->access_role != "cashier"){
            Session::flash('flash_error', 'You do not have the permission to access the page.');
            return back();
        }
        return view('category.index', ['data' => Category::get()]);
    }

    public function getEditCategory($id){
        $user = Auth::user();
        // if($user->access_role != "admin"){
        if(Auth::user()->access_role != "admin" && 
            Auth::user()->access_role != "cashier"){
            Session::flash('flash_error', 'You do not have the permission to access the page.');
            return back();
        }
        return view('category.edit', ['data' => Category::find($id)]);
    }

    public function updateCategory(Request $request, $id){
        // if(Auth::user()->access_role != "admin"){
        if(Auth::user()->access_role != "admin" && 
            Auth::user()->access_role != "cashier"){
            Session::flash('flash_error', 'You do not have the permission to access the page.');
            return back();
        }
        
        $this->validate($request, [
            'title' => 'required',
        ]);

        $nCategory = Category::find($id);
        $nCategory->title = $request->input('title');
        $nCategory->description = $request->input('description');
        $nCategory->user()->associate(Auth::user());
        $nCategory->save();
        Session::flash('flash_message', 'Successfuly Saved');
        return back();
    }

    public function addCategory(Request $request)
    {
        // if(Auth::user()->access_role != "admin"){
        if(Auth::user()->access_role != "admin" && 
            Auth::user()->access_role != "cashier"){
            Session::flash('flash_error', 'You do not have the permission to access the page.');
            return back();
        }
        
        $this->validate($request, [
        	'title' => 'required|unique:categories',
        ]);
        
        //the validate method ensures we don't get here on failure
        try{
            
            DB::transaction(function() use($request)
            {

                $nCategory = new Category;
                $nCategory->title = $request->input('title');
                $nCategory->description = $request->input('description');
                $nCategory->user()->associate(Auth::user());
                $nCategory->save();
                
                // Session::flash('flash_message', 'Successfuly Saved');
                // return back();
            });

        } catch(\Exception $e){
            // die(var_dump($e));
            
            Session::flash('flash_message', 'Sorry, an Error Occured');
            return back();
        
        }
        
        Session::flash('flash_message', 'Successfuly Saved!');
        return back();
    }

    
    // public function delete($id){
        
    // }
    public function delete($id)
    {
        //Delete student registration profile
        $record = Category::find($id);
        if($record){
            try{
                $record->delete();
                Session::flash('flash_message', "Record Deleted!");
                return redirect()->route('category_list');
            }catch(\Exception $e){
                // die(var_dump($e));
                
                if($e->errorInfo[0] == 23000){
                    Session::flash('flash_message', "This record cannot be deleted, it depends on a number of records!");   
                }else{
                    Session::flash('flash_message', "An unknown error occured!");
                }
                return back();
            }
        }
    }
}
