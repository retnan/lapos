<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use Auth;
use App\Product;
use App\Stock;
use Redirect;
use Session;
use App\Warehouse;
use App\WarehouseMovedItem;
use App\WarehouseItem;
use DB;
use Cache;


class StockController extends Controller
{
    private $AppConfig;

    public function __construct()
    {
        $this->AppConfig = Cache::get('AppConfig');
    }

	public function getAddStock(){
    	$user = Auth::user();
        if($user->access_role  != "admin"){
            Session::flash('flash_error', 'You do not have the permission to access the page.');
            return back();
        }

        $rawWarehouses = Warehouse::get(['id', 'title']);
        $warehouses = [];
        //TODO; Subject to AppConfig (Warehouse support)
        $warehouses['default'] = "Select Warehouse";

        for ($i=0; $i < count($rawWarehouses); $i++) {
            $warehouses[$rawWarehouses[$i]->id] = $rawWarehouses[$i]->title;
        }

        return view('stock.add',['warehouses' => $warehouses]);
    }

	public function getStocks(){
    	$user = Auth::user();
        if(Auth::user()->access_role != "admin" && 
            Auth::user()->access_role != "cashier"){
            Session::flash('flash_error', 'You do not have the permission to access the page.');
            return back();
        }

        $stockItems = Stock::with(['product'])
        ->select(DB::raw("product_id, COALESCE((SUM(quantity)), 0) - COALESCE(S.SQty, 0) AS Qty FROM stocks LEFT JOIN (SELECT product_id AS a, SUM(quantity) as SQty FROM sales GROUP BY a) AS S ON (product_id = a) GROUP BY product_id --"))
        ->get();

        return view('stock.index', ['data' => $stockItems]);
    }

    public function addStock(Request $request){

        if(Auth::user()->access_role != "admin" ){
            Session::flash('flash_error', 'You do not have the permission to access the page.');
            return back();
        }

        $messages = [
            'product_id.required' => 'The product field is required',
            'product_id.numeric' => 'The product field is required',
        ];
        
        $this->validate($request, [
            'quantity' => 'required|integer|min:1',
            'product_id' => 'required|numeric',
            'warehouse_id' => 'sometimes|integer',
            'direct' => 'sometimes|in:on,off'
        ], $messages);

        if (!$this->AppConfig->supports_warehouse || $request->input('direct') == "on") {
            $product = Product::find($request->input('product_id'));
            $stock = new Stock;
            $stock->quantity = $request->input('quantity');
            $stock->unit_cost_price = $product->unit_cost_price;
            $stock->unit_selling_price = $product->unit_selling_price;
            $stock->product()->associate($product);
            $stock->user()->associate(Auth::user());
            $stock->save();
            Session::flash('flash_message', 'Successfuly Saved');
            return back();
        }else{

            if ($this->AppConfig->supports_warehouse && !$request->has('warehouse_id')) return back()->withErrors("You have not selected the warehouse you want to move the product from");
            if ( (int) $request->has('product_id') < 1) return back()->withErrors("The product field is required");

            if (!$this->canTakeFromWarehouse($request->input('product_id'), 
                $request->input('warehouse_id'), $request->input('quantity')
                )) {
                return back()->withErrors("The quantity is too much, please verify the amount of products available in the warehouse.");
            }

            $stock = new Stock;
            $stock->quantity = $request->input('quantity');
            $stock->product()->associate(Product::find($request->input('product_id')));
            $stock->user()->associate(Auth::user());
            $stock->save();

            $warehouseMovedItem = new WarehouseMovedItem;
            $warehouseMovedItem->quantity = $request->input('quantity');
            $warehouseMovedItem->warehouse()->associate(Warehouse::find($request->input('warehouse_id')));
            $product = Product::find($request->input('product_id'));
            $warehouseMovedItem->product()->associate($product);
            $warehouseMovedItem->unit_cost_price = $product->unit_cost_price;
            $warehouseMovedItem->unit_selling_price = $product->unit_selling_price;
            $warehouseMovedItem->tax = $product->tax;
            $warehouseMovedItem->user()->associate(Auth::user());
            $warehouseMovedItem->save();
            Session::flash('flash_message', 'Successfuly Saved');
            return back();
        }
    }

    private function canTakeFromWarehouse($product_id, $warehouseId, $quantity){
        $warehouseItem = WarehouseItem::with(['product','warehouse'])
        ->select(DB::raw("product_id, warehouse_id, COALESCE((SUM(quantity)), 0) - COALESCE(S.SQty, 0) AS Qty FROM warehouse_items LEFT JOIN (SELECT product_id AS a, warehouse_id AS b, SUM(quantity) as SQty FROM warehouse_moved_items WHERE product_id = $product_id AND warehouse_id = $warehouseId GROUP BY a, b) AS S ON (product_id = a AND warehouse_id = b) WHERE product_id = $product_id AND warehouse_id = $warehouseId GROUP BY product_id, warehouse_id --"))
        ->get()->first();
        if($warehouseItem && $quantity <= $warehouseItem->Qty){
            return true;
        }else{
            return false;
        }
        // die($warehouseItems->toJson());
    }  
}