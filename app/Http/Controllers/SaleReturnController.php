<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Warehouse;
use Auth;
use App\SaleReturnOrder;
use App\SaleReturn;
use App\Customer;
use Response;
use DB;
use App\Sale;
use App\SaleOrder;
use Carbon\Carbon;
use App\Account;
use LaPOS\AccountCode;
use App\Transaction;
use App\Stock;

class SaleReturnController extends Controller
{
    public function form(){
    	return view('sale_return.form', ['warehouses' => Warehouse::get()->pluck('title','id')]);
    }

    private function validate_cart($request){
        if ($request->has('customer') && $request->has('products') 
        	&& $request->has('receipt_number') && $request->has('payment_type')) return true;
        return false;
    }

    public function getReceipt(Request $request, $receiptId){
        $printable = false;
        if($request->has('q')){
            $printable = true;
        }
        $data = SaleReturnOrder::with(['sale_order.sales', 'sale_returns', 'customer', 'sale_returns.product'])
        ->where('receipt_number', $receiptId)->get()->first();
        return view('sale_return.receipt', ['data' => $data, 'isPrintable' => $printable]);
    }

    public function returnItems(Request $request){
    	$user = Auth::user();
        if($user->access_role != "admin"){
            Session::flash('flash_error', 'You do not have the permission to access the page.');
            return back();
        }
        if(!$this->validate_cart($request)){
            return Response::json(['status' => false, 'error_code' => 1061]);
        }

        $saleReturnOrder = new SaleReturnOrder;
        try {
            DB::transaction(function() use($request, $saleReturnOrder) {

            	$request = $request->all();
                $customer = $request['customer'];
                $products = $request['products'];
                $saleOrder = SaleOrder::with('sales')
                ->where('receipt_number', $request['receipt_number'])->get()->first();
                $sales = $saleOrder->sales;

                if(!$saleOrder && count($sales) == 0){
                	throw new \Exception("Sale 404", 1);
                }

                $saleReturnOrder->receipt_number = $this->genReceiptNumber();
                $saleReturnOrder->customer()->associate(Customer::find($customer['customer_id']));
                if (array_key_exists('payment_type', $request) && 
                    $request['payment_type'] == 'CASH') {
                    $saleReturnOrder->payment_type = 'CASH';
                    $saleReturnOrder->receipt_number = $this->genReceiptNumber();
                }else if (array_key_exists('payment_type', $request) && 
                    $request['payment_type'] == 'TRANSFER') {
                    $saleReturnOrder->payment_type = 'TRANSFER';
                    $saleReturnOrder->receipt_number = $this->genReceiptNumber();
                }else if(array_key_exists('payment_type', $request) && 
                    $request['payment_type'] == 'CREDIT'){
                    $saleReturnOrder->payment_type = 'CREDIT';
                    $saleReturnOrder->receipt_number = $this->genReceiptNumber();
                }else{
                	throw new \Exception("payment_type 404", 1);
                }
                $saleReturnOrder->sale_order()->associate($saleOrder);
                $saleReturnOrder->user()->associate(Auth::user());
                $saleReturnOrder->save();

                $cart = array();
                $cartToCommit = array();
                $totalCost = 0;
                foreach ($products as $product) {
                    if(0 == $product['quantity']) continue;
                    foreach ($sales as $item) {
                    	if($item->product_id == $product['product_id']){
                            if($product['quantity'] > $item->quantity) throw new \Exception("Quantity Overflow", 1);
                    		array_push($cart, array('quantity' => $product['quantity'], 
                    			'unit_cost_price' => $item->unit_cost_price, 
                    			'unit_selling_price' => $item->unit_selling_price, 
                    			'product_id' => $product['product_id'],
                    			'sale_return_order_id' => $saleReturnOrder->id, 'seller_id' => Auth::user()->id, 
                    			'tax' => $item->tax, 
                    			'created_at' => Carbon::now(), 'updated_at' => Carbon::now()));
                            array_push($cartToCommit, array('quantity' => $product['quantity'], 
                                'user_id' => Auth::user()->id,
                                'product_id' => $product['product_id'],
                                'created_at' => Carbon::now(), 'updated_at' => Carbon::now()));
                    		$subTotal = $product['quantity'] * $item->unit_selling_price;
                    		$totalCost += (($item->tax / 100)  * $subTotal) + $subTotal;
                    		break;
                    	}
                    }
                }
                //FOR DEBUGING PURPOSE
                if( array_key_exists('total_cost', $request) && 
                	$totalCost != $request['total_cost'] )throw new \Exception("Total Mismatch", 1);

                $saleReturnOrder->total_amount = $totalCost;
                $saleReturnOrder->save();
                SaleReturn::insert($cart);

                if($request['payment_type'] == 'CREDIT'){
                	$customer = Customer::find($saleReturnOrder->customer_id);
                    $customer->prev_balance = $customer->balance;
                    $customer->balance += $totalCost;
                    $customer->save();

                    $payable = Account::where('account_no', AccountCode::$LIABILITY_PAYABLE)->get()->first();
                    $payable->prev_balance = $payable->balance;
                    $payable->balance += $totalCost;
                    $payable->save();

                    $transaction = new Transaction;
                    $transaction->amount = $totalCost;
                    $transaction->account()->associate($payable);
                    $transaction->naration = 'Return Items as (STORE CREDIT) Ref #' . $saleOrder->receipt_number . ', #' . $saleReturnOrder->receipt_number;
                    $transaction->transaction_reference = $saleReturnOrder->receipt_number;
                    $transaction->transaction_type = 'cr';
                    $transaction->user()->associate(Auth::user());
                    $transaction->save();

                    $salesReturnAcct = Account::where('account_no', AccountCode::$ASSETS_SALES_RETURN)->get()->first();
                    $salesReturnAcct->prev_balance = $salesReturnAcct->balance;
                    $salesReturnAcct->balance += $totalCost; //Cash Paid out
                    $salesReturnAcct->save();

                    $transaction = new Transaction;
                    $transaction->amount = $totalCost;
                    $transaction->account()->associate($salesReturnAcct);
                    $transaction->naration = 'Return Items as STORE CREDIT (Items Retured) Ref #' . $saleOrder->receipt_number . ', #' . $saleReturnOrder->receipt_number;
                    $transaction->transaction_reference = $saleReturnOrder->receipt_number;
					$transaction->transaction_type = 'dr';
					$transaction->user()->associate(Auth::user());
					$transaction->save();

                }else if($request['payment_type'] == 'CASH'){
                	$payable = Account::where('account_no', AccountCode::$LIABILITY_PAYABLE)->get()->first();
                    $payable->prev_balance = $payable->balance;
                    $payable->balance += $totalCost;
                    $payable->save();

                    $transaction = new Transaction;
                    $transaction->amount = $totalCost;
                    $transaction->account()->associate($payable);
                    $transaction->naration = 'Return Items (CASH) Ref #' . $saleOrder->receipt_number . ', #' . $saleReturnOrder->receipt_number;
                    $transaction->transaction_reference = $saleReturnOrder->receipt_number;
                    $transaction->transaction_type = 'dr';
                    $transaction->user()->associate(Auth::user());
                    $transaction->save();

                    $cash = Account::where('account_no', AccountCode::$ASSETS_CASH)->get()->first();
                    $cash->prev_balance = $cash->balance;
                    $cash->balance += $totalCost; //Cash Paid out
                    $cash->save();

                    $transaction = new Transaction;
                    $transaction->amount = $totalCost;
                    $transaction->account()->associate($cash);
                    $transaction->naration = 'Return Cash to Customer (Items Retured) Ref #' . $saleOrder->receipt_number . ', #' . $saleReturnOrder->receipt_number;
                    $transaction->transaction_reference = $saleReturnOrder->receipt_number;
					$transaction->transaction_type = 'cr';
					$transaction->user()->associate(Auth::user());
					$transaction->save();
                }else if($request['payment_type'] == 'TRANSFER'){
                	$payable = Account::where('account_no', AccountCode::$LIABILITY_PAYABLE)->get()->first();
                    $payable->prev_balance = $payable->balance;
                    $payable->balance += $totalCost;
                    $payable->save();

                    $transaction = new Transaction;
                    $transaction->amount = $totalCost;
                    $transaction->account()->associate($payable);
                    $transaction->naration = 'Return Items (TRANSFER/USSD) Ref #' . $saleOrder->receipt_number . ', #' . $saleReturnOrder->receipt_number;
                    $transaction->transaction_reference = $saleReturnOrder->receipt_number;
                    $transaction->transaction_type = 'dr';
                    $transaction->user()->associate(Auth::user());
                    $transaction->save();

                    $transfer = Account::where('account_no', AccountCode::$ASSETS_BANK . '02')->get()->first();
                    $transfer->prev_balance = $transfer->balance;
                    $transfer->balance += $totalCost; //Cash Paid out via transfer/ussd
                    $transfer->save();

                    $transaction = new Transaction;
                    $transaction->amount = $totalCost;
                    $transaction->account()->associate($transfer);
                    $transaction->naration = 'Return Cash via Transfer/USSD to Customer (Items Retured) Ref #' . $saleOrder->receipt_number . ', #' . $saleReturnOrder->receipt_number;
                    $transaction->transaction_reference = $saleReturnOrder->receipt_number;
					$transaction->transaction_type = 'cr';
					$transaction->user()->associate(Auth::user());
					$transaction->save();
                }

                if( array_key_exists('warehouse', $request) ){
                    //TODO: Copy items to warehouse
                    //$request['warehouse']['warehouse_id'];
                }else{
                    //return the items to stock
                    Stock::insert($cartToCommit);                    
                }

            });
        } catch(\Exception $e) {
            // die($e);
            return Response::json(['status' => false, 'error_code' => 1002]);
        }
        $response = array('status' => true, 'receipt_number' => $saleReturnOrder->receipt_number, 'order_id' => $saleReturnOrder->id);
        return Response::json($response);
    }

    private function genReceiptNumber(){
        $result = 'RRPT';
        for ($i=0; $i < 10; $i++) $result .= mt_rand(0, 9);
        return trim($result);
    }
}
