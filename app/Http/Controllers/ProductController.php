<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use Response;
use Auth;
use App\Product;
use App\Category;
use Redirect;
use Session;
use DB;
use App\PurchaseOrder;
use App\Purchase;
use App\Supplier;
use App\Stock;
use App\WarehouseItem;
use Cache;
use Carbon\Carbon;
use App\Warehouse;
use App\Account;
use LaPOS\AccountCode;
use App\Transaction;

class ProductController extends Controller
{

    private $AppConfig;

    public function __construct()
    {
        $this->AppConfig = Cache::get('AppConfig');
    }

    public function getAddProduct(){
    	$user = Auth::user();
        if(Auth::user()->access_role != "admin" && 
            Auth::user()->access_role != "cashier"){
            Session::flash('flash_error', 'You do not have the permission to access the page.');
            return back();
        }

        $categories = Category::get()->pluck('title','id');
        return view('product.create',['categories' => $categories]);
    }

    public function showProduct($id){
        $user = Auth::user();
        if(Auth::user()->access_role != "admin" && 
            Auth::user()->access_role != "cashier"){
            Session::flash('flash_error', 'You do not have the permission to access the page.');
            return back();
        }
        return view('product.show', ['data' => Product::with('category')->find($id)]);
    }

    public function getProducts(){
    	$user = Auth::user();
        if(Auth::user()->access_role != "admin" && 
            Auth::user()->access_role != "cashier"){
            Session::flash('flash_error', 'You do not have the permission to access the page.');
            return back();
        }
        return view('product.index', ['data' => Product::with('category')->get()]);
    }

    public function getEditProduct($id){
        $user = Auth::user();
        if(Auth::user()->access_role != "admin" && 
            Auth::user()->access_role != "cashier"){
            Session::flash('flash_error', 'You do not have the permission to access the page.');
            return back();
        }

        $rawCategories = Category::get(['id', 'title']);
        $categories = [];

        for ($i=0; $i < count($rawCategories); $i++) {
            $categories[$rawCategories[$i]->id] = $rawCategories[$i]->title;
        }

        return view('product.edit', ['data' => Product::with('category')->find($id), 
            'categories' => $categories]);
    }

    public function updateProduct(Request $request, $id){
        if(Auth::user()->access_role != "admin" && 
            Auth::user()->access_role != "cashier"){
            Session::flash('flash_error', 'You do not have the permission to access the page.');
            return back();
        }
        
        $this->validate($request, [
            'title' => 'required',
            'unit_selling_price' => 'required|numeric',
            'unit_cost_price' => 'required|numeric',
            'category_id' => 'required|numeric',
            'tax' => 'sometimes|numeric|min:0',
            'min_threshold' => 'sometimes|integer|min:0',
        ]);

        try {
            DB::transaction(function() use($request, $id) {


                $nProduct = Product::find($id);
                if(!$nProduct) return back()->withErrors("Invalid product");
                $nProduct->title = $request->input('title');
                $nProduct->description = $request->input('description');
                $nProduct->unit_selling_price = $request->input('unit_selling_price');
                $nProduct->unit_cost_price = $request->input('unit_cost_price');
                $nProduct->tax = $request->has('tax') ? $request->input('tax') : 0;
                $nProduct->min_threshold = $request->input('min_threshold');
                $nProduct->tag = strtolower($request->input('tag'));
                $nProduct->category()->associate(Category::find($request->input('category_id')));
                $nProduct->save();


            });
        } catch(\Exception $e) {
            //die(var_dump($e));

            Session::flash('flash_message', 'Sucessfully Updated');
            return back();
        }

        Session::flash('flash_message', 'Successfuly Saved');
        return back();
    }

    public function addProduct(Request $request){
        if(Auth::user()->access_role != "admin" && 
            Auth::user()->access_role != "cashier"){
            Session::flash('flash_error', 'You do not have the permission to access the page.');
            return back();
        }
        
        $this->validate($request, [
        	'title' => 'required',
            'unit_selling_price' => 'required|numeric',
            'unit_cost_price' => 'required|numeric',
            'category_id' => 'required|numeric',
            'tax' => 'sometimes|numeric|min:0',
            'min_threshold' => 'sometimes|integer|min:0'
        ]);
        
        //the validate method ensures we don't get here on failure
        try {
            DB::transaction(function() use($request)
            {


                $nProduct = new Product;
                $nProduct->title = $request->input('title');
                $nProduct->description = $request->input('description');
                $nProduct->unit_selling_price = $request->input('unit_selling_price');
                $nProduct->unit_cost_price = $request->input('unit_cost_price');
                $nProduct->tax =  $request->has('tax') ? $request->input('tax') : 0;
                $nProduct->min_threshold = $request->input('min_threshold');
                $nProduct->tag = strtolower($request->input('tag'));
                $nProduct->category()->associate(Category::find($request->input('category_id')));
                $nProduct->user()->associate(Auth::user());
                $nProduct->save();
            });
        } catch(\Exception $e) {
            //die(var_dump ($e));

            Session::flash('flash_message', 'Sorry, an Error Occured');
            return back();
        }

        Session::flash('flash_message', 'Successfuly Saved');
        return back();
    }

    //delete product
    public function delete($id)
    {
        //Delete product 
        $record = Product::find($id);
        if($record){
            try{
                $record->delete();
                Session::flash('flash_message', "Record Deleted!");
                return redirect()->route('product_list');
            }catch(\Exception $e){
                // die(var_dump($e));
                
                if($e->errorInfo[0] == 23000){
                    Session::flash('flash_message', "This record cannot be deleted, it depends on a number of records!");   
                }else{
                    Session::flash('flash_message', "An unknown error occured!");
                }
                return back();
            }
        }
    }
}