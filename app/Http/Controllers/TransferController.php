<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Transfer;
use App\Account;
use DB;
use Auth;
use App\Transaction;
use Session;

class TransferController extends Controller
{
    public function index(){
    	$user = Auth::user();
    	if($user->access_role  != "admin"){
            Session::flash('flash_error', 'You do not have the permission to access the page.');
            return back();
        }
    	$transfers = Transfer::with(['source_account', 'destination_account', 'user'])->get();
    	return view('transfer.index', ['data' => $transfers, 'accounts' => Account::get()->pluck('title','account_no')]);
    }

    public function transfer(Request $request){
        $user = Auth::user();
    	if($user->access_role  != "admin"){
            Session::flash('flash_error', 'You do not have the permission to access the page.');
            return back();
        }

    	$this->validate($request, [
        	'amount' => 'required|numeric',
        	'source_account' => 'required|numeric',
        	'destination_account' => 'required|numeric',
        ]);
        if($request->input('source_account') == $request->input('destination_account')){
        	Session::flash('flash_error', 'Please select a different destination account');
            return back();
        }

        $srcAccount = Account::where('account_no', $request->input('source_account'))->get()->first();
        $destAccount = Account::where('account_no', $request->input('destination_account'))->get()->first();
        if(!$srcAccount && !$destAccount){
        	Session::flash('flash_error', 'One or more of the selected accounts does not exist');
            return back();
        }
        // if ($srcAccount->balance < $request->input('amount')) {
        // 	Session::flash('flash_error', 'There is no suffient fund in the source account');
        //     return back();
        // }
        try {
            DB::transaction(function() use($srcAccount, $destAccount, $request) {

            	$newTransfer = new Transfer;
            	$newTransfer->amount = $request->input('amount');
            	$newTransfer->source_account()->associate($srcAccount);
            	$newTransfer->destination_account()->associate($destAccount);
            	$newTransfer->user()->associate(Auth::user());
            	$newTransfer->comment = $request->input('comment');
            	$newTransfer->save();

            	$srcAccount->prev_balance = $srcAccount->balance;
            	$srcAccount->balance -= $request->input('amount');
            	$srcAccount->save();

            	$transaction = new Transaction;
                $transaction->amount = $request->input('amount');
                $transaction->account()->associate($srcAccount);
                $transaction->naration = 'Transfer #' . $newTransfer->id .', ' . $request->input('comment');
                $transaction->transaction_type = 'dr'; //verify
                $transaction->user()->associate(Auth::user());
                $transaction->save();

            	$destAccount->prev_balance = $destAccount->balance;
            	$destAccount->balance += $request->input('amount');
            	$destAccount->save();

            	$transaction = new Transaction;
                $transaction->amount = $request->input('amount');
                $transaction->account()->associate($destAccount);
                $transaction->naration = 'Transfer #' . $newTransfer->id .', ' . $request->input('comment');
                $transaction->transaction_type = 'cr'; //verify
                $transaction->user()->associate(Auth::user());
                $transaction->save();
            });
        }catch(\Exception $e) {
            Session::flash('flash_error', 'An error occured');
            return back();
        }
        Session::flash('flash_message', 'Sucessfully Transfered');
        return back();
    }

    public function createExpenses(){
    	$user = Auth::user();
    	if(Auth::user()->access_role != "admin" && 
            Auth::user()->access_role != "cashier"){
            Session::flash('flash_error', 'You do not have the permission to access the page.');
            return back();
        }
    	$transfers = Transfer::with(['source_account', 'destination_account', 'user'])->get();
    	return view('transfer.expenses', ['data' => $transfers, 'accounts' => Account::get()->pluck('title','account_no')]);
    }

    public function logExpenses(Request $request){
        $user = Auth::user();
    	if(Auth::user()->access_role != "admin" && 
            Auth::user()->access_role != "cashier"){
            Session::flash('flash_error', 'You do not have the permission to access the page.');
            return back();
        }

    	$this->validate($request, [
        	'amount' => 'required|numeric',
        	'source_account' => 'required|numeric',
        	'destination_account' => 'required|numeric',
        ]);
        if($request->input('source_account') == $request->input('destination_account')){
        	Session::flash('flash_error', 'Please select a different destination account');
            return back();
        }

        $srcAccount = Account::where('account_no', $request->input('source_account'))->get()->first();
        $destAccount = Account::where('account_no', $request->input('destination_account'))->get()->first();
        if(!$srcAccount && !$destAccount){
        	Session::flash('flash_error', 'One or more of the selected accounts does not exist');
            return back();
        }
        // if ($srcAccount->balance < $request->input('amount')) {
        // 	Session::flash('flash_error', 'There is no suffient fund in the source account');
        //     return back();
        // }
        try {
            DB::transaction(function() use($srcAccount, $destAccount, $request) {

            	$newTransfer = new Transfer;
            	$newTransfer->amount = $request->input('amount');
            	$newTransfer->source_account()->associate($srcAccount);
            	$newTransfer->destination_account()->associate($destAccount);
            	$newTransfer->user()->associate(Auth::user());
            	$newTransfer->comment = $request->input('comment');
            	$newTransfer->save();

            	$srcAccount->prev_balance = $srcAccount->balance;
            	$srcAccount->balance -= $request->input('amount');
            	$srcAccount->save();

            	$transaction = new Transaction;
                $transaction->amount = $request->input('amount');
                $transaction->account()->associate($srcAccount);
                $transaction->naration = 'Expenses: #' . $newTransfer->id .', ' . $request->input('comment');
                $transaction->transaction_type = 'dr'; //verify
                $transaction->user()->associate(Auth::user());
                $transaction->save();

            	$destAccount->prev_balance = $destAccount->balance;
            	$destAccount->balance += $request->input('amount');
            	$destAccount->save();

            	$transaction = new Transaction;
                $transaction->amount = $request->input('amount');
                $transaction->account()->associate($destAccount);
                $transaction->naration = 'Expenses: #' . $newTransfer->id .', ' . $request->input('comment');
                $transaction->transaction_type = 'cr'; //verify
                $transaction->user()->associate(Auth::user());
                $transaction->save();
            });
        }catch(\Exception $e) {
            Session::flash('flash_error', 'An error occured');
            return back();
        }
        Session::flash('flash_message', 'Sucessfully Transfered');
        return back();
    }
}
