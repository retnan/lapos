<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use Auth;
use Redirect;

use App\User;
use Session;
use App\Config;


class DashboardController extends Controller
{
    public function getConfig(){
        return view('config', ['config' => Config::find(1)]);
    }

    public function putConfig(){

    }

    public function getAdminPage(){
        if(Auth::user()->access_role != "admin"){
            Session::flash('flash_error', 'You do not have the permission to access the page.');
            return back();
        }
        return view('admin', ['data' => null]);
    }

    public function getCachierPage(){
        if(Auth::user()->access_role != "cashier"){
            Session::flash('flash_error', 'You do not have the permission to access the page.');
            return back();
        }
        return view('cashier', ['data' => null]);
    }    
}
