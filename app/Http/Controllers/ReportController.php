<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use Session;
use Auth;
use Carbon\Carbon;
use App\SaleOrder;
use App\Sale;
use App\Stock;
use App\User;
use App\Customer;
use App\Account;
use LaPOS\AccountCode;
use App\Transaction;
use DB;

class ReportController extends Controller
{
    public function transactionReport(Request $request){
    	$from = $request->input('from');
    	$to = $request->input('to');
    	if( $this->isDate($from) && $this->isDate($to) ){
    		$fromDate = Carbon::createFromFormat('Y-m-d', $from)->startOfDay();
        	$toDate = Carbon::createFromFormat('Y-m-d', $to)->endOfDay();
    	}else{
    		$fromDate = Carbon::today()->startOfDay();
    		$toDate = Carbon::today()->endOfDay();
    	}
        $transaction = Transaction::with(['account','user'])
        ->whereBetween('created_at', array($fromDate, $toDate))
        ->get();
        return view('report.transactions',['data' => $transaction, 
        	'from' => $fromDate, 
        	'to' => $toDate]);
	}
	
	public function salesTransaction(Request $request){
		$from = $request->input('from');
	 $to = $request->input('to');
	 if( $this->isDate($from) && $this->isDate($to) ){
		 $fromDate = Carbon::createFromFormat('Y-m-d', $from)->startOfDay();
		 $toDate = Carbon::createFromFormat('Y-m-d', $to)->endOfDay();
	 }else{
		 $fromDate = Carbon::today()->startOfDay();
		 $toDate = Carbon::today()->endOfDay();
	 }
	 $sales = Transaction::with(['account','user'])
	 ->where('account_id', Account::where('account_no', 
		 AccountCode::$REVENUE_SALES)->get()->first()->id)
	 ->whereBetween('created_at', array($fromDate, $toDate))
	 ->get();
	 return view('report.sales_transaction',['data' => $sales, 
		 'from' => $fromDate, 
		 'to' => $toDate]);
	}

    public function salesReport(Request $request){
       	$from = $request->input('from');
    	$to = $request->input('to');
    	if( $this->isDate($from) && $this->isDate($to) ){
    		$fromDate = Carbon::createFromFormat('Y-m-d', $from)->startOfDay();
        	$toDate = Carbon::createFromFormat('Y-m-d', $to)->endOfDay();
    	}else{
    		$fromDate = Carbon::today()->startOfDay();
    		$toDate = Carbon::today()->endOfDay();
    	}
        $sales = Sale::with(['product','seller','sale_order'])
        ->whereBetween('created_at', array($fromDate, $toDate))
        ->get();
        return view('report.sales',['data' => $sales, 
        	'from' => $fromDate, 
        	'to' => $toDate]);
    }

    public function stocksReport(Request $request){
    	$from = $request->input('from');
    	$to = $request->input('to');
    	if( $this->isDate($from) && $this->isDate($to) ){
    		$fromDate = Carbon::createFromFormat('Y-m-d', $from)->startOfDay();
        	$toDate = Carbon::createFromFormat('Y-m-d', $to)->endOfDay();
    	}else{
    		$fromDate = Carbon::today()->startOfDay();
    		$toDate = Carbon::today()->endOfDay();
    	}
        $stocks = Stock::with(['product', 'product.category', 'user'])
        ->whereBetween('created_at', array($fromDate, $toDate))
        ->get();
        return view('report.stock',['data' => $stocks, 
        	'from' => $fromDate, 
        	'to' => $toDate]);
	}
	
	public function stockValue(Request $request){
		$user = Auth::user();
        if(Auth::user()->access_role != "admin" && 
            Auth::user()->access_role != "cashier"){
            Session::flash('flash_error', 'You do not have the permission to access the page.');
            return back();
        }

        $stockItems = Stock::with(['product'])
        ->select(DB::raw("product_id, stocks.unit_selling_price, stocks.unit_cost_price, COALESCE((SUM(quantity)), 0) - COALESCE(S.SQty, 0) AS Qty FROM stocks LEFT JOIN (SELECT product_id AS a, SUM(quantity) as SQty FROM sales GROUP BY a) AS S ON (product_id = a) GROUP BY product_id --"))
		->get();
		$si = $stockItems->groupBy("product.tag");
		// dd($si->toArray());

        return view('report.stock_value', ['data' => $si]);
	}

    private function isDate($date){
    	if(date_create_from_format('Y-m-d', $date)){
    		return true;
    	}else{
    		return false;
    	}
    }

    public function accountReport(){
        $account = Account::get();
        return view('report.account',['data' => $account]);
    }
}