<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Response;

use App\Http\Requests;
use Session;
use Auth;
use Carbon\Carbon;
use App\SaleOrder;
use App\Sale;
use App\Stock;
use App\User;
use App\Customer;
use App\Account;
use LaPOS\AccountCode;
use App\Transaction;
use App\Discount;

class SaleController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function getPOS(){
    	$user = Auth::user();
        if($user->access_role != "admin" && 
            Auth::user()->access_role != "cashier"){
            Session::flash('flash_error', 'You do not have the permission to access the page.');
            return back();
        }
        return view('sale.pos');
    }

    public function markOrderForPayment(Request $request){
        $user = Auth::user();
        if($user->access_role != "admin" && 
            Auth::user()->access_role != "cashier"){
            Session::flash('flash_error', 'You do not have the permission to access the page.');
            return back();
        }

        $order = SaleOrder::where('receipt_number', $request->input('invoice_number'))
        ->where('payment_type', 'CREDIT')->get()->first();
        if(!$order){
            Session::flash('flash_error', "This invoice does not exist.");
            return back();
        }

        try {
            DB::transaction(function() use($order) {

                $order->previous_invoice_number = $order->receipt_number;
                $order->receipt_number = $this->genReceiptNumber();
                $order->payment_type = 'CASH';
                $order->user()->associate(Auth::user());
                $order->save();
                /* Update Accounting Information */
                $customer = Customer::find($order->customer_id);
                $customer->prev_balance = $customer->balance;
                $customer->balance += abs($order->total_amount - $order->deposit);
                $customer->save();


                $receivable = Account::where('account_no', AccountCode::$ASSETS_RECEIVABLE)->get()->first();
                $receivable->prev_balance = $receivable->balance;
                $receivable->balance += abs($order->total_amount - $order->deposit);
                $receivable->save();

                $transaction = new Transaction;
                $transaction->amount = abs($order->total_amount - $order->deposit);
                $transaction->account()->associate($receivable);
                $transaction->naration = 'Customer pays debt obligation #' . $order->receipt_number;
                $transaction->transaction_reference = $order->receipt_number;
                $transaction->transaction_type = 'cr';
                $transaction->user()->associate(Auth::user());
                $transaction->save();

                $cash = Account::where('account_no', AccountCode::$ASSETS_CASH)->get()->first();
                $cash->prev_balance = $cash->balance;
                $cash->balance += abs($order->total_amount - $order->deposit); //Cash
                $cash->save();

                $transaction = new Transaction;
                $transaction->amount = abs($order->total_amount - $order->deposit);
                $transaction->account()->associate($cash);
                $transaction->naration = 'Received cash owed #' . $order->receipt_number;
                $transaction->transaction_reference = $order->receipt_number;
                $transaction->transaction_type = 'dr';
                $transaction->user()->associate(Auth::user());
                $transaction->save();
            });
        } catch(\Exception $e) {
            Session::flash('flash_error', 'An error occured');
            return back();
        }

        Session::flash('flash_message', 'Sucessfully Updated');
        return redirect()->route('sales_receipt', $order->receipt_number);
    }

    public function processOrder(Request $request){
        $user = Auth::user();
        if($user->access_role != "admin" && 
            Auth::user()->access_role != "cashier"){
            Session::flash('flash_error', 'You do not have the permission to access the page.');
            return back();
        }
        if(!$this->validate_cart($request)){
            return Response::json(['status' => false, 'error_code' => 1001]);
        }

        $order = new SaleOrder;
        try {
            DB::transaction(function() use($request, $user, $order) {

                $request = $request->all();
                $customer = $request['customer'];
                $products = $request['products'];

                $order->receipt_number = $this->genReceiptNumber();
                $order->customer()->associate(Customer::find($customer['customer_id']));
                if (array_key_exists('payment_type', $request) && 
                    $request['payment_type'] == 'CASH') {
                    $order->payment_type = 'CASH';
                    $order->receipt_number = $this->genReceiptNumber();
                }else if (array_key_exists('payment_type', $request) && 
                    $request['payment_type'] == 'TRANSFER') {
                    $order->payment_type = 'TRANSFER';
                    $order->receipt_number = $this->genReceiptNumber();
                }else if(array_key_exists('payment_type', $request) && 
                    $request['payment_type'] == 'CREDIT'){
                    $order->payment_type = 'CREDIT';
                    $order->deposit = array_key_exists('deposit_amount', $request) ? $request['deposit_amount'] : 0;
                    $order->receipt_number = $this->genInvoiceNumber();
                }else{
                    throw new \Exception("Error Processing Request", 1);
                }
                $discount_code = $request['discount_code'];
                if($order->payment_type != 'CREDIT'){
                    $order->coupon_code = $discount_code;
                }
                $order->user()->associate(Auth::user());
                $order->save();

                $coupons = [];
                
                if($discount_code && strlen($discount_code) > 0 && $order->payment_type != 'CREDIT'){
                    $expired_in_days =  (0 * 24 * 60 * 60) + time();
                    $coupons = Discount::where('coupon', '=', $discount_code)
                        ->where('expired_in', '>', $expired_in_days)->get();
                }

                $cart = array();
                $totalCost = 0;
                $totalCostCopy = 0;
                foreach ($products as $product) {
                    $search_term = (int) $product['product_id'];
                    $response = Stock::with(['product'])
                    ->select(DB::raw("product_id, COALESCE((SUM(quantity)), 0) - COALESCE(S.SQty, 0) AS stock_quantity FROM stocks LEFT JOIN (SELECT product_id AS a, SUM(quantity) as SQty FROM sales WHERE product_id = $search_term GROUP BY a) AS S ON (product_id = a) WHERE product_id = $search_term GROUP BY product_id --"))
                    ->get()->first();
                    if($response->stock_quantity < $product['quantity'] || $product['quantity'] <= 0){
                        throw new \Exception("Error Processing Request", 1);
                    }
                    $discount = 0;
                    $discount_id = null;
                    if($order->payment_type != 'CREDIT'){
                        //discount not supported for partial payments
                        for($j = 0;  $j < count($coupons); $j++){
                            if($coupons[$j]->product_id == null || $product['product_id'] == $coupons[$j]->product_id){
                                if($coupons[$j]->frequency == null || $coupons[$j]->usage_counter < $coupons[$j]->frequency){
                                    if($coupons[$j]->amount < 1){ //only percentage is applicable per
                                        $discount = $coupons[$j]->amount;
                                        $discount_id = $coupons[$j]->id;
                                    }
                                    break;
                                }
                            }
                        }
                    }
                    $pushIn = array('discount_id' => $discount_id, 'discount' => $discount, 'coupon_code' => $discount_code, 'quantity' => $product['quantity'], 'unit_cost_price' => $response->product->unit_cost_price, 
                        'unit_selling_price' => $response->product->unit_selling_price, 'product_id' => $product['product_id'], 'sale_order_id' => $order->id,
                        'seller_id' => $user->id, 'tax' => $response->product->tax, 'created_at' => Carbon::now(), 'updated_at' => Carbon::now());
                    array_push($cart, $pushIn);
                    $subTotal = $product['quantity'] * $response->product->unit_selling_price;
                    $subTotalCopy = $subTotal;
                    $subTotal = $subTotal - ($discount * $subTotal);
                    $totalCost += (($response->product->tax / 100)  * $subTotal) + $subTotal;
                    $totalCostCopy += (($response->product->tax / 100)  * $subTotalCopy) + $subTotalCopy;
                    //TODO: TAX
                }
                //finally cache the final order total

                if(count($coupons) == 1 && $coupons[0]->product_id == null && $coupons[0]->amount > 0){
                    //only absolute is applicable for total
                    if($coupons[0]->frequency == null || $coupons[0]->usage_counter < $coupons[0]->frequency){
                        $order->total_amount = $totalCost - $coupons[0]->amount;
                        $order->discount_id = $coupons[0]->id;
                    }else{
                        $order->total_amount = $totalCost;
                    }
                }else{
                    $order->total_amount = $totalCost;
                }
                $order->save();

                if($order->deposit >= $totalCost){
                    throw new \Exception("Error Processing Request", 1);
                };
                Sale::insert($cart);

                /* Update Accounts*/
                if($request['payment_type'] == 'CREDIT'){
                    if(abs($order->deposit - $totalCost) == $totalCost){
                        // No Fund deposited
                        $receivable = Account::where('account_no', AccountCode::$ASSETS_RECEIVABLE)->get()->first();
                        $receivable->prev_balance = $receivable->balance;
                        $receivable->balance += $totalCost;
                        $receivable->save();

                        $transaction = new Transaction;
                        $transaction->amount = $totalCost;
                        $transaction->account()->associate($receivable);
                        $transaction->naration = '!Credit Transaction #' . $order->receipt_number;
                        $transaction->transaction_reference = $order->receipt_number;
                        $transaction->transaction_type = 'dr';
                        $transaction->user()->associate(Auth::user());
                        $transaction->save();
                    }else{
                        $receivable = Account::where('account_no', AccountCode::$ASSETS_RECEIVABLE)->get()->first();
                        $receivable->prev_balance = $receivable->balance;
                        $receivable->balance += abs($totalCost - $order->deposit);
                        $receivable->save();

                        $transaction = new Transaction;
                        $transaction->amount = abs($totalCost - $order->deposit);
                        $transaction->account()->associate($receivable);
                        $transaction->naration = '!Credit Part Payment Transaction #' . $order->receipt_number;
                        $transaction->transaction_reference = $order->receipt_number;
                        $transaction->transaction_type = 'dr';
                        $transaction->user()->associate(Auth::user());
                        $transaction->save();

                        $cash = Account::where('account_no', AccountCode::$ASSETS_CASH)->get()->first();
                        $cash->prev_balance = $cash->balance;
                        $cash->balance += $order->deposit; //Cash
                        $cash->save();

                        $transaction = new Transaction;
                        $transaction->amount = $order->deposit;
                        $transaction->account()->associate($cash);
                        $transaction->naration = '!Credit Part Payment Transaction #' . $order->receipt_number;
                        $transaction->transaction_reference = $order->receipt_number;
                        $transaction->transaction_type = 'dr';
                        $transaction->user()->associate(Auth::user());
                        $transaction->save();
                    }
                    $customer = Customer::find($order->customer_id);
                    $customer->prev_balance = $customer->balance;
                    $customer->balance -= abs($totalCost - $order->deposit);
                    $customer->save();
                }else if($request['payment_type'] == 'CASH'){
                    if($totalCost == $totalCostCopy){
                        $cash = Account::where('account_no', AccountCode::$ASSETS_CASH)->get()->first();
                        $cash->prev_balance = $cash->balance;
                        $cash->balance += $totalCost;
                        $cash->save();

                        $transaction = new Transaction;
                        $transaction->amount = $totalCost;
                        $transaction->account()->associate($cash);
                        $transaction->naration = 'Cash Transaction #' . $order->receipt_number;
                        $transaction->transaction_reference = $order->receipt_number;
                        $transaction->transaction_type = 'dr';
                        $transaction->user()->associate(Auth::user());
                        $transaction->save();
                    }else{
                        $cash = Account::where('account_no', AccountCode::$ASSETS_CASH)->get()->first();
                        $cash->prev_balance = $cash->balance;
                        $cash->balance += $totalCost;
                        $cash->save();

                        $transaction = new Transaction;
                        $transaction->amount = $totalCost;
                        $transaction->account()->associate($cash);
                        $transaction->naration = 'Cash Transaction with Discount '. $discount_code .'#' . $order->receipt_number;
                        $transaction->transaction_reference = $order->receipt_number;
                        $transaction->transaction_type = 'dr';
                        $transaction->user()->associate(Auth::user());
                        $transaction->save();

                        $discount = Account::where('account_no', AccountCode::$SALES_DISCOUNT)->get()->first();
                        $discount->prev_balance = $discount->balance;
                        $discount->balance += abs($totalCostCopy - $totalCost);
                        $discount->save();

                        $transaction = new Transaction;
                        $transaction->amount = abs($totalCostCopy - $totalCost);
                        $transaction->account()->associate($discount);
                        $transaction->naration = 'Cash Transaction with Discount ' . $discount_code . ' #' . $order->receipt_number;
                        $transaction->transaction_reference = $order->receipt_number;
                        $transaction->transaction_type = 'dr';
                        $transaction->user()->associate(Auth::user());
                        $transaction->save();

                        //discount effect
                        $receivable = Account::where('account_no', AccountCode::$ASSETS_RECEIVABLE)->get()->first();
                        $receivable->prev_balance = $receivable->balance;
                        $receivable->balance += $totalCostCopy;
                        $receivable->save();

                        $transaction = new Transaction;
                        $transaction->amount = $totalCostCopy;
                        $transaction->account()->associate($receivable);
                        $transaction->naration = 'Cash Transaction with Discount ' . $discount_code . ' #' . $order->receipt_number;
                        $transaction->transaction_reference = $order->receipt_number;
                        $transaction->transaction_type = 'cr';
                        $transaction->user()->associate(Auth::user());
                        $transaction->save();
                    }
                }else if($request['payment_type'] == 'TRANSFER'){
                    if($totalCost == $totalCostCopy){
                        $transfer = Account::where('account_no', AccountCode::$ASSETS_BANK . '02')->get()->first();
                        $transfer->prev_balance = $transfer->balance;
                        $transfer->balance += $totalCost;
                        $transfer->save();

                        $transaction = new Transaction;
                        $transaction->amount = $totalCost;
                        $transaction->account()->associate($transfer);
                        $transaction->naration = 'Transfer/USSD #' . $order->receipt_number;
                        $transaction->transaction_reference = $order->receipt_number;
                        $transaction->transaction_type = 'dr';
                        $transaction->user()->associate(Auth::user());
                        $transaction->save();
                    }else{
                        $transfer = Account::where('account_no', AccountCode::$ASSETS_BANK . '02')->get()->first();
                        $transfer->prev_balance = $transfer->balance;
                        $transfer->balance += $totalCost;
                        $transfer->save();

                        $transaction = new Transaction;
                        $transaction->amount = $totalCost;
                        $transaction->account()->associate($transfer);
                        $transaction->naration = 'Transfer/USSD with Discount '. $discount_code .'#' . $order->receipt_number;
                        $transaction->transaction_reference = $order->receipt_number;
                        $transaction->transaction_type = 'dr';
                        $transaction->user()->associate(Auth::user());
                        $transaction->save();

                        $discount = Account::where('account_no', AccountCode::$SALES_DISCOUNT)->get()->first();
                        $discount->prev_balance = $discount->balance;
                        $discount->balance += abs($totalCostCopy - $totalCost);
                        $discount->save();

                        $transaction = new Transaction;
                        $transaction->amount = abs($totalCostCopy - $totalCost);
                        $transaction->account()->associate($discount);
                        $transaction->naration = 'Transfer/USSD with Discount ' . $discount_code . ' #' . $order->receipt_number;
                        $transaction->transaction_reference = $order->receipt_number;
                        $transaction->transaction_type = 'dr';
                        $transaction->user()->associate(Auth::user());
                        $transaction->save();

                        $receivable = Account::where('account_no', AccountCode::$ASSETS_RECEIVABLE)->get()->first();
                        $receivable->prev_balance = $receivable->balance;
                        $receivable->balance += $totalCostCopy;
                        $receivable->save();

                        $transaction = new Transaction;
                        $transaction->amount = $totalCostCopy;
                        $transaction->account()->associate($receivable);
                        $transaction->naration = 'Transfer/USSD with Discount ' . $discount_code . ' #' . $order->receipt_number;
                        $transaction->transaction_reference = $order->receipt_number;
                        $transaction->transaction_type = 'cr';
                        $transaction->user()->associate(Auth::user());
                        $transaction->save();
                    }
                }

                $sales = Account::where('account_no', AccountCode::$REVENUE_SALES)->get()->first();
                $sales->prev_balance = $sales->balance;
                $sales->balance += $totalCost;
                $sales->save();

                $transaction = new Transaction;
                $transaction->amount = $totalCost;
                $transaction->account()->associate($sales);
                $transaction->naration = 'Sales Transaction #' . $order->receipt_number;
                $transaction->transaction_reference = $order->receipt_number;
                $transaction->transaction_type = 'cr';
                $transaction->user()->associate(Auth::user());
                $transaction->save();
            });
        } catch(\Exception $e) {
            die($e);
            return Response::json(['status' => false, 'error_code' => 1002]);
        }
        $response = array('status' => true, 'receipt_number' => $order->receipt_number, 'order_id' => $order->id);
        return Response::json($response);
    }

    private function validate_cart($request){
        if ($request->has('customer') && $request->has('products') &&
            count($request->input('products')) > 0) return true;
        return false;
    }

    private function genReceiptNumber(){
        $result = 'CRPT';
        for ($i=0; $i < 10; $i++) $result .= mt_rand(0, 9);
        return trim($result);
    }

    private function genInvoiceNumber(){
        $result = 'CINV';
        for ($i=0; $i < 10; $i++) $result .= mt_rand(0, 9);
        return trim($result);
    }

    public function getReceipt(Request $request, $receiptId){
        $printable = false;
        if($request->has('q')){
            $printable = true;
        }
        $data = SaleOrder::with(['sales','customer', 'sales.product'])
        ->where('receipt_number', $receiptId)->get()->first();
        return view('sale.receipt', ['data' => $data, 'isPrintable' => $printable]);
    }
}