<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use Auth;
use Redirect;
use App\User;
use Session;

class UserController extends Controller
{    
    public function getAddUser(){
        $user = Auth::user();
        if($user->access_role  != "admin"){
            Session::flash('flash_error', 'You do not have the permission to access the page.');
            return back();
        }
        return view('user.create');
    }

    public function getUsers(){
        $user = Auth::user();
        if($user->access_role  != "admin"){
            Session::flash('flash_error', 'You do not have the permission to access the page.');
            return back();
        }
        return view('user.index', ['users' => User::get()]);
    }

    public function getEditUser($id){
        $user = Auth::user();
        if($user->access_role  != "admin"){
            Session::flash('flash_error', 'You do not have the permission to access the page.');
            return back();
        }
        $user = User::find($id);
        if(!$user){
            return back(); //TODO: send reason too
        }
        return view('user.edit', ['data' => $user]);
    }

    public function updateUser(Request $request, $id){

        $this->validate($request, [
            'name' => 'required|max:255',
            'email' => 'required|email|max:255',
            'password' => 'sometimes|min:6',
            'access_role' => 'required|min:3',
        ]);

        $mUser = User::find($id);
        if ($mUser->access_role == 'virtual') {
            Session::flash('flash_error', "Ooopse!! You can edit a virtual user");
            return back();
        }
        $mUser->name = $request->input('name');
        $mUser->email = $request->input('email');
        // $mUser->password = bcrypt($request->input('password'));
        $mUser->access_role = $request->input('access_role');
        if ($request->has('active')) {
            $mUser->active = 1;
        }else{
            $mUser->active = 0;
        }
        $mUser->save();
        Session::flash('flash_message', 'Successfuly Saved');
        return redirect()->route('user_list');
    }
}
