<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use Auth;
use Redirect;
use App\Warehouse;
use App\User;
use App\Account;
use App\WarehouseItem;
use Session;
use App\Supplier;
use App\Product;
use Response;
use DB;
use Cache;

class WarehouseController extends Controller
{
    private $AppConfig;

    public function __construct()
    {
        $this->AppConfig = Cache::get('AppConfig');
    }

    public function getWarehousesHavingProduct(Request $request){
        if($this->AppConfig->supports_warehouse){
            $productId = (int) $request->input('product_id');
            $warehouseItems = WarehouseItem::with(['product','warehouse'])
            ->select(DB::raw("product_id, warehouse_id, COALESCE((SUM(quantity)), 0) - COALESCE(S.SQty, 0) AS Qty FROM warehouse_items LEFT JOIN (SELECT product_id AS a, warehouse_id AS b, SUM(quantity) as SQty FROM warehouse_moved_items WHERE product_id = $productId GROUP BY a, b) AS S ON (product_id = a AND warehouse_id = b) WHERE product_id = $productId GROUP BY product_id, warehouse_id --"))
            ->get();
            $response = array('warehouse_support' => $this->AppConfig->supports_warehouse, 
                'warehouses' => Warehouse::get(), 'warehouse_items' => $warehouseItems);
        }else{
            $response = array('warehouse_support' => $this->AppConfig->supports_warehouse);
        }
        return Response::json($response);
    }

    public function getWarehouses(){
    	$user = Auth::user();
        if($user->access_role != "admin"){
            Session::flash('flash_error', 'You do not have the permission to access the page.');
            return back();
        }
        return view('warehouse.index', ['data' => Warehouse::with('user')->get()]);
    }

    public function getAddWarehouse(){
    	$user = Auth::user();
        if($user->access_role != "admin"){
            Session::flash('flash_error', 'You do not have the permission to access the page.');
            return back();
        }
        return view('warehouse.create');
    }

    public function getEditWarehouse($id){
        $user = Auth::user();
        if($user->access_role != "admin"){
            Session::flash('flash_error', 'You do not have the permission to access the page.');
            return back();
        }
        return view('warehouse.edit', ['data' => Warehouse::find($id)]);
    }

    public function updateWarehouse(Request $request, $id){

        if(Auth::user()->access_role != "admin"){
            Session::flash('flash_error', 'You do not have the permission to access the page.');
            return back();
        }
        
        $this->validate($request, [
            'title' => 'required',
            'address' => 'required',
        ]);

        try {
            DB::transaction(function() use($request, $id)
            {

               $nWarehouse = Warehouse::find($id);
                $nWarehouse->title = $request->input('title');
                $nWarehouse->address = $request->input('address');
                $nWarehouse->save();

               });
        }catch(\Exception $e){
            //die(var_dump($e));

            Session::flash('flash_message', 'Sorry, an Error Occurred');
            return back();
        }
        Session::flash('flash_message', 'Successfuly Updated');
        return back();
    }

    public function addWarehouse(Request $request){
        if(Auth::user()->access_role != "admin"){
            Session::flash('flash_error', 'You do not have the permission to access the page.');
            return back();
        }
        
        $this->validate($request, [
        	'title' => 'required|unique:warehouses',
            'address' => 'required',
        ]);
        
        //the validate method ensures we don't get here on failure
        
        try {
            DB::transaction(function() use($request)
            {

                $nWarehouse = new Warehouse;
                $nWarehouse->title = $request->input('title');
                $nWarehouse->address = $request->input('address');
                $nWarehouse->user()->associate(Auth::user());
                $nWarehouse->save();

            });
        }catch(\Exception $e) {
            Session::flash('flash_message', 'Sorry, an Error Occurred');
            return back();
        }
        Session::flash('flash_message', 'Successfuly Saved');
        return back();
    }


    public function getWarehouseItems($id){
        $user = Auth::user();
        if($user->access_role != "admin"){
            Session::flash('flash_error', 'You do not have the permission to access the page.');
            return back();
        }
        // $items = WarehouseItem::with(['user', 'product', 'supplier', 'warehouse'])
        // ->where('warehouse_id', $id)->with('user')->get();
        $items = WarehouseItem::with(['user', 'product', 'supplier', 'warehouse'])
            ->select(DB::raw("created_at, user_id, updated_at, supplier_id, product_id, warehouse_id, COALESCE((SUM(quantity)), 0) - COALESCE(S.SQty, 0) AS quantity FROM warehouse_items LEFT JOIN (SELECT product_id AS a, warehouse_id AS b, SUM(quantity) as SQty FROM warehouse_moved_items WHERE warehouse_id = $id GROUP BY a, b) AS S ON (product_id = a AND warehouse_id = b) WHERE warehouse_id = $id GROUP BY product_id, warehouse_id --"))
            ->get();
        $warehouse = Warehouse::find($id);

        return view('warehouse.items', ['data' => $items, 'warehouse' => $warehouse]);
    }

    public function addWarehouseItem(Request $request){

        if(Auth::user()->access_role != "admin"){
            Session::flash('flash_error', 'You do not have the permission to access the page.');
            return back();
        }

        $messages = [
            'product_id.required' => 'Please select the product first',
            'product_id.integer' => 'Please select the product first',
            'warehouse_id.required' => 'Invalid warehouse selection',
            'warehouse_id.integer' => 'Invalid warehouse selection',
            'supplier_id.required' => 'The supplier field is required',
            'supplier_id.integer' => 'The supplier field is required',
        ];
        
        $this->validate($request, [            
            'quantity' => 'required|integer|min:1',
            'product_id' => 'required|integer',
            'supplier_id' => 'required|integer',
            'warehouse_id' => 'required|integer',
        ], $messages);

        try{
            DB::transaction(function() use($request)
            {

                $warehouseItem = new WarehouseItem;
                $warehouseItem->quantity = $request->input('quantity');
                $product = Product::find($request->input('product_id'));
                $warehouseItem->product()->associate($product);
                $warehouseItem->unit_cost_price = $product->unit_cost_price;
                $warehouseItem->unit_selling_price = $product->unit_selling_price;
                $warehouseItem->tax = $product->tax;
                $warehouseItem->supplier()->associate(Supplier::find($request->input('supplier_id')));
                $warehouseItem->warehouse()->associate(Warehouse::find($request->input('warehouse_id')));
                $warehouseItem->user()->associate(Auth::user());
                $warehouseItem->save();
            });
        }catch(\Exception $e) {
            Session::flash('flash_message', 'Sorry, an Error Occured');
            return back();
        }
        Session::flash('flash_message', 'Successfuly Saved');
        return back();
    }
}