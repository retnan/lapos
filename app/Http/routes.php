<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', "Auth\AuthController@getLogin");

Route::get('/land', function () {
    return view('landing');
});

Route::post('/subscribe', 'NewsLetterController@subscribe');

/* Authentication */
Route::get('/login', "Auth\AuthController@getLogin");
Route::get('/logout', "Auth\AuthController@logout");
Route::post('/login', "Auth\AuthController@postLogin");

Route::get('/product/search/', 'SearchController@search');
Route::get('/supplier/search', 'SearchController@searchSupplier');
Route::get('/customer/search', 'SearchController@searchCustomer');
Route::get('/stocks/search', 'SearchController@searchStocks');
Route::get('/sale_order/search', 'SearchController@searchSaleOrder');
Route::get('/search', 'SearchController@universalSearch');

Route::get('/dashboard/pos/{receiptId}', [ 'as' => 'sales_receipt', 'uses' => 'SaleController@getReceipt']);
Route::get('/dashboard/purchase', [ 'uses' => 'PurchaseController@getReceivings']);
Route::get('/dashboard/purchase/{receiptId}', ['uses' => 'PurchaseController@getReceivingReceipt']);
Route::post('/dashboard/pos', [ 'as' => 'sales_process', 'uses' => 'SaleController@processOrder']);
Route::put('/dashboard/pos', [
        'as' => 'put_markorderforpayment', 'uses' => 'SaleController@markOrderForPayment'
]);
Route::get('/dashboard/report/transactions', [ 'as' => 'transaction_report', 'uses' => 'ReportController@transactionReport']);
Route::get('/dashboard/report/sales', ['as' => 'sales_report', 'uses' => 'ReportController@salesReport']);
Route::get('/dashboard/report/sales_transaction', ['as' => 'sales_transaction', 'uses' => 'ReportController@salesTransaction']);
Route::get('/dashboard/report/stocks', ['as' => 'stocks_report', 'uses' => 'ReportController@stocksReport']);
Route::get('/dashboard/report/account', ['as' => 'account_report', 'uses' => 'ReportController@accountReport']);
Route::get('/warehouse/product', 'WarehouseController@getWarehousesHavingProduct');


Route::get('/dashboard/pos/', [
    'as' => 'pos', 'uses' => 'SaleController@getPOS'
]);

Route::get('/dashboard/discount', [
    'as' => 'query_discount', 'uses' => 'DiscountController@queryDiscount'
]);

Route::group(['prefix' => '/dashboard/customer/', 'middleware' => ['auth', 'hashid']], function () {
    Route::get('/', [
        'as' => 'customer_list', 'uses' => 'CustomerController@getCustomers'
    ]);
    Route::get('/create', [
        'as' => 'customer_create', 'uses' => 'CustomerController@getAddCustomer'
    ]);
    Route::get('/{id}', [
        'as' => 'customer_show', 'uses' => 'CustomerController@show'
    ]);
    Route::get('/{id}/edit', [
        'as' => 'customer_edit', 
        'uses' => 'CustomerController@getEditCustomer'
    ]);
    Route::put('/{id}', [
        'as' => 'customer_put', 'uses' => 'CustomerController@updateCustomer'
    ]);
    Route::post('/', ['uses' => "CustomerController@addCustomer"]);
});

Route::group(['prefix' => '/dashboard/cashier', 'middleware' => ['auth', 'hashid']], function () {
    Route::get('/', [
    'as' => 'cashier_dashboard', 'uses' => 'DashboardController@getCachierPage']);
});

Route::get('/dashboard/createExpenses', [
    'as' => 'expenses_index', 'uses' => 'TransferController@createExpenses']);

Route::post('/dashboard/logExpenses', [
    'as' => 'expenses_create', 'uses' => 'TransferController@logExpenses']);

Route::group(['prefix' => '/dashboard/admin', 'middleware' => ['auth', 'hashid']], function () {
    Route::get('/', [
        'as' => 'admin_dashboard', 'uses' => 'DashboardController@getAdminPage'
    ]);

    Route::group(['prefix' => '/return'], function(){
        Route::get('/sales', ['as' => 'get_sale_return', 'uses' => 'SaleReturnController@form']);
        Route::get('/sales/{receiptId}', ['as' => 'sale_return_receipt', 'uses' => 'SaleReturnController@getReceipt']);
        Route::post('/sales', ['uses' => 'SaleReturnController@returnItems']);
        Route::get('/purchase', ['as' => 'get_purchase_return', 'uses' => 'PurchaseReturnController@form']);
    });
    Route::get('/transfer', [
        'as' => 'transfer_index', 'uses' => 'TransferController@index']);
    Route::post('/transfer', ['uses' => 'TransferController@transfer']);

    Route::get('/config', [
        'as' => 'config_list', 'uses' => 'DashboardController@getConfig'
    ]);
    Route::put('/config/1', [
        'as' => 'config_put', 'uses' => 'DashboardController@putConfig'
    ]);
    /* Category*/
    Route::get('/category', [
        'as' => 'category_list', 'uses' => 'CategoryController@getCategories'
    ]);
    Route::get('/category/create', [
        'as' => 'category_create', 'uses' => 'CategoryController@getAddCategory'
    ]);

    // delete category
    Route::get('/category/{id}/delete', [
        'as' => 'category_delete', 'uses' => 'CategoryController@delete'
    ]);
    
    
    Route::get('/category/{id}/edit', [
        'as' => 'category_edit', 'uses' => 'CategoryController@getEditCategory'
    ]);
    Route::put('/category/{id}', [
        'as' => 'category_put', 'uses' => 'CategoryController@updateCategory'
    ]);
    Route::post('/category', "CategoryController@addCategory");

    /* Product */
    Route::get('/product', [
        'as' => 'product_list', 'uses' => 'ProductController@getProducts'
    ]);
    Route::get('/receivings', [
        'as' => 'get_receivings', 'uses' => 'PurchaseController@getReceivings'
    ]);
    Route::post('/receivings', [
        'as' => 'post_receivings', 'uses' => 'PurchaseController@processReceivings'
    ]);
    Route::put('/receivings', [
        'as' => 'put_receivings', 'uses' => 'PurchaseController@markReceivingsForPayment'
    ]);
    Route::get('/receivings/{receiptId}', [ 'as' => 'receiving_receipt', 
        'uses' => 'PurchaseController@getReceivingReceipt']);
    Route::get('/product/create', [
        'as' => 'product_create', 'uses' => 'ProductController@getAddProduct'
    ]);
    Route::get('/product/{id}/edit', [
        'as' => 'product_edit', 'uses' => 'ProductController@getEditProduct'
    ]);
    Route::put('/product/{id}', [
        'as' => 'product_put', 'uses' => 'ProductController@updateProduct'
    ]);
    Route::get('/product/{id}', [
        'as' => 'product_show', 'uses' => 'ProductController@showProduct'
    ]);

    Route::post('/product', ['as' => 'product_post','uses'=> "ProductController@addProduct"]);
    
    // delete product
    Route::get('/product/{id}/delete', [
        'as' => 'product_delete', 'uses' => 'ProductController@delete'
    ]);

    /* Stocks */
    Route::get('/stocks', [
        'as' => 'stock_list', 'uses' => 'StockController@getStocks'
    ]);

    Route::get('/stocks_value', ['as' => 'stock_value', 'uses' => 'ReportController@stockValue']);

    Route::get('/stocks/create', [
        'as' => 'stock_create', 'uses' => 'StockController@getAddStock'
    ]);
    Route::post('/stocks', "StockController@addStock");

    /* Discount */
    Route::get('/discount', [
        'as' => 'discount_list', 'uses' => 'DiscountController@getDiscounts'
    ]);
    Route::get('/discount/create', [
        'as' => 'discount_create', 'uses' => 'DiscountController@getAddDiscount'
    ]);
    Route::post('/discount', "DiscountController@addDiscount");


    /* User & Authentication */
    Route::get('/user', [
        'as' => 'user_list', 'uses' => 'UserController@getUsers'
    ]);
    Route::get('/user/create', [
        'as' => 'user_create', 'uses' => 'UserController@getAddUser'
    ]);
    Route::get('/user/{id}/edit', [
        'as' => 'user_edit', 'uses' => 'UserController@getEditUser'
    ]);
    Route::put('/user/{id}', "UserController@updateUser");
    Route::post('/user/create', "Auth\AuthController@postSignUp");

    /* Suppliers*/
    Route::get('/supplier', [
        'as' => 'supplier_list', 'uses' => 'SupplierController@getSuppliers'
    ]);
    Route::get('/supplier/create', [
        'as' => 'supplier_create', 'uses' => 'SupplierController@getAddSupplier'
    ]);
    Route::get('/supplier/{id}', [
        'as' => 'supplier_show', 'uses' => 'SupplierController@show'
    ]);
    Route::get('/supplier/{id}/edit', [
        'as' => 'supplier_edit', 'uses' => 'SupplierController@getEditSupplier'
    ]);
    Route::put('/supplier/{id}', [
        'as' => 'supplier_put', 'uses' => 'SupplierController@updateSupplier'
    ]);
    Route::post('/supplier', "SupplierController@addSupplier");

    /* Banks*/
    Route::get('/bank', [
        'as' => 'bank_list', 'uses' => 'BankController@getBanks'
    ]);
    Route::get('/bank/create', [
        'as' => 'bank_create', 'uses' => 'BankController@getAddBank'
    ]);
    Route::get('/bank/{id}/edit', [
        'as' => 'bank_edit', 'uses' => 'BankController@getEditBank'
    ]);
    Route::put('/bank/{id}', [
        'as' => 'bank_put', 'uses' => 'BankController@updateBank'
    ]);
    Route::post('/bank', "BankController@addBank");

    /* Warehouse*/
    Route::get('/warehouse', [
        'as' => 'warehouse_list', 'uses' => 'WarehouseController@getWarehouses'
    ]);
    Route::get('/warehouse/create', [
        'as' => 'warehouse_create', 'uses' => 'WarehouseController@getAddWarehouse'
    ]);
    Route::get('/warehouse/{id}', [
        'as' => 'warehouse_show', 'uses' => 'WarehouseController@getWarehouseItems'
    ]);
    Route::get('/warehouse/{id}/edit', [
        'as' => 'warehouse_edit', 'uses' => 'WarehouseController@getEditWarehouse'
    ]);
    Route::put('/warehouse/{id}', [
        'as' => 'warehouse_put', 'uses' => 'WarehouseController@updateWarehouse'
    ]);
    Route::post('/warehouse', "WarehouseController@addWarehouse");
    Route::post('/warehouse/add_product', "WarehouseController@addWarehouseItem");
});