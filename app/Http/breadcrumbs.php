<?php
// Home
Breadcrumbs::register('admin_dashboard', function($breadcrumbs)
{
    $breadcrumbs->push('Dashboard', route('admin_dashboard'));
});

// Dashboard > Configuration
Breadcrumbs::register('config_list', function($breadcrumbs)
{
    $breadcrumbs->parent('admin_dashboard');
    $breadcrumbs->push('Config', route('config_list'));
});

// Dashboard > Product
Breadcrumbs::register('product_list', function($breadcrumbs)
{
    $breadcrumbs->parent('admin_dashboard');
    $breadcrumbs->push('Product', route('product_list'));
});

// Dashboard > Product > EDIT
Breadcrumbs::register('product_edit', function($breadcrumbs, $product)
{
    $breadcrumbs->parent('product_list');
    $breadcrumbs->push('Edit', route('product_edit', $product->id));
});

// Dashboard > Product > SHOW
Breadcrumbs::register('product_show', function($breadcrumbs, $product)
{
    $breadcrumbs->parent('product_list');
    $breadcrumbs->push($product->title, route('product_show', $product->id));
});

// Dashboard > Stock
Breadcrumbs::register('stock_list', function($breadcrumbs)
{
    $breadcrumbs->parent('admin_dashboard');
    $breadcrumbs->push('Stock', route('stock_list'));
});

// Dashboard > Stock > Create
Breadcrumbs::register('stock_create', function($breadcrumbs)
{
    $breadcrumbs->parent('stock_list');
    $breadcrumbs->push('Add Stocks', route('stock_create'));
});

// Dashboard > Product > Create
Breadcrumbs::register('product_create', function($breadcrumbs)
{
    $breadcrumbs->parent('product_list');
    $breadcrumbs->push('Create', route('product_create'));
});

// Dashboard > Product > Category
Breadcrumbs::register('category_list', function($breadcrumbs)
{
    $breadcrumbs->parent('product_list');
    $breadcrumbs->push('Category', route('category_list'));
});

// Dashboard > Product > Category > Create
Breadcrumbs::register('category_create', function($breadcrumbs)
{
    $breadcrumbs->parent('category_list');
    $breadcrumbs->push('Create', route('category_create'));
});

// Dashboard > Product > Category > EDIT
Breadcrumbs::register('category_edit', function($breadcrumbs, $category)
{
    $breadcrumbs->parent('category_list');
    $breadcrumbs->push('Edit', route('category_edit', $category->id));
});

// Dashboard > Product > Supplier
Breadcrumbs::register('supplier_list', function($breadcrumbs)
{
    $breadcrumbs->parent('product_list');
    $breadcrumbs->push('Supplier', route('supplier_list'));
});

Breadcrumbs::register('supplier_show', function($breadcrumbs, $supplier)
{
    $breadcrumbs->parent('product_list');
    $breadcrumbs->push($supplier->title, route('supplier_show', $supplier->id));
});

// Dashboard > Product > Supplier > Create
Breadcrumbs::register('supplier_create', function($breadcrumbs)
{
    $breadcrumbs->parent('supplier_list');
    $breadcrumbs->push('Create', route('supplier_create'));
});

// Dashboard > Product > Supplier > EDIT
Breadcrumbs::register('supplier_edit', function($breadcrumbs, $supplier)
{
    $breadcrumbs->parent('supplier_list');
    $breadcrumbs->push('Edit', route('supplier_edit', $supplier->id));
});

// Dashboard > User
Breadcrumbs::register('user_list', function($breadcrumbs)
{
    $breadcrumbs->parent('admin_dashboard');
    $breadcrumbs->push('User', route('user_list'));
});

// Dashboard > User > Create
Breadcrumbs::register('user_create', function($breadcrumbs)
{
    $breadcrumbs->parent('user_list');
    $breadcrumbs->push('Create', route('user_list'));
});

// Dashboard > User > EDIT
Breadcrumbs::register('user_edit', function($breadcrumbs, $user)
{
    $breadcrumbs->parent('user_list');
    $breadcrumbs->push('Edit', route('user_edit', $user->id));
});

// Dashboard > Product > Discount
Breadcrumbs::register('discount_list', function($breadcrumbs)
{
    $breadcrumbs->parent('product_list');
    $breadcrumbs->push('Discount', route('discount_list'));
});

// Dashboard > [Control Panel] > Bank
Breadcrumbs::register('bank_list', function($breadcrumbs)
{
    $breadcrumbs->parent('admin_dashboard');
    $breadcrumbs->push('Bank', route('bank_list'));
});

// Dashboard > [Control Panel] -> Bank > Create
Breadcrumbs::register('bank_create', function($breadcrumbs)
{
    $breadcrumbs->parent('bank_list');
    $breadcrumbs->push('Create', route('bank_create'));
});

// Dashboard > [Control Panel] -> Bank > Edit
Breadcrumbs::register('bank_edit', function($breadcrumbs, $data)
{
    $breadcrumbs->parent('bank_list');
    $breadcrumbs->push('Edit', route('bank_edit', $data->getHashId()));
});


// Dashboard > [Control Panel] > Warehouse
Breadcrumbs::register('warehouse_list', function($breadcrumbs)
{
    $breadcrumbs->parent('admin_dashboard');
    $breadcrumbs->push('Warehouse', route('warehouse_list'));
});

// Dashboard > [Control Panel] -> Warehouse > Create
Breadcrumbs::register('warehouse_create', function($breadcrumbs)
{
    $breadcrumbs->parent('warehouse_list');
    $breadcrumbs->push('Create', route('warehouse_create'));
});

// Dashboard > [Control Panel] -> Warehouse > EDIT
Breadcrumbs::register('warehouse_edit', function($breadcrumbs, $data)
{
    $breadcrumbs->parent('warehouse_list');
    $breadcrumbs->push('Edit', route('warehouse_edit', $data->id));
});

// Dashboard > [Control Panel] -> Warehouse > [The Warehouse] >
Breadcrumbs::register('warehouse_show', function($breadcrumbs, $warehouse)
{
    $breadcrumbs->parent('warehouse_list');
    $breadcrumbs->push($warehouse->title, route('warehouse_show', $warehouse->id));
});


// Dashboard
Breadcrumbs::register('cashier_dashboard', function($breadcrumbs)
{
    $breadcrumbs->push('Dashboard', route('cashier_dashboard'));
});

// Dashboard > Customer
Breadcrumbs::register('customer_list', function($breadcrumbs, $access_role)
{
    if($access_role == "admin"){
        $breadcrumbs->parent('admin_dashboard');
    }else{
        $breadcrumbs->parent('cashier_dashboard');
    }
    $breadcrumbs->push('Customer', route('customer_list'));
});

Breadcrumbs::register('customer_create', function($breadcrumbs, $access_role)
{
    $breadcrumbs->parent('customer_list', $access_role);
    $breadcrumbs->push('Customer', route('customer_list'));
});

Breadcrumbs::register('customer_edit', function($breadcrumbs, $data, $access_role)
{
    $breadcrumbs->parent('customer_list', $access_role);
    $breadcrumbs->push('Edit', route('customer_list', $data->id));
});

Breadcrumbs::register('customer_show', function($breadcrumbs, $data, $access_role)
{
    if($access_role == "admin"){
        $breadcrumbs->parent('admin_dashboard');
    }else{
        $breadcrumbs->parent('cashier_dashboard');
    }
    $breadcrumbs->push($data->title, route('customer_show', $data->id));
});

// Dashboard > Point of Sales
Breadcrumbs::register('pos', function($breadcrumbs, $access_role)
{
    if($access_role == "admin"){
        $breadcrumbs->parent('admin_dashboard');
    }else{
        $breadcrumbs->parent('cashier_dashboard');
    }
    $breadcrumbs->push('Point of Sale', route('pos'));
});

Breadcrumbs::register('sales_report', function($breadcrumbs)
{
    // if($access_role == "admin"){
        $breadcrumbs->parent('admin_dashboard');
    // }else{
        // $breadcrumbs->parent('cashier_dashboard');
    // }
    $breadcrumbs->push('Sales Report', route('sales_report'));
});

Breadcrumbs::register('sales_receipt', function($breadcrumbs, $data, $access_role)
{
    $breadcrumbs->parent('pos', $access_role);
    $breadcrumbs->push($data->payment_type == 'CASH' ? 'Receipt' : 'Invoice', route('sales_receipt', $data->id));
});

// Dashboard > Receivings
Breadcrumbs::register('get_receivings', function($breadcrumbs)
{
    $breadcrumbs->parent('admin_dashboard');
    $breadcrumbs->push('Receivings', route('get_receivings'));
});

Breadcrumbs::register('receiving_receipt', function($breadcrumbs, $data)
{
    $breadcrumbs->parent('get_receivings');
    $breadcrumbs->push($data->payment_type == 'CASH' ? 'Receipt' : 'Invoice', route('receiving_receipt', $data->id));
});

// Dashboard > Sales Return
Breadcrumbs::register('get_sale_return', function($breadcrumbs)
{
    $breadcrumbs->parent('admin_dashboard');
    $breadcrumbs->push('Sale Return', route('get_sale_return'));
});

// Dashboard > Purchase Return
Breadcrumbs::register('get_purchase_return', function($breadcrumbs)
{
    $breadcrumbs->parent('admin_dashboard');
    $breadcrumbs->push('Purchase Return', route('get_purchase_return'));
});

// Dashboard > Transfer
Breadcrumbs::register('transfer_index', function($breadcrumbs)
{
    $breadcrumbs->parent('admin_dashboard');
    $breadcrumbs->push('Transfer', route('transfer_index'));
});


Breadcrumbs::register('sale_return_receipt', function($breadcrumbs, $data)
{
    $breadcrumbs->parent('get_sale_return');
    $breadcrumbs->push('Refund', route('sale_return_receipt', $data->id));
});

// Dashboard > Transaction
Breadcrumbs::register('transaction_report', function($breadcrumbs)
{
    $breadcrumbs->parent('admin_dashboard');
    $breadcrumbs->push('Transactions', route('transaction_report'));
});

// Dashboard > Transaction
Breadcrumbs::register('account_report', function($breadcrumbs)
{
    $breadcrumbs->parent('admin_dashboard');
    $breadcrumbs->push('Accounts', route('account_report'));
});

Breadcrumbs::register('stocks_report', function($breadcrumbs)
{
    // if($access_role == "admin"){
        $breadcrumbs->parent('admin_dashboard');
    // }else{
        // $breadcrumbs->parent('cashier_dashboard');
    // }
    $breadcrumbs->push('Stocks Report', route('stocks_report'));
});