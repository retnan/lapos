<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SaleOrder extends Model
{
    public function sales()
    {
        return $this->hasMany('App\Sale');
    }

    public function customer()
    {
        return $this->belongsTo('App\Customer');
    }

    public function sale_return_orders(){
        return $this->hasMany('App\SaleReturnOrder');
    }

    public function user()
    {
        return $this->belongsTo('App\User');
    }
}
