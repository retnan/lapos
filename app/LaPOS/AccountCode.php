<?php

namespace LaPOS;

/**
* Account Codes
*/
class AccountCode
{
	/* Asset Account 1000 - 1999*/
	public static $ASSETS = '1';
	public static $ASSETS_CASH = '1000';
	public static $ASSETS_BANK = '11';
	public static $ASSETS_PURCHASE = '1001';
	public static $ASSETS_SALES_RETURN = '1002';
	public static $ASSETS_RECEIVABLE = '1003';
	/* Liability Accounts 2000 - 3999*/
	public static $LIABILITY = '2';
	public static $LIABILITY_PAYABLE = '2000';
	/* Revenue Accounts 4000 - 5999*/
	public static $REVENUE = '4';
	public static $REVENUE_SALES = '4000';
	/* Expenses 6000 - 7999*/
	public static $EXPENSES = '6';
	public static $EXPENSES_MISCELLANEOUS = '6000';
	public static $EXPENSES_SALARY = '6001';
	
	public static $SALES_DISCOUNT = '7000';
}