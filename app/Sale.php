<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Sale extends Model
{
    public function seller()
    {
        return $this->belongsTo('App\User');
    }

    public function product()
    {
    	return $this->belongsTo('App\Product');
    }

    public function sale_order(){
    	return $this->belongsTo('App\SaleOrder');
    }
}
