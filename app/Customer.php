<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Customer extends Model
{
    public function user()
    {
        return $this->belongsTo('App\User');
    }

    public function author()
    {
        return $this->belongsTo('App\User');
    }

    public function sale_orders()
    {
    	return $this->hasMany('App\SaleOrder');
    }
}
