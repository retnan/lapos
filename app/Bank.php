<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use LaPOS\HashIdInterface;
use Hashids;

class Bank extends Model implements HashIdInterface
{
    public function user()
    {
        return $this->belongsTo('App\User');
    }

    public function account()
    {
        return $this->belongsTo('App\Account');
    }

    public function getHashId(){
        return Hashids::encode($this->id);
    }
}
