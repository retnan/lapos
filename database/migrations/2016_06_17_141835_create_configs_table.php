<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateConfigsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('configs', function (Blueprint $table) {

            $table->engine = 'InnoDB';
            
            $table->increments('id');
            $table->string('company_name')->default("Point of Sale");
            $table->string('company_short_name')->default("Point of Sale");
            $table->string('address')->default("Jos, Nigeria.");
            $table->string('mobile_number')->default("+2348161730129");
            $table->string('currency_name')->default("Naira");
            $table->string('currency_symbol')->default("&#8358;");
            $table->string('supports_warehouse')->default(false);
            $table->string('supports_tax')->default(false);
            $table->string('email')->default("dretnan@logicaladdress.com");
            $table->string('app_version')->default('v0.1');
            $table->string('api_key')->nullable();
            $table->string('api_secret')->unique();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('configs');
    }
}
