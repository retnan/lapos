<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDiscountsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('discounts', function (Blueprint $table) {

            $table->engine = 'InnoDB';
            
            $table->increments('id');
            $table->decimal('amount', 15, 2)->default(0);         
            $table->enum('type', ['percentage', 'absolute']);
            $table->string('description')->nullable();
            $table->string('coupon');
            $table->string('expired_in');
            $table->integer('threshold_quantity')->default(1);
            $table->integer('product_id')->unsigned()->nullable();
            $table->integer('frequency')->unsigned()->nullable();
            $table->integer('usage_counter')->default(0);
            $table->integer('user_id')->unsigned();
            $table->timestamps();
            $table->foreign('user_id')->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('discounts');
    }
}
