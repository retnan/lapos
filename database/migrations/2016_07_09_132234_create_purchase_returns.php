<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePurchaseReturns extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('purchase_returns', function (Blueprint $table) {

            $table->engine = 'InnoDB';
            
            $table->increments('id');
            $table->integer('quantity');
            $table->decimal('unit_cost_price', 15, 4);
            $table->decimal('unit_selling_price', 15, 4);
            $table->decimal('tax', 5, 2)->default(0);
            $table->integer('product_id')->unsigned();
            $table->integer('purchase_return_order_id')->unsigned();
            $table->integer('seller_id')->unsigned();
            $table->timestamps();
            $table->foreign('product_id')->references('id')->on('products');
            $table->foreign('purchase_return_order_id')->references('id')->on('purchase_return_orders');
            $table->foreign('seller_id')->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('purchase_returns');
    }
}
