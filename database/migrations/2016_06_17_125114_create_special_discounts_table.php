<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSpecialDiscountsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('special_discounts', function (Blueprint $table) {

            $table->engine = 'InnoDB';
            
            $table->increments('id');
            $table->decimal('amount', 13, 4)->default(0);
            $table->enum('type', ['percentage', 'absolute']);
            $table->string('description')->nullable();
            $table->string('coupon')->unique();
            $table->enum('access', ['All customers', 'first N customers']);
            $table->integer('ncustomers')->nullable();
            $table->integer('use_counter')->nullable();// targetting first N customers
            $table->string('expired_in');
            $table->integer('threshold_quantity')->default(1);
            $table->integer('user_id')->unsigned();
            $table->timestamps();
            $table->foreign('user_id')->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('special_discounts');
    }
}
