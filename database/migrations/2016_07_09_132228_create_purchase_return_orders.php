<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePurchaseReturnOrders extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('purchase_return_orders', function (Blueprint $table) {

            $table->engine = 'InnoDB';
            
            $table->increments('id');
            $table->string('receipt_number')->unique();
            $table->decimal('total_amount', 15, 4)->default(0);
            $table->string('payment_type')->default('CASH'); //CASH, CREDIT, NONE (waste)
            $table->integer('customer_id')->unsigned();
            $table->integer('purchase_order_id')->unsigned();
            $table->integer('user_id')->unsigned();   
            $table->timestamps();
            $table->foreign('customer_id')->references('id')->on('customers');
            $table->foreign('purchase_order_id')->references('id')->on('purchase_orders'); 
            $table->foreign('user_id')->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('purchase_return_orders');
    }
}
