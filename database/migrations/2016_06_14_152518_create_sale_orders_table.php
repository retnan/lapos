<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSaleOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sale_orders', function (Blueprint $table) {

            $table->engine = 'InnoDB';
            
            $table->increments('id');
            $table->string('receipt_number')->unique();
            $table->string('previous_invoice_number')->nullable();
            $table->decimal('deposit', 15, 4)->default(0);
            $table->decimal('total_amount', 15, 4)->default(0);
            $table->string('payment_type')->default('CASH');
            $table->integer('customer_id')->unsigned();
            $table->integer('user_id')->unsigned();   
            $table->timestamps();
            $table->foreign('customer_id')->references('id')->on('customers'); 
            $table->foreign('user_id')->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('sale_orders');
    }
}
