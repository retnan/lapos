<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTransfersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('transfers', function (Blueprint $table) {

            $table->engine = 'InnoDB';
            
            $table->increments('id');
            $table->decimal('amount', 15, 4);
            $table->integer('source_account_id')->unsigned();
            $table->integer('destination_account_id')->unsigned();
            $table->string('comment')->nullable();
            $table->integer('user_id')->unsigned();
            $table->timestamps();
            $table->foreign('source_account_id')->references('id')->on('accounts');
            $table->foreign('destination_account_id')->references('id')->on('accounts');
            $table->foreign('user_id')->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('transfers');
    }
}
