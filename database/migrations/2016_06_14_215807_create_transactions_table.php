<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTransactionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('transactions', function (Blueprint $table) {

            $table->engine = 'InnoDB';
            
            $table->increments('id');
            $table->decimal('amount', 15, 4)->signed(); //(+) credit, (-) debit
            $table->integer('account_id')->unsigned();
            $table->integer('user_id')->unsigned();
            $table->string('naration')->nullable();
            $table->char('transaction_type', 2)->nullable(); //debit or credit
            $table->timestamps();
            $table->foreign('account_id')->references('id')->on('accounts');
            $table->foreign('user_id')->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('transactions');
    }
}
