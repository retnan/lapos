<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStocksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        Schema::create('stocks', function (Blueprint $table) {

            $table->engine = 'InnoDB';
            
            $table->increments('id');

            $table->integer('quantity');
            $table->decimal('unit_cost_price', 15, 4);
            $table->decimal('unit_selling_price', 15, 4);
            $table->decimal('tax', 5, 2)->default(0);
            $table->integer('product_id')->unsigned();
            $table->integer('user_id')->unsigned();            
            $table->timestamps();
            $table->foreign('product_id')->references('id')->on('products');
            $table->foreign('user_id')->references('id')->on('users');
        });
        
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('stocks');
    }
}
