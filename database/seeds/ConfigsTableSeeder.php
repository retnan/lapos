<?php

use Illuminate\Database\Seeder;
use App\Config;

class ConfigsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	$faker = Faker\Factory::create();
        Config::create(array(
            'company_short_name' => 'Le Gastro',
            'address' => 'Alliance Francaise, West of Mines, Jos.',
            'company_name' => 'Le Gastro Hive ',
            'api_key'    =>   $faker->bankAccountNumber,
            'api_secret' => $faker->sha256,
            'supports_warehouse' => false,
            'supports_tax' => false,
            'email' => 'chef_sewuese@legastro.com.ng',
            'mobile_number' => '08174462483'
        ));
    }
}
