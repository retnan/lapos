<?php

use Illuminate\Database\Seeder;
use App\User;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // DB::table('banks')->delete();
        // DB::table('accounts')->delete();
        // DB::table('customers')->delete();
        // DB::table('users')->delete();

        $faker = Faker\Factory::create();

        User::create(array(
            'id' => 5001,
            'name'     =>   'SYSTEM',
            'email'    =>   'd.retnan@nhubnigeria.com',
            'access_role' =>  'virtual',
            'password' =>   Hash::make($faker->password),
        ));

        User::create(array(
            'id' => 5002,
            'name'     =>   'ADMIN',
            'email'    =>   'admin@legastro.com.ng',
            'access_role' =>  'admin',
            'password' =>   Hash::make('fakesys001'),
        ));
        
        User::create(array(
            'id' => 5003,
            'name'     =>   'CASHIER-001',
            'email'    =>   'cashier001@legastro.com.ng',
            'access_role' =>  'cashier',
            'password' =>   Hash::make('faker00t002'),
        ));
        
        User::create(array(
            'id' => 5004,
            'name'     =>   'CUSTOMER',
            'email'    =>   $faker->email,
            'access_role' =>  'virtual',
            'password' =>   Hash::make($faker->password),
        ));
    }
}