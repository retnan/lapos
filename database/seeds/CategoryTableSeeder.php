<?php

use Illuminate\Database\Seeder;

use App\Category;

class CategoryTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Category::create(array(
            'id' => 1,
            'title'     =>   'General',
            'description'    =>   'general',
            'user_id' => 5001,
        ));
    }
}
