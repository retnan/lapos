<?php

use Illuminate\Database\Seeder;
use App\Account;
use App\Bank;

class BanksTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker\Factory::create();
        
        Bank::create(array(
            'title'     =>   'POS Bank I',
            'address'    =>   $faker->address,
            'sort_code'    =>   $faker->randomNumber(5),
            'user_id' => 5001,
            'account_id' => 2
        ));

        Bank::create(array(
            'title'     =>   'POS Bank II',
            'address'    =>   $faker->address,
            'sort_code'    =>   $faker->randomNumber(5),
            'user_id' => 5001,
            'account_id' => 3
        ));
    }
}
