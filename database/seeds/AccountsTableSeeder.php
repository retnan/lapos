<?php

use Illuminate\Database\Seeder;
use App\Account;
use LaPOS\AccountCode;

class AccountsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        /* Asset Account 1000 - 1999*/

        Account::create(array(
            'id' => 1,
            'title'     =>   'CASH',
            'balance' => 0,
            'account_no'    =>   AccountCode::$ASSETS_CASH,
            'user_id' => 5001, 
        ));

        /* Banks Sub Group 11*/
        Account::create(array(
            'id' => 2,
            'title'     =>   'POS BANK I',
            'account_no'    =>   AccountCode::$ASSETS_BANK . '00',
            'user_id' => 5001,
        ));

        Account::create(array(
            'id' => 3,
            'title'     =>   'POS BANK II',
            'account_no'    =>   AccountCode::$ASSETS_BANK . '01',
            'user_id' => 5001,
        ));

        Account::create(array(
            'id' => 4,
            'title'     =>   'TRANSFER',
            'account_no'    =>   AccountCode::$ASSETS_BANK . '02',
            'user_id' => 5001,
        ));

        Account::create(array(
            'id' => 5,
            'title'     =>   'PURCHASE',
            'account_no'    =>   AccountCode::$ASSETS_PURCHASE,
            'user_id' => 5001,
        ));

        Account::create(array(
            'id' => 6,
            'title'     =>   'SALES RETURN',
            'account_no'    =>   AccountCode::$ASSETS_SALES_RETURN,
            'user_id' => 5001,
        ));

        Account::create(array(
            'id' => 7,
            'title'     =>   'RECEIVABLE',
            'account_no'    =>   AccountCode::$ASSETS_RECEIVABLE,
            'user_id' => 5001,
        ));

        /* Liability Accounts 2000 - 3999*/

        Account::create(array(
            'id' => 8,
            'title'     =>   'PAYABLE',
            'account_no'    =>   AccountCode::$LIABILITY_PAYABLE,
            'user_id' => 5001,
        ));


        /* Revenue Accounts 4000 - 5999*/

        Account::create(array(
            'id' => 9,
            'title'     =>   'SALES',
            'account_no'    =>  AccountCode::$REVENUE_SALES,
            'user_id' => 5001,
        ));

        /* Expenses 6000 - 7999*/

        Account::create(array(
            'id' => 10,
            'title'     =>   'MISCELLANEOUS EXPENSES',
            'account_no'    => AccountCode::$EXPENSES_MISCELLANEOUS,
            'user_id' => 5001,
        ));

        Account::create(array(
            'id' => 11,
            'title'     =>   'SALARY',
            'account_no'    =>   AccountCode::$EXPENSES_SALARY,
            'user_id' => 5001,
        ));

        Account::create(array(
            'id' => 12,
            'title' => 'SALES DISCOUNT',
            'account_no' => AccountCode::$SALES_DISCOUNT,
            'user_id' => 5001
        ));
    }
}