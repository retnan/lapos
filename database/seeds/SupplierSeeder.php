<?php

use Illuminate\Database\Seeder;

use App\Supplier;

class SupplierSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	$faker = Faker\Factory::create();

        Supplier::create(array(
            'title'     =>   'SUPPLIER',
            'address'    =>   $faker->address,
            'mobile_number'    =>   $faker->e164PhoneNumber,
            'user_id' => 5001,
        ));
    }
}
