<?php

use Illuminate\Database\Seeder;
use App\Warehouse;

class WarehousesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker\Factory::create();
        Warehouse::create(array(
            'title'     =>   'WAREHOUSE-001',
            'address'    =>   $faker->streetAddress,
            'user_id' => 5001,
        ));
    }
}
