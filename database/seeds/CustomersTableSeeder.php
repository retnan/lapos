<?php

use Illuminate\Database\Seeder;

use App\Customer;

class CustomersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
    	$faker = Faker\Factory::create();

        Customer::create(array(
            'title'     =>   'CUSTOMER',
            'address'    =>   $faker->address,
            'mobile_number'    =>   $faker->e164PhoneNumber,
            'user_id' => 5004,
            'author_id' => 5001
        ));
    }
}
