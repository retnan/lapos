<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call(ConfigsTableSeeder::class);
        $this->call(UsersTableSeeder::class);
        $this->call(CategoryTableSeeder::class);
        $this->call(ProductSeeder::class);
        $this->call(AccountsTableSeeder::class);
        $this->call(BanksTableSeeder::class);
        $this->call(WarehousesTableSeeder::class);
        $this->call(CustomersTableSeeder::class);
        $this->call(SupplierSeeder::class);
    }
}
